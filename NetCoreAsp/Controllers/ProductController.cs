﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NetCoreAsp.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NetCoreAsp.Controllers
{
    public class ProductController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            MysqlConnectionController context = HttpContext.RequestServices.GetService(typeof(MysqlConnectionController)) as MysqlConnectionController;

            return View(context.GetAllProducts());

        }
    }
}
