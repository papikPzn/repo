﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsOrderReturnState
    {
        public int IdOrderReturnState { get; set; }
        public string Color { get; set; }
    }
}
