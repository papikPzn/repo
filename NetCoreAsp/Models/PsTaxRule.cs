﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsTaxRule
    {
        public int IdTaxRule { get; set; }
        public int Behavior { get; set; }
        public string Description { get; set; }
        public int IdCountry { get; set; }
        public int IdState { get; set; }
        public int IdTax { get; set; }
        public int IdTaxRulesGroup { get; set; }
        public string ZipcodeFrom { get; set; }
        public string ZipcodeTo { get; set; }
    }
}
