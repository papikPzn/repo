﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCartRuleProductRuleValue
    {
        public int IdProductRule { get; set; }
        public int IdItem { get; set; }
    }
}
