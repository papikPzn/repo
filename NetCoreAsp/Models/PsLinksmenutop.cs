﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsLinksmenutop
    {
        public int IdLinksmenutop { get; set; }
        public int IdShop { get; set; }
        public sbyte NewWindow { get; set; }
    }
}
