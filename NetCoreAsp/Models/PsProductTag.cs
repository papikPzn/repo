﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsProductTag
    {
        public int IdProduct { get; set; }
        public int IdTag { get; set; }
        public int IdLang { get; set; }
    }
}
