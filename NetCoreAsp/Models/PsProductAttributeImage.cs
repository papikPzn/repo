﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsProductAttributeImage
    {
        public int IdProductAttribute { get; set; }
        public int IdImage { get; set; }
    }
}
