﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsDateRange
    {
        public int IdDateRange { get; set; }
        public DateTime TimeEnd { get; set; }
        public DateTime TimeStart { get; set; }
    }
}
