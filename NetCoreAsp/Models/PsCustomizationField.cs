﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCustomizationField
    {
        public int IdCustomizationField { get; set; }
        public int IdProduct { get; set; }
        public sbyte Required { get; set; }
        public sbyte Type { get; set; }
    }
}
