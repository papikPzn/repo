﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsWebBrowser
    {
        public int IdWebBrowser { get; set; }
        public string Name { get; set; }
    }
}
