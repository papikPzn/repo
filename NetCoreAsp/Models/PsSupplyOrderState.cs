﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsSupplyOrderState
    {
        public int IdSupplyOrderState { get; set; }
        public string Color { get; set; }
        public sbyte DeliveryNote { get; set; }
        public sbyte Editable { get; set; }
        public sbyte Enclosed { get; set; }
        public sbyte PendingReceipt { get; set; }
        public sbyte ReceiptState { get; set; }
    }
}
