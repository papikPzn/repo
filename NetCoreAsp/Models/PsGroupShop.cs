﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsGroupShop
    {
        public int IdGroup { get; set; }
        public int IdShop { get; set; }
    }
}
