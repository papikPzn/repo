﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsConnectionsPage
    {
        public int IdConnections { get; set; }
        public int IdPage { get; set; }
        public DateTime TimeStart { get; set; }
        public DateTime? TimeEnd { get; set; }
    }
}
