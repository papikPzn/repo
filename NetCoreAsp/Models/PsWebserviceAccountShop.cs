﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsWebserviceAccountShop
    {
        public int IdWebserviceAccount { get; set; }
        public int IdShop { get; set; }
    }
}
