﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCategory
    {
        public int IdCategory { get; set; }
        public sbyte Active { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public int IdParent { get; set; }
        public int IdShopDefault { get; set; }
        public sbyte IsRootCategory { get; set; }
        public sbyte LevelDepth { get; set; }
        public int Nleft { get; set; }
        public int Nright { get; set; }
        public int Position { get; set; }
    }
}
