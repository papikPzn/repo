﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsOrderPayment
    {
        public int IdOrderPayment { get; set; }
        public decimal Amount { get; set; }
        public string CardBrand { get; set; }
        public char? CardExpiration { get; set; }
        public string CardHolder { get; set; }
        public string CardNumber { get; set; }
        public decimal ConversionRate { get; set; }
        public DateTime DateAdd { get; set; }
        public int IdCurrency { get; set; }
        public string OrderReference { get; set; }
        public string PaymentMethod { get; set; }
        public string TransactionId { get; set; }
    }
}
