﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsRiskLang
    {
        public int IdRisk { get; set; }
        public int IdLang { get; set; }
        public string Name { get; set; }
    }
}
