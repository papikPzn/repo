﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCategoryGroup
    {
        public int IdCategory { get; set; }
        public int IdGroup { get; set; }
    }
}
