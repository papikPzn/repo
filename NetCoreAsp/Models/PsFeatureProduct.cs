﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsFeatureProduct
    {
        public int IdFeature { get; set; }
        public int IdProduct { get; set; }
        public int IdFeatureValue { get; set; }
    }
}
