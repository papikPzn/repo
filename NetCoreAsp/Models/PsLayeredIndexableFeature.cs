﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsLayeredIndexableFeature
    {
        public int IdFeature { get; set; }
        public sbyte Indexable { get; set; }
    }
}
