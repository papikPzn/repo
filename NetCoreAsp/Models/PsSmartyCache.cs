﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsSmartyCache
    {
        public char IdSmartyCache { get; set; }
        public string CacheId { get; set; }
        public string Content { get; set; }
        public DateTime Modified { get; set; }
        public char Name { get; set; }
    }
}
