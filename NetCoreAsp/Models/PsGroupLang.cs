﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsGroupLang
    {
        public int IdGroup { get; set; }
        public int IdLang { get; set; }
        public string Name { get; set; }
    }
}
