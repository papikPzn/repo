﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsSupplier
    {
        public int IdSupplier { get; set; }
        public sbyte Active { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public string Name { get; set; }
    }
}
