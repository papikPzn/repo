﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsThemeconfigurator
    {
        public int IdItem { get; set; }
        public sbyte Active { get; set; }
        public string Hook { get; set; }
        public string Html { get; set; }
        public int IdLang { get; set; }
        public int IdShop { get; set; }
        public string Image { get; set; }
        public string ImageH { get; set; }
        public string ImageW { get; set; }
        public int ItemOrder { get; set; }
        public sbyte Target { get; set; }
        public string Title { get; set; }
        public sbyte TitleUse { get; set; }
        public string Url { get; set; }
    }
}
