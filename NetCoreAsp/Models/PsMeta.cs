﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsMeta
    {
        public int IdMeta { get; set; }
        public sbyte Configurable { get; set; }
        public string Page { get; set; }
    }
}
