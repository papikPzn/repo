﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsRangeWeight
    {
        public int IdRangeWeight { get; set; }
        public decimal Delimiter1 { get; set; }
        public decimal Delimiter2 { get; set; }
        public int IdCarrier { get; set; }
    }
}
