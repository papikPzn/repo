﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCustomerMessage
    {
        public int IdCustomerMessage { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public string FileName { get; set; }
        public int? IdCustomerThread { get; set; }
        public int? IdEmployee { get; set; }
        public string IpAddress { get; set; }
        public string Message { get; set; }
        public sbyte Private { get; set; }
        public sbyte Read { get; set; }
        public string UserAgent { get; set; }
    }
}
