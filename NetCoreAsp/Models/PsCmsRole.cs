﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCmsRole
    {
        public int IdCmsRole { get; set; }
        public int IdCms { get; set; }
        public string Name { get; set; }
    }
}
