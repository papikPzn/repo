﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsProductGroupReductionCache
    {
        public int IdProduct { get; set; }
        public int IdGroup { get; set; }
        public decimal Reduction { get; set; }
    }
}
