﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCategoryShop
    {
        public int IdCategory { get; set; }
        public int IdShop { get; set; }
        public int Position { get; set; }
    }
}
