﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsStockMvtReasonLang
    {
        public int IdStockMvtReason { get; set; }
        public int IdLang { get; set; }
        public string Name { get; set; }
    }
}
