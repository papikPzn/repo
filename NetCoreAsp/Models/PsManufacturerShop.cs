﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsManufacturerShop
    {
        public int IdManufacturer { get; set; }
        public int IdShop { get; set; }
    }
}
