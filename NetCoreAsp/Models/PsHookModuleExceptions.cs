﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsHookModuleExceptions
    {
        public int IdHookModuleExceptions { get; set; }
        public string FileName { get; set; }
        public int IdHook { get; set; }
        public int IdModule { get; set; }
        public int IdShop { get; set; }
    }
}
