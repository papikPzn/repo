﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsModuleShop
    {
        public int IdModule { get; set; }
        public int IdShop { get; set; }
        public sbyte EnableDevice { get; set; }
    }
}
