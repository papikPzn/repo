﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsOrderSlipDetail
    {
        public int IdOrderSlip { get; set; }
        public int IdOrderDetail { get; set; }
        public decimal? AmountTaxExcl { get; set; }
        public decimal? AmountTaxIncl { get; set; }
        public int ProductQuantity { get; set; }
        public decimal? TotalPriceTaxExcl { get; set; }
        public decimal? TotalPriceTaxIncl { get; set; }
        public decimal? UnitPriceTaxExcl { get; set; }
        public decimal? UnitPriceTaxIncl { get; set; }
    }
}
