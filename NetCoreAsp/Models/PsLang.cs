﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsLang
    {
        public int IdLang { get; set; }
        public sbyte Active { get; set; }
        public char DateFormatFull { get; set; }
        public char DateFormatLite { get; set; }
        public sbyte IsRtl { get; set; }
        public char IsoCode { get; set; }
        public char LanguageCode { get; set; }
        public string Name { get; set; }
    }
}
