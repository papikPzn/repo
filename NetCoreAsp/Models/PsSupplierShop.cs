﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsSupplierShop
    {
        public int IdSupplier { get; set; }
        public int IdShop { get; set; }
    }
}
