﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCmsBlock
    {
        public int IdCmsBlock { get; set; }
        public sbyte DisplayStore { get; set; }
        public int IdCmsCategory { get; set; }
        public sbyte Location { get; set; }
        public int Position { get; set; }
    }
}
