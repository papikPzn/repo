﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCartRuleCountry
    {
        public int IdCartRule { get; set; }
        public int IdCountry { get; set; }
    }
}
