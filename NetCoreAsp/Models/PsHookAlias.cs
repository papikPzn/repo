﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsHookAlias
    {
        public int IdHookAlias { get; set; }
        public string Alias { get; set; }
        public string Name { get; set; }
    }
}
