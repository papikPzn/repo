﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsOrderHistory
    {
        public int IdOrderHistory { get; set; }
        public DateTime DateAdd { get; set; }
        public int IdEmployee { get; set; }
        public int IdOrder { get; set; }
        public int IdOrderState { get; set; }
    }
}
