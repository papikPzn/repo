﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsOrderReturnDetail
    {
        public int IdOrderReturn { get; set; }
        public int IdOrderDetail { get; set; }
        public int IdCustomization { get; set; }
        public int ProductQuantity { get; set; }
    }
}
