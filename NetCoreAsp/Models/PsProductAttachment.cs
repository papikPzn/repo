﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsProductAttachment
    {
        public int IdProduct { get; set; }
        public int IdAttachment { get; set; }
    }
}
