﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsTimezone
    {
        public int IdTimezone { get; set; }
        public string Name { get; set; }
    }
}
