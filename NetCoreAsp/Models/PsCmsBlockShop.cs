﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCmsBlockShop
    {
        public int IdCmsBlock { get; set; }
        public int IdShop { get; set; }
    }
}
