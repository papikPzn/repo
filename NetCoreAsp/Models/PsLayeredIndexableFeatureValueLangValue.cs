﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsLayeredIndexableFeatureValueLangValue
    {
        public int IdFeatureValue { get; set; }
        public int IdLang { get; set; }
        public string MetaTitle { get; set; }
        public string UrlName { get; set; }
    }
}
