﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCustomer
    {
        public int IdCustomer { get; set; }
        public sbyte Active { get; set; }
        public string Ape { get; set; }
        public DateTime? Birthday { get; set; }
        public string Company { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public sbyte Deleted { get; set; }
        public string Email { get; set; }
        public string Firstname { get; set; }
        public int IdDefaultGroup { get; set; }
        public int IdGender { get; set; }
        public int? IdLang { get; set; }
        public int IdRisk { get; set; }
        public int IdShop { get; set; }
        public int IdShopGroup { get; set; }
        public string IpRegistrationNewsletter { get; set; }
        public sbyte IsGuest { get; set; }
        public DateTime LastPasswdGen { get; set; }
        public string Lastname { get; set; }
        public int MaxPaymentDays { get; set; }
        public sbyte Newsletter { get; set; }
        public DateTime? NewsletterDateAdd { get; set; }
        public string Note { get; set; }
        public sbyte Optin { get; set; }
        public decimal OutstandingAllowAmount { get; set; }
        public string Passwd { get; set; }
        public string SecureKey { get; set; }
        public sbyte ShowPublicPrices { get; set; }
        public string Siret { get; set; }
        public string Website { get; set; }
    }
}
