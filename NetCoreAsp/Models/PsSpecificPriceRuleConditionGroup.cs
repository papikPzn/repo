﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsSpecificPriceRuleConditionGroup
    {
        public int IdSpecificPriceRuleConditionGroup { get; set; }
        public int IdSpecificPriceRule { get; set; }
    }
}
