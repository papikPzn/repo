﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCarrierTaxRulesGroupShop
    {
        public int IdCarrier { get; set; }
        public int IdTaxRulesGroup { get; set; }
        public int IdShop { get; set; }
    }
}
