﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsSearchEngine
    {
        public int IdSearchEngine { get; set; }
        public string Getvar { get; set; }
        public string Server { get; set; }
    }
}
