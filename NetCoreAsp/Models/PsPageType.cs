﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsPageType
    {
        public int IdPageType { get; set; }
        public string Name { get; set; }
    }
}
