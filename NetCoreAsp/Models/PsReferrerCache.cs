﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsReferrerCache
    {
        public int IdConnectionsSource { get; set; }
        public int IdReferrer { get; set; }
    }
}
