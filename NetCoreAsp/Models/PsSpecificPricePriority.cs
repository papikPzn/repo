﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsSpecificPricePriority
    {
        public int IdSpecificPricePriority { get; set; }
        public int IdProduct { get; set; }
        public string Priority { get; set; }
    }
}
