﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsAttributeLang
    {
        public int IdAttribute { get; set; }
        public int IdLang { get; set; }
        public string Name { get; set; }
    }
}
