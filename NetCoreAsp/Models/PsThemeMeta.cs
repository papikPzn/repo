﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsThemeMeta
    {
        public int IdThemeMeta { get; set; }
        public int IdMeta { get; set; }
        public int IdTheme { get; set; }
        public sbyte LeftColumn { get; set; }
        public sbyte RightColumn { get; set; }
    }
}
