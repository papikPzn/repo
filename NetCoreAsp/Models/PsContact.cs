﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsContact
    {
        public int IdContact { get; set; }
        public sbyte CustomerService { get; set; }
        public string Email { get; set; }
        public sbyte Position { get; set; }
    }
}
