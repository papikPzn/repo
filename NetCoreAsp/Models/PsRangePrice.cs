﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsRangePrice
    {
        public int IdRangePrice { get; set; }
        public decimal Delimiter1 { get; set; }
        public decimal Delimiter2 { get; set; }
        public int IdCarrier { get; set; }
    }
}
