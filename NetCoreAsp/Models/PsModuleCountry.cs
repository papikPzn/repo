﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsModuleCountry
    {
        public int IdModule { get; set; }
        public int IdShop { get; set; }
        public int IdCountry { get; set; }
    }
}
