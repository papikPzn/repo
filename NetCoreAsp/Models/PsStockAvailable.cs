﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsStockAvailable
    {
        public int IdStockAvailable { get; set; }
        public sbyte DependsOnStock { get; set; }
        public int IdProduct { get; set; }
        public int IdProductAttribute { get; set; }
        public int IdShop { get; set; }
        public int IdShopGroup { get; set; }
        public sbyte OutOfStock { get; set; }
        public int Quantity { get; set; }
    }
}
