﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsProductAttributeCombination
    {
        public int IdAttribute { get; set; }
        public int IdProductAttribute { get; set; }
    }
}
