﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCmsCategoryShop
    {
        public int IdCmsCategory { get; set; }
        public int IdShop { get; set; }
    }
}
