﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCarrierZone
    {
        public int IdCarrier { get; set; }
        public int IdZone { get; set; }
    }
}
