﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsProduct
    {
        public int IdProduct { get; set; }
        public sbyte Active { get; set; }
        public decimal AdditionalShippingCost { get; set; }
        public sbyte AdvancedStockManagement { get; set; }
        public DateTime AvailableDate { get; set; }
        public sbyte AvailableForOrder { get; set; }
        public int? CacheDefaultAttribute { get; set; }
        public sbyte CacheHasAttachments { get; set; }
        public sbyte CacheIsPack { get; set; }
        public sbyte Customizable { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public decimal Depth { get; set; }
        public string Ean13 { get; set; }
        public decimal Ecotax { get; set; }
        public decimal Height { get; set; }
        public int? IdCategoryDefault { get; set; }
        public int? IdManufacturer { get; set; }
        public int IdProductRedirected { get; set; }
        public int IdShopDefault { get; set; }
        public int? IdSupplier { get; set; }
        public int IdTaxRulesGroup { get; set; }
        public sbyte Indexed { get; set; }
        public sbyte IsVirtual { get; set; }
        public string Location { get; set; }
        public int MinimalQuantity { get; set; }
        public sbyte OnSale { get; set; }
        public sbyte OnlineOnly { get; set; }
        public int OutOfStock { get; set; }
        public int PackStockType { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public sbyte? QuantityDiscount { get; set; }
        public string Reference { get; set; }
        public sbyte ShowPrice { get; set; }
        public string SupplierReference { get; set; }
        public sbyte TextFields { get; set; }
        public decimal UnitPriceRatio { get; set; }
        public string Unity { get; set; }
        public string Upc { get; set; }
        public sbyte UploadableFiles { get; set; }
        public decimal Weight { get; set; }
        public decimal WholesalePrice { get; set; }
        public decimal Width { get; set; }
    }
}
