﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsAdvice
    {
        public int IdAdvice { get; set; }
        public sbyte Hide { get; set; }
        public int IdPsAdvice { get; set; }
        public int IdTab { get; set; }
        public string IdsTab { get; set; }
        public string Selector { get; set; }
        public int StartDay { get; set; }
        public int StopDay { get; set; }
        public sbyte Validated { get; set; }
        public int? Weight { get; set; }
    }
}
