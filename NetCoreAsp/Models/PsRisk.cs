﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsRisk
    {
        public int IdRisk { get; set; }
        public string Color { get; set; }
        public sbyte Percent { get; set; }
    }
}
