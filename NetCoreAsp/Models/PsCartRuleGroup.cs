﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCartRuleGroup
    {
        public int IdCartRule { get; set; }
        public int IdGroup { get; set; }
    }
}
