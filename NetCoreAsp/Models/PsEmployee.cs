﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsEmployee
    {
        public int IdEmployee { get; set; }
        public sbyte Active { get; set; }
        public string BoColor { get; set; }
        public string BoCss { get; set; }
        public sbyte BoMenu { get; set; }
        public string BoTheme { get; set; }
        public int BoWidth { get; set; }
        public int DefaultTab { get; set; }
        public string Email { get; set; }
        public string Firstname { get; set; }
        public int IdLang { get; set; }
        public int IdLastCustomer { get; set; }
        public int IdLastCustomerMessage { get; set; }
        public int IdLastOrder { get; set; }
        public int IdProfile { get; set; }
        public DateTime? LastConnectionDate { get; set; }
        public DateTime LastPasswdGen { get; set; }
        public string Lastname { get; set; }
        public sbyte Optin { get; set; }
        public string Passwd { get; set; }
        public string PreselectDateRange { get; set; }
        public DateTime? StatsCompareFrom { get; set; }
        public int StatsCompareOption { get; set; }
        public DateTime? StatsCompareTo { get; set; }
        public DateTime? StatsDateFrom { get; set; }
        public DateTime? StatsDateTo { get; set; }
    }
}
