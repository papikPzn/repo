﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsWarehouseShop
    {
        public int IdShop { get; set; }
        public int IdWarehouse { get; set; }
    }
}
