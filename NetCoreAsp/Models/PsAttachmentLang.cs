﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsAttachmentLang
    {
        public int IdAttachment { get; set; }
        public int IdLang { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
    }
}
