﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsTagCount
    {
        public int IdGroup { get; set; }
        public int IdTag { get; set; }
        public int Counter { get; set; }
        public int IdLang { get; set; }
        public int IdShop { get; set; }
    }
}
