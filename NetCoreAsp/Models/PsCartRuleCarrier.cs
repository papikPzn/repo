﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCartRuleCarrier
    {
        public int IdCartRule { get; set; }
        public int IdCarrier { get; set; }
    }
}
