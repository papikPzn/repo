﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsEmployeeShop
    {
        public int IdEmployee { get; set; }
        public int IdShop { get; set; }
    }
}
