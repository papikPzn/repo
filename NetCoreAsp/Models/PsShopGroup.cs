﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsShopGroup
    {
        public int IdShopGroup { get; set; }
        public sbyte Active { get; set; }
        public sbyte Deleted { get; set; }
        public string Name { get; set; }
        public sbyte ShareCustomer { get; set; }
        public sbyte ShareOrder { get; set; }
        public sbyte ShareStock { get; set; }
    }
}
