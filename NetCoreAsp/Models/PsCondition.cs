﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCondition
    {
        public int IdCondition { get; set; }
        public int IdPsCondition { get; set; }
        public string CalculationDetail { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public string Operator { get; set; }
        public string Request { get; set; }
        public string Result { get; set; }
        public sbyte Validated { get; set; }
        public string Value { get; set; }
    }
}
