﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCarrierGroup
    {
        public int IdCarrier { get; set; }
        public int IdGroup { get; set; }
    }
}
