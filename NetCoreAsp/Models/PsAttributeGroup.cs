﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsAttributeGroup
    {
        public int IdAttributeGroup { get; set; }
        public sbyte IsColorGroup { get; set; }
        public int Position { get; set; }
    }
}
