﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsConnections
    {
        public int IdConnections { get; set; }
        public DateTime DateAdd { get; set; }
        public string HttpReferer { get; set; }
        public int IdGuest { get; set; }
        public int IdPage { get; set; }
        public int IdShop { get; set; }
        public int IdShopGroup { get; set; }
        public long? IpAddress { get; set; }
    }
}
