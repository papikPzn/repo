﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsRequestSql
    {
        public int IdRequestSql { get; set; }
        public string Name { get; set; }
        public string Sql { get; set; }
    }
}
