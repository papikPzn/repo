﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsZone
    {
        public int IdZone { get; set; }
        public sbyte Active { get; set; }
        public string Name { get; set; }
    }
}
