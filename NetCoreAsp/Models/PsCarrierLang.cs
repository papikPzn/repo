﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCarrierLang
    {
        public int IdCarrier { get; set; }
        public int IdShop { get; set; }
        public int IdLang { get; set; }
        public string Delay { get; set; }
    }
}
