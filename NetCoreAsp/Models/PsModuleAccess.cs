﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsModuleAccess
    {
        public int IdProfile { get; set; }
        public int IdModule { get; set; }
        public sbyte Configure { get; set; }
        public sbyte Uninstall { get; set; }
        public sbyte View { get; set; }
    }
}
