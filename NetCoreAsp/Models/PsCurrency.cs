﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCurrency
    {
        public int IdCurrency { get; set; }
        public sbyte Active { get; set; }
        public sbyte Blank { get; set; }
        public decimal ConversionRate { get; set; }
        public sbyte Decimals { get; set; }
        public sbyte Deleted { get; set; }
        public sbyte Format { get; set; }
        public string IsoCode { get; set; }
        public string IsoCodeNum { get; set; }
        public string Name { get; set; }
        public string Sign { get; set; }
    }
}
