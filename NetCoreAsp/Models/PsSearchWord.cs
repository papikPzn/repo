﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsSearchWord
    {
        public int IdWord { get; set; }
        public int IdLang { get; set; }
        public int IdShop { get; set; }
        public string Word { get; set; }
    }
}
