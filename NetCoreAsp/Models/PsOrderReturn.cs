﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsOrderReturn
    {
        public int IdOrderReturn { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public int IdCustomer { get; set; }
        public int IdOrder { get; set; }
        public string Question { get; set; }
        public sbyte State { get; set; }
    }
}
