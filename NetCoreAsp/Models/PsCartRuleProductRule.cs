﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCartRuleProductRule
    {
        public int IdProductRule { get; set; }
        public int IdProductRuleGroup { get; set; }
    }
}
