﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsBadgeLang
    {
        public int IdBadge { get; set; }
        public int IdLang { get; set; }
        public string Description { get; set; }
        public string GroupName { get; set; }
        public string Name { get; set; }
    }
}
