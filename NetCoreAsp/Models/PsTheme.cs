﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsTheme
    {
        public int IdTheme { get; set; }
        public sbyte DefaultLeftColumn { get; set; }
        public sbyte DefaultRightColumn { get; set; }
        public string Directory { get; set; }
        public string Name { get; set; }
        public int ProductPerPage { get; set; }
        public sbyte Responsive { get; set; }
    }
}
