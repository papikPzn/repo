﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsFeatureShop
    {
        public int IdFeature { get; set; }
        public int IdShop { get; set; }
    }
}
