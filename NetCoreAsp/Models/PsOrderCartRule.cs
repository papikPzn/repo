﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsOrderCartRule
    {
        public int IdOrderCartRule { get; set; }
        public sbyte FreeShipping { get; set; }
        public int IdCartRule { get; set; }
        public int IdOrder { get; set; }
        public int? IdOrderInvoice { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
        public decimal ValueTaxExcl { get; set; }
    }
}
