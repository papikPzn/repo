﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsStockMvtReason
    {
        public int IdStockMvtReason { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public sbyte Deleted { get; set; }
        public sbyte Sign { get; set; }
    }
}
