﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsZoneShop
    {
        public int IdZone { get; set; }
        public int IdShop { get; set; }
    }
}
