﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsQuickAccessLang
    {
        public int IdQuickAccess { get; set; }
        public int IdLang { get; set; }
        public string Name { get; set; }
    }
}
