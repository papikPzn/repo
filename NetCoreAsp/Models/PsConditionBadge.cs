﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsConditionBadge
    {
        public int IdCondition { get; set; }
        public int IdBadge { get; set; }
    }
}
