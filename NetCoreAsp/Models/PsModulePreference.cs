﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsModulePreference
    {
        public int IdModulePreference { get; set; }
        public sbyte? Favorite { get; set; }
        public int IdEmployee { get; set; }
        public sbyte? Interest { get; set; }
        public string Module { get; set; }
    }
}
