﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsHomeslider
    {
        public int IdHomesliderSlides { get; set; }
        public int IdShop { get; set; }
    }
}
