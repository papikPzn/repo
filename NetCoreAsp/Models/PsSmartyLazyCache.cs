﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsSmartyLazyCache
    {
        public string TemplateHash { get; set; }
        public string CacheId { get; set; }
        public string CompileId { get; set; }
        public string Filepath { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}
