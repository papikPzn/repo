﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCmsShop
    {
        public int IdCms { get; set; }
        public int IdShop { get; set; }
    }
}
