﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsLayeredIndexableAttributeGroup
    {
        public int IdAttributeGroup { get; set; }
        public sbyte Indexable { get; set; }
    }
}
