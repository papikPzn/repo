﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCms
    {
        public int IdCms { get; set; }
        public sbyte Active { get; set; }
        public int IdCmsCategory { get; set; }
        public sbyte Indexation { get; set; }
        public int Position { get; set; }
    }
}
