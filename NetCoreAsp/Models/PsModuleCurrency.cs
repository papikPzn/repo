﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsModuleCurrency
    {
        public int IdModule { get; set; }
        public int IdShop { get; set; }
        public int IdCurrency { get; set; }
    }
}
