﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsHomesliderSlidesLang
    {
        public int IdHomesliderSlides { get; set; }
        public int IdLang { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Legend { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
    }
}
