﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCustomizedData
    {
        public int IdCustomization { get; set; }
        public sbyte Type { get; set; }
        public int Index { get; set; }
        public string Value { get; set; }
    }
}
