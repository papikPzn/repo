﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCarrierShop
    {
        public int IdCarrier { get; set; }
        public int IdShop { get; set; }
    }
}
