﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsPack
    {
        public int IdProductPack { get; set; }
        public int IdProductItem { get; set; }
        public int IdProductAttributeItem { get; set; }
        public int Quantity { get; set; }
    }
}
