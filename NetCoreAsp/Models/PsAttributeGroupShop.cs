﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsAttributeGroupShop
    {
        public int IdAttributeGroup { get; set; }
        public int IdShop { get; set; }
    }
}
