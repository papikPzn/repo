﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsAttributeImpact
    {
        public int IdAttributeImpact { get; set; }
        public int IdAttribute { get; set; }
        public int IdProduct { get; set; }
        public decimal Price { get; set; }
        public decimal Weight { get; set; }
    }
}
