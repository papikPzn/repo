﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCountry
    {
        public int IdCountry { get; set; }
        public sbyte Active { get; set; }
        public int CallPrefix { get; set; }
        public sbyte ContainsStates { get; set; }
        public sbyte DisplayTaxLabel { get; set; }
        public int IdCurrency { get; set; }
        public int IdZone { get; set; }
        public string IsoCode { get; set; }
        public sbyte NeedIdentificationNumber { get; set; }
        public sbyte NeedZipCode { get; set; }
        public string ZipCodeFormat { get; set; }
    }
}
