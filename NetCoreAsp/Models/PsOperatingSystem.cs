﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsOperatingSystem
    {
        public int IdOperatingSystem { get; set; }
        public string Name { get; set; }
    }
}
