﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsImageShop
    {
        public int IdImage { get; set; }
        public int IdShop { get; set; }
        public sbyte? Cover { get; set; }
        public int IdProduct { get; set; }
    }
}
