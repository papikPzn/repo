﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsImageLang
    {
        public int IdImage { get; set; }
        public int IdLang { get; set; }
        public string Legend { get; set; }
    }
}
