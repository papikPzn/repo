﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsProfileLang
    {
        public int IdLang { get; set; }
        public int IdProfile { get; set; }
        public string Name { get; set; }
    }
}
