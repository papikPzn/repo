﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsWebservicePermission
    {
        public int IdWebservicePermission { get; set; }
        public int IdWebserviceAccount { get; set; }
        public string Resource { get; set; }
    }
}
