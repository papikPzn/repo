﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsWarehouse
    {
        public int IdWarehouse { get; set; }
        public sbyte Deleted { get; set; }
        public int IdAddress { get; set; }
        public int IdCurrency { get; set; }
        public int IdEmployee { get; set; }
        public string Name { get; set; }
        public string Reference { get; set; }
    }
}
