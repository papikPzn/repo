﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsFeatureValueLang
    {
        public int IdFeatureValue { get; set; }
        public int IdLang { get; set; }
        public string Value { get; set; }
    }
}
