﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsContactShop
    {
        public int IdContact { get; set; }
        public int IdShop { get; set; }
    }
}
