﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsOrderReturnStateLang
    {
        public int IdOrderReturnState { get; set; }
        public int IdLang { get; set; }
        public string Name { get; set; }
    }
}
