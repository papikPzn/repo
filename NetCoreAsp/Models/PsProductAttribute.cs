﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsProductAttribute
    {
        public int IdProductAttribute { get; set; }
        public DateTime AvailableDate { get; set; }
        public sbyte? DefaultOn { get; set; }
        public string Ean13 { get; set; }
        public decimal Ecotax { get; set; }
        public int IdProduct { get; set; }
        public string Location { get; set; }
        public int MinimalQuantity { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public string Reference { get; set; }
        public string SupplierReference { get; set; }
        public decimal UnitPriceImpact { get; set; }
        public string Upc { get; set; }
        public decimal Weight { get; set; }
        public decimal WholesalePrice { get; set; }
    }
}
