﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCmsBlockPage
    {
        public int IdCmsBlockPage { get; set; }
        public int IdCms { get; set; }
        public int IdCmsBlock { get; set; }
        public sbyte IsCategory { get; set; }
    }
}
