﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsQuickAccess
    {
        public int IdQuickAccess { get; set; }
        public string Link { get; set; }
        public sbyte NewWindow { get; set; }
    }
}
