﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsInfoLang
    {
        public int IdInfo { get; set; }
        public int IdLang { get; set; }
        public string Text { get; set; }
    }
}
