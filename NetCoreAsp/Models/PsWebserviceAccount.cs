﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsWebserviceAccount
    {
        public int IdWebserviceAccount { get; set; }
        public sbyte Active { get; set; }
        public string ClassName { get; set; }
        public string Description { get; set; }
        public sbyte IsModule { get; set; }
        public string Key { get; set; }
        public string ModuleName { get; set; }
    }
}
