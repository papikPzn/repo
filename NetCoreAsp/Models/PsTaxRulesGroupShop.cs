﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsTaxRulesGroupShop
    {
        public int IdTaxRulesGroup { get; set; }
        public int IdShop { get; set; }
    }
}
