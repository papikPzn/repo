﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCmsRoleLang
    {
        public int IdCmsRole { get; set; }
        public int IdLang { get; set; }
        public int IdShop { get; set; }
        public string Name { get; set; }
    }
}
