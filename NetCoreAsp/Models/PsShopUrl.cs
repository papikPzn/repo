﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsShopUrl
    {
        public int IdShopUrl { get; set; }
        public sbyte Active { get; set; }
        public string Domain { get; set; }
        public string DomainSsl { get; set; }
        public int IdShop { get; set; }
        public sbyte Main { get; set; }
        public string PhysicalUri { get; set; }
        public string VirtualUri { get; set; }
    }
}
