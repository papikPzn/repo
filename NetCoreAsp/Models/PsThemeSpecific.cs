﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsThemeSpecific
    {
        public int IdTheme { get; set; }
        public int IdShop { get; set; }
        public int Entity { get; set; }
        public int IdObject { get; set; }
    }
}
