﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCartRuleLang
    {
        public int IdCartRule { get; set; }
        public int IdLang { get; set; }
        public string Name { get; set; }
    }
}
