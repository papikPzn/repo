﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsProductSale
    {
        public int IdProduct { get; set; }
        public DateTime DateUpd { get; set; }
        public int Quantity { get; set; }
        public int SaleNbr { get; set; }
    }
}
