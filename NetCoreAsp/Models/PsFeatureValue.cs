﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsFeatureValue
    {
        public int IdFeatureValue { get; set; }
        public sbyte? Custom { get; set; }
        public int IdFeature { get; set; }
    }
}
