﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsOrderState
    {
        public int IdOrderState { get; set; }
        public string Color { get; set; }
        public sbyte Deleted { get; set; }
        public sbyte Delivery { get; set; }
        public sbyte Hidden { get; set; }
        public sbyte? Invoice { get; set; }
        public sbyte Logable { get; set; }
        public string ModuleName { get; set; }
        public sbyte Paid { get; set; }
        public sbyte PdfDelivery { get; set; }
        public sbyte PdfInvoice { get; set; }
        public sbyte SendEmail { get; set; }
        public sbyte Shipped { get; set; }
        public sbyte Unremovable { get; set; }
    }
}
