﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCartProduct
    {
        public int IdCart { get; set; }
        public int IdProduct { get; set; }
        public int IdAddressDelivery { get; set; }
        public int IdProductAttribute { get; set; }
        public DateTime DateAdd { get; set; }
        public int IdShop { get; set; }
        public int Quantity { get; set; }
    }
}
