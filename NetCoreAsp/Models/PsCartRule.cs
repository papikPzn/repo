﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCartRule
    {
        public int IdCartRule { get; set; }
        public sbyte Active { get; set; }
        public sbyte CarrierRestriction { get; set; }
        public sbyte CartRuleRestriction { get; set; }
        public string Code { get; set; }
        public sbyte CountryRestriction { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public DateTime DateUpd { get; set; }
        public string Description { get; set; }
        public sbyte FreeShipping { get; set; }
        public int GiftProduct { get; set; }
        public int GiftProductAttribute { get; set; }
        public sbyte GroupRestriction { get; set; }
        public sbyte Highlight { get; set; }
        public int IdCustomer { get; set; }
        public decimal MinimumAmount { get; set; }
        public int MinimumAmountCurrency { get; set; }
        public sbyte MinimumAmountShipping { get; set; }
        public sbyte MinimumAmountTax { get; set; }
        public sbyte PartialUse { get; set; }
        public int Priority { get; set; }
        public sbyte ProductRestriction { get; set; }
        public int Quantity { get; set; }
        public int QuantityPerUser { get; set; }
        public decimal ReductionAmount { get; set; }
        public int ReductionCurrency { get; set; }
        public decimal ReductionPercent { get; set; }
        public int ReductionProduct { get; set; }
        public sbyte ReductionTax { get; set; }
        public sbyte ShopRestriction { get; set; }
    }
}
