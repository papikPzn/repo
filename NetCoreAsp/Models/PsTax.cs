﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsTax
    {
        public int IdTax { get; set; }
        public sbyte Active { get; set; }
        public sbyte Deleted { get; set; }
        public decimal Rate { get; set; }
    }
}
