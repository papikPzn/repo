﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsSceneCategory
    {
        public int IdScene { get; set; }
        public int IdCategory { get; set; }
    }
}
