﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCartRuleCombination
    {
        public int IdCartRule1 { get; set; }
        public int IdCartRule2 { get; set; }
    }
}
