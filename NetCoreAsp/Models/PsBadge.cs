﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsBadge
    {
        public int IdBadge { get; set; }
        public int? Awb { get; set; }
        public int GroupPosition { get; set; }
        public int IdGroup { get; set; }
        public int IdPsBadge { get; set; }
        public int Scoring { get; set; }
        public string Type { get; set; }
        public sbyte Validated { get; set; }
    }
}
