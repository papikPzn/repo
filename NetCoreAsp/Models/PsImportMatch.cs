﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsImportMatch
    {
        public int IdImportMatch { get; set; }
        public string Match { get; set; }
        public string Name { get; set; }
        public int Skip { get; set; }
    }
}
