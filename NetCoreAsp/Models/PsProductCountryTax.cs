﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsProductCountryTax
    {
        public int IdProduct { get; set; }
        public int IdCountry { get; set; }
        public int IdTax { get; set; }
    }
}
