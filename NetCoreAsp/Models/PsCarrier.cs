﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCarrier
    {
        public int IdCarrier { get; set; }
        public sbyte Active { get; set; }
        public sbyte Deleted { get; set; }
        public string ExternalModuleName { get; set; }
        public int? Grade { get; set; }
        public int IdReference { get; set; }
        public int? IdTaxRulesGroup { get; set; }
        public sbyte IsFree { get; set; }
        public sbyte IsModule { get; set; }
        public int? MaxDepth { get; set; }
        public int? MaxHeight { get; set; }
        public decimal? MaxWeight { get; set; }
        public int? MaxWidth { get; set; }
        public string Name { get; set; }
        public sbyte NeedRange { get; set; }
        public int Position { get; set; }
        public sbyte RangeBehavior { get; set; }
        public sbyte ShippingExternal { get; set; }
        public sbyte ShippingHandling { get; set; }
        public int ShippingMethod { get; set; }
        public string Url { get; set; }
    }
}
