﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsOrderMessage
    {
        public int IdOrderMessage { get; set; }
        public DateTime DateAdd { get; set; }
    }
}
