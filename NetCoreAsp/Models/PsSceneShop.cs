﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsSceneShop
    {
        public int IdScene { get; set; }
        public int IdShop { get; set; }
    }
}
