﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsGuest
    {
        public int IdGuest { get; set; }
        public string AcceptLanguage { get; set; }
        public sbyte? AdobeDirector { get; set; }
        public sbyte? AdobeFlash { get; set; }
        public sbyte? AppleQuicktime { get; set; }
        public int? IdCustomer { get; set; }
        public int? IdOperatingSystem { get; set; }
        public int? IdWebBrowser { get; set; }
        public sbyte? Javascript { get; set; }
        public sbyte MobileTheme { get; set; }
        public sbyte? RealPlayer { get; set; }
        public sbyte? ScreenColor { get; set; }
        public short? ScreenResolutionX { get; set; }
        public short? ScreenResolutionY { get; set; }
        public sbyte? SunJava { get; set; }
        public sbyte? WindowsMedia { get; set; }
    }
}
