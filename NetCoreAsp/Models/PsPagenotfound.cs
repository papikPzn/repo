﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsPagenotfound
    {
        public int IdPagenotfound { get; set; }
        public DateTime DateAdd { get; set; }
        public string HttpReferer { get; set; }
        public int IdShop { get; set; }
        public int IdShopGroup { get; set; }
        public string RequestUri { get; set; }
    }
}
