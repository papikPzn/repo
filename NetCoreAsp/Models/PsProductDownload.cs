﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsProductDownload
    {
        public int IdProductDownload { get; set; }
        public sbyte Active { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime? DateExpiration { get; set; }
        public string DisplayFilename { get; set; }
        public string Filename { get; set; }
        public int IdProduct { get; set; }
        public sbyte IsShareable { get; set; }
        public int? NbDaysAccessible { get; set; }
        public int? NbDownloadable { get; set; }
    }
}
