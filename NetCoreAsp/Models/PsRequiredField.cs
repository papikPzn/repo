﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsRequiredField
    {
        public int IdRequiredField { get; set; }
        public string FieldName { get; set; }
        public string ObjectName { get; set; }
    }
}
