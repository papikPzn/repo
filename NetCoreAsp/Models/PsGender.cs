﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsGender
    {
        public int IdGender { get; set; }
        public sbyte Type { get; set; }
    }
}
