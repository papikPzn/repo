﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsTaxLang
    {
        public int IdTax { get; set; }
        public int IdLang { get; set; }
        public string Name { get; set; }
    }
}
