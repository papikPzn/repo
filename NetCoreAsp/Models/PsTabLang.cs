﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsTabLang
    {
        public int IdTab { get; set; }
        public int IdLang { get; set; }
        public string Name { get; set; }
    }
}
