﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsTaxRulesGroup
    {
        public int IdTaxRulesGroup { get; set; }
        public int Active { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public sbyte Deleted { get; set; }
        public string Name { get; set; }
    }
}
