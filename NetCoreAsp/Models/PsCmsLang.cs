﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCmsLang
    {
        public int IdCms { get; set; }
        public int IdLang { get; set; }
        public int IdShop { get; set; }
        public string Content { get; set; }
        public string LinkRewrite { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaTitle { get; set; }
    }
}
