﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsTab
    {
        public int IdTab { get; set; }
        public sbyte Active { get; set; }
        public string ClassName { get; set; }
        public sbyte HideHostMode { get; set; }
        public int IdParent { get; set; }
        public string Module { get; set; }
        public int Position { get; set; }
    }
}
