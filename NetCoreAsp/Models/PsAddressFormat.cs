﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsAddressFormat
    {
        public int IdCountry { get; set; }
        public string Format { get; set; }
    }
}
