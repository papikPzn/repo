﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsStore
    {
        public int IdStore { get; set; }
        public sbyte Active { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string Hours { get; set; }
        public int IdCountry { get; set; }
        public int? IdState { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public string Phone { get; set; }
        public string Postcode { get; set; }
    }
}
