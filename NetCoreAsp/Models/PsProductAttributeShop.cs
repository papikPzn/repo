﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsProductAttributeShop
    {
        public int IdProductAttribute { get; set; }
        public int IdShop { get; set; }
        public DateTime AvailableDate { get; set; }
        public sbyte? DefaultOn { get; set; }
        public decimal Ecotax { get; set; }
        public int IdProduct { get; set; }
        public int MinimalQuantity { get; set; }
        public decimal Price { get; set; }
        public decimal UnitPriceImpact { get; set; }
        public decimal Weight { get; set; }
        public decimal WholesalePrice { get; set; }
    }
}
