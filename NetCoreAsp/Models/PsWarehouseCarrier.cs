﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsWarehouseCarrier
    {
        public int IdCarrier { get; set; }
        public int IdWarehouse { get; set; }
    }
}
