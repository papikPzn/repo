﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsOrderSlip
    {
        public int IdOrderSlip { get; set; }
        public decimal Amount { get; set; }
        public decimal ConversionRate { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public int IdCustomer { get; set; }
        public int IdOrder { get; set; }
        public sbyte OrderSlipType { get; set; }
        public sbyte Partial { get; set; }
        public sbyte ShippingCost { get; set; }
        public decimal ShippingCostAmount { get; set; }
        public decimal? TotalProductsTaxExcl { get; set; }
        public decimal? TotalProductsTaxIncl { get; set; }
        public decimal? TotalShippingTaxExcl { get; set; }
        public decimal? TotalShippingTaxIncl { get; set; }
    }
}
