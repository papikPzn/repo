﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsLayeredIndexableAttributeGroupLangValue
    {
        public int IdAttributeGroup { get; set; }
        public int IdLang { get; set; }
        public string MetaTitle { get; set; }
        public string UrlName { get; set; }
    }
}
