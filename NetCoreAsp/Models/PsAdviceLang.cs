﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsAdviceLang
    {
        public int IdAdvice { get; set; }
        public int IdLang { get; set; }
        public string Html { get; set; }
    }
}
