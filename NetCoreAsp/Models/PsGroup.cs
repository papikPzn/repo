﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsGroup
    {
        public int IdGroup { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public sbyte PriceDisplayMethod { get; set; }
        public decimal Reduction { get; set; }
        public sbyte ShowPrices { get; set; }
    }
}
