﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCmsCategory
    {
        public int IdCmsCategory { get; set; }
        public sbyte Active { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public int IdParent { get; set; }
        public sbyte LevelDepth { get; set; }
        public int Position { get; set; }
    }
}
