﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsReferrerShop
    {
        public int IdReferrer { get; set; }
        public int IdShop { get; set; }
        public decimal? CacheOrderRate { get; set; }
        public int? CacheOrders { get; set; }
        public int? CachePages { get; set; }
        public decimal? CacheRegRate { get; set; }
        public int? CacheRegistrations { get; set; }
        public decimal? CacheSales { get; set; }
        public int? CacheVisitors { get; set; }
        public int? CacheVisits { get; set; }
    }
}
