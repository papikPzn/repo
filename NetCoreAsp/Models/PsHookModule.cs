﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsHookModule
    {
        public int IdModule { get; set; }
        public int IdShop { get; set; }
        public int IdHook { get; set; }
        public sbyte Position { get; set; }
    }
}
