﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsScene
    {
        public int IdScene { get; set; }
        public sbyte Active { get; set; }
    }
}
