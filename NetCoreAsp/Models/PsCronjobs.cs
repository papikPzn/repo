﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCronjobs
    {
        public int IdCronjob { get; set; }
        public sbyte? Active { get; set; }
        public int? Day { get; set; }
        public int? DayOfWeek { get; set; }
        public string Description { get; set; }
        public int? Hour { get; set; }
        public int? IdModule { get; set; }
        public int? IdShop { get; set; }
        public int? IdShopGroup { get; set; }
        public int? Month { get; set; }
        public sbyte OneShot { get; set; }
        public string Task { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
