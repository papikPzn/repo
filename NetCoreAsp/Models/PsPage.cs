﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsPage
    {
        public int IdPage { get; set; }
        public int? IdObject { get; set; }
        public int IdPageType { get; set; }
    }
}
