﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsLayeredFriendlyUrl
    {
        public int IdLayeredFriendlyUrl { get; set; }
        public string Data { get; set; }
        public int IdLang { get; set; }
        public string UrlKey { get; set; }
    }
}
