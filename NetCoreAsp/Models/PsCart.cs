﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCart
    {
        public int IdCart { get; set; }
        public sbyte AllowSeperatedPackage { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public string DeliveryOption { get; set; }
        public sbyte Gift { get; set; }
        public string GiftMessage { get; set; }
        public int IdAddressDelivery { get; set; }
        public int IdAddressInvoice { get; set; }
        public int IdCarrier { get; set; }
        public int IdCurrency { get; set; }
        public int IdCustomer { get; set; }
        public int IdGuest { get; set; }
        public int IdLang { get; set; }
        public int IdShop { get; set; }
        public int IdShopGroup { get; set; }
        public sbyte MobileTheme { get; set; }
        public sbyte Recyclable { get; set; }
        public string SecureKey { get; set; }
    }
}
