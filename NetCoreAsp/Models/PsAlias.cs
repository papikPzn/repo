﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsAlias
    {
        public int IdAlias { get; set; }
        public sbyte Active { get; set; }
        public string Alias { get; set; }
        public string Search { get; set; }
    }
}
