﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsLog
    {
        public int IdLog { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public int? ErrorCode { get; set; }
        public int? IdEmployee { get; set; }
        public string Message { get; set; }
        public int? ObjectId { get; set; }
        public string ObjectType { get; set; }
        public sbyte Severity { get; set; }
    }
}
