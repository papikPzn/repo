﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsImage
    {
        public int IdImage { get; set; }
        public sbyte? Cover { get; set; }
        public int IdProduct { get; set; }
        public short Position { get; set; }
    }
}
