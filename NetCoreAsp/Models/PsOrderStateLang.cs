﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsOrderStateLang
    {
        public int IdOrderState { get; set; }
        public int IdLang { get; set; }
        public string Name { get; set; }
        public string Template { get; set; }
    }
}
