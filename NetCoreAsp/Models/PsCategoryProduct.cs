﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCategoryProduct
    {
        public int IdCategory { get; set; }
        public int IdProduct { get; set; }
        public int Position { get; set; }
    }
}
