﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsState
    {
        public int IdState { get; set; }
        public sbyte Active { get; set; }
        public int IdCountry { get; set; }
        public int IdZone { get; set; }
        public string IsoCode { get; set; }
        public string Name { get; set; }
        public short TaxBehavior { get; set; }
    }
}
