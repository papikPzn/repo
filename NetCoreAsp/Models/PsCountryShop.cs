﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCountryShop
    {
        public int IdCountry { get; set; }
        public int IdShop { get; set; }
    }
}
