﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsModulesPerfs
    {
        public int IdModulesPerfs { get; set; }
        public int MemoryEnd { get; set; }
        public int MemoryStart { get; set; }
        public string Method { get; set; }
        public string Module { get; set; }
        public int Session { get; set; }
        public decimal TimeEnd { get; set; }
        public decimal TimeStart { get; set; }
    }
}
