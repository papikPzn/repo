﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsMessageReaded
    {
        public int IdMessage { get; set; }
        public int IdEmployee { get; set; }
        public DateTime DateAdd { get; set; }
    }
}
