﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsSearchIndex
    {
        public int IdProduct { get; set; }
        public int IdWord { get; set; }
        public short Weight { get; set; }
    }
}
