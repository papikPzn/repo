﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsLayeredFilterShop
    {
        public int IdLayeredFilter { get; set; }
        public int IdShop { get; set; }
    }
}
