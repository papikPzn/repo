﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsStoreShop
    {
        public int IdStore { get; set; }
        public int IdShop { get; set; }
    }
}
