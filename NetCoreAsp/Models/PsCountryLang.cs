﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCountryLang
    {
        public int IdCountry { get; set; }
        public int IdLang { get; set; }
        public string Name { get; set; }
    }
}
