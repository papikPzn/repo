﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCartCartRule
    {
        public int IdCart { get; set; }
        public int IdCartRule { get; set; }
    }
}
