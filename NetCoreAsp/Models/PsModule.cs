﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsModule
    {
        public int IdModule { get; set; }
        public sbyte Active { get; set; }
        public string Name { get; set; }
        public string Version { get; set; }
    }
}
