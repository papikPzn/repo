﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCustomerGroup
    {
        public int IdCustomer { get; set; }
        public int IdGroup { get; set; }
    }
}
