﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsHook
    {
        public int IdHook { get; set; }
        public string Description { get; set; }
        public sbyte LiveEdit { get; set; }
        public string Name { get; set; }
        public sbyte Position { get; set; }
        public string Title { get; set; }
    }
}
