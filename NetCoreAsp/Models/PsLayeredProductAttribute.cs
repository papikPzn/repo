﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsLayeredProductAttribute
    {
        public int IdAttribute { get; set; }
        public int IdProduct { get; set; }
        public int IdShop { get; set; }
        public int IdAttributeGroup { get; set; }
    }
}
