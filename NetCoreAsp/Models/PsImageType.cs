﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsImageType
    {
        public int IdImageType { get; set; }
        public sbyte Categories { get; set; }
        public int Height { get; set; }
        public sbyte Manufacturers { get; set; }
        public string Name { get; set; }
        public sbyte Products { get; set; }
        public sbyte Scenes { get; set; }
        public sbyte Stores { get; set; }
        public sbyte Suppliers { get; set; }
        public int Width { get; set; }
    }
}
