﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsInfo
    {
        public int IdInfo { get; set; }
        public int? IdShop { get; set; }
    }
}
