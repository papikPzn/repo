﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsConfigurationKpiLang
    {
        public int IdConfigurationKpi { get; set; }
        public int IdLang { get; set; }
        public DateTime? DateUpd { get; set; }
        public string Value { get; set; }
    }
}
