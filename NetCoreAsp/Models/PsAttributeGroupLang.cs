﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsAttributeGroupLang
    {
        public int IdAttributeGroup { get; set; }
        public int IdLang { get; set; }
        public string Name { get; set; }
        public string PublicName { get; set; }
    }
}
