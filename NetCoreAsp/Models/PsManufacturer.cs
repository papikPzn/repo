﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsManufacturer
    {
        public int IdManufacturer { get; set; }
        public sbyte Active { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public string Name { get; set; }
    }
}
