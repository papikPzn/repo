﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsConditionAdvice
    {
        public int IdCondition { get; set; }
        public int IdAdvice { get; set; }
        public sbyte Display { get; set; }
    }
}
