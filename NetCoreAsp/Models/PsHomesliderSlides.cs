﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsHomesliderSlides
    {
        public int IdHomesliderSlides { get; set; }
        public sbyte Active { get; set; }
        public int Position { get; set; }
    }
}
