﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsConfiguration
    {
        public int IdConfiguration { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public int? IdShop { get; set; }
        public int? IdShopGroup { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
