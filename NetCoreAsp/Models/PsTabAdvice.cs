﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsTabAdvice
    {
        public int IdTab { get; set; }
        public int IdAdvice { get; set; }
    }
}
