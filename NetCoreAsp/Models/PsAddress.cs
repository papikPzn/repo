﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsAddress
    {
        public int IdAddress { get; set; }
        public sbyte Active { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Alias { get; set; }
        public string City { get; set; }
        public string Company { get; set; }
        public DateTime DateAdd { get; set; }
        public DateTime DateUpd { get; set; }
        public sbyte Deleted { get; set; }
        public string Dni { get; set; }
        public string Firstname { get; set; }
        public int IdCountry { get; set; }
        public int IdCustomer { get; set; }
        public int IdManufacturer { get; set; }
        public int? IdState { get; set; }
        public int IdSupplier { get; set; }
        public int IdWarehouse { get; set; }
        public string Lastname { get; set; }
        public string Other { get; set; }
        public string Phone { get; set; }
        public string PhoneMobile { get; set; }
        public string Postcode { get; set; }
        public string VatNumber { get; set; }
    }
}
