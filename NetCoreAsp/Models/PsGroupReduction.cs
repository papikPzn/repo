﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsGroupReduction
    {
        public int IdGroupReduction { get; set; }
        public int IdCategory { get; set; }
        public int IdGroup { get; set; }
        public decimal Reduction { get; set; }
    }
}
