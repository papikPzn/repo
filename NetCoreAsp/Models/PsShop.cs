﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsShop
    {
        public int IdShop { get; set; }
        public sbyte Active { get; set; }
        public sbyte Deleted { get; set; }
        public int IdCategory { get; set; }
        public int IdShopGroup { get; set; }
        public int IdTheme { get; set; }
        public string Name { get; set; }
    }
}
