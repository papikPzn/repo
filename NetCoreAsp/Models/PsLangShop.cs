﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsLangShop
    {
        public int IdLang { get; set; }
        public int IdShop { get; set; }
    }
}
