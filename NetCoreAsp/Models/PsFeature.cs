﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsFeature
    {
        public int IdFeature { get; set; }
        public int Position { get; set; }
    }
}
