﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsModuleGroup
    {
        public int IdModule { get; set; }
        public int IdShop { get; set; }
        public int IdGroup { get; set; }
    }
}
