﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsAttribute
    {
        public int IdAttribute { get; set; }
        public string Color { get; set; }
        public int IdAttributeGroup { get; set; }
        public int Position { get; set; }
    }
}
