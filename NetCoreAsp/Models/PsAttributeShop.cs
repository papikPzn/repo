﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsAttributeShop
    {
        public int IdAttribute { get; set; }
        public int IdShop { get; set; }
    }
}
