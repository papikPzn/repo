﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsMail
    {
        public int IdMail { get; set; }
        public DateTime DateAdd { get; set; }
        public int IdLang { get; set; }
        public string Recipient { get; set; }
        public string Subject { get; set; }
        public string Template { get; set; }
    }
}
