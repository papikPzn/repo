﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsFeatureLang
    {
        public int IdFeature { get; set; }
        public int IdLang { get; set; }
        public string Name { get; set; }
    }
}
