﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsSceneLang
    {
        public int IdScene { get; set; }
        public int IdLang { get; set; }
        public string Name { get; set; }
    }
}
