﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace NetCoreAsp.Models
{
    public partial class prestashopContext : DbContext
    {
        public virtual DbSet<PsAccess> PsAccess { get; set; }
        public virtual DbSet<PsAddress> PsAddress { get; set; }
        public virtual DbSet<PsAddressFormat> PsAddressFormat { get; set; }
        public virtual DbSet<PsAdvice> PsAdvice { get; set; }
        public virtual DbSet<PsAdviceLang> PsAdviceLang { get; set; }
        public virtual DbSet<PsAlias> PsAlias { get; set; }
        public virtual DbSet<PsAttachment> PsAttachment { get; set; }
        public virtual DbSet<PsAttachmentLang> PsAttachmentLang { get; set; }
        public virtual DbSet<PsAttribute> PsAttribute { get; set; }
        public virtual DbSet<PsAttributeGroup> PsAttributeGroup { get; set; }
        public virtual DbSet<PsAttributeGroupLang> PsAttributeGroupLang { get; set; }
        public virtual DbSet<PsAttributeGroupShop> PsAttributeGroupShop { get; set; }
        public virtual DbSet<PsAttributeImpact> PsAttributeImpact { get; set; }
        public virtual DbSet<PsAttributeLang> PsAttributeLang { get; set; }
        public virtual DbSet<PsAttributeShop> PsAttributeShop { get; set; }
        public virtual DbSet<PsBadge> PsBadge { get; set; }
        public virtual DbSet<PsBadgeLang> PsBadgeLang { get; set; }
        public virtual DbSet<PsCarrier> PsCarrier { get; set; }
        public virtual DbSet<PsCarrierGroup> PsCarrierGroup { get; set; }
        public virtual DbSet<PsCarrierLang> PsCarrierLang { get; set; }
        public virtual DbSet<PsCarrierShop> PsCarrierShop { get; set; }
        public virtual DbSet<PsCarrierTaxRulesGroupShop> PsCarrierTaxRulesGroupShop { get; set; }
        public virtual DbSet<PsCarrierZone> PsCarrierZone { get; set; }
        public virtual DbSet<PsCart> PsCart { get; set; }
        public virtual DbSet<PsCartCartRule> PsCartCartRule { get; set; }
        public virtual DbSet<PsCartProduct> PsCartProduct { get; set; }
        public virtual DbSet<PsCartRule> PsCartRule { get; set; }
        public virtual DbSet<PsCartRuleCarrier> PsCartRuleCarrier { get; set; }
        public virtual DbSet<PsCartRuleCombination> PsCartRuleCombination { get; set; }
        public virtual DbSet<PsCartRuleCountry> PsCartRuleCountry { get; set; }
        public virtual DbSet<PsCartRuleGroup> PsCartRuleGroup { get; set; }
        public virtual DbSet<PsCartRuleLang> PsCartRuleLang { get; set; }
        public virtual DbSet<PsCartRuleProductRule> PsCartRuleProductRule { get; set; }
        public virtual DbSet<PsCartRuleProductRuleGroup> PsCartRuleProductRuleGroup { get; set; }
        public virtual DbSet<PsCartRuleProductRuleValue> PsCartRuleProductRuleValue { get; set; }
        public virtual DbSet<PsCartRuleShop> PsCartRuleShop { get; set; }
        public virtual DbSet<PsCategory> PsCategory { get; set; }
        public virtual DbSet<PsCategoryGroup> PsCategoryGroup { get; set; }
        public virtual DbSet<PsCategoryLang> PsCategoryLang { get; set; }
        public virtual DbSet<PsCategoryProduct> PsCategoryProduct { get; set; }
        public virtual DbSet<PsCategoryShop> PsCategoryShop { get; set; }
        public virtual DbSet<PsCms> PsCms { get; set; }
        public virtual DbSet<PsCmsBlock> PsCmsBlock { get; set; }
        public virtual DbSet<PsCmsBlockLang> PsCmsBlockLang { get; set; }
        public virtual DbSet<PsCmsBlockPage> PsCmsBlockPage { get; set; }
        public virtual DbSet<PsCmsBlockShop> PsCmsBlockShop { get; set; }
        public virtual DbSet<PsCmsCategory> PsCmsCategory { get; set; }
        public virtual DbSet<PsCmsCategoryLang> PsCmsCategoryLang { get; set; }
        public virtual DbSet<PsCmsCategoryShop> PsCmsCategoryShop { get; set; }
        public virtual DbSet<PsCmsLang> PsCmsLang { get; set; }
        public virtual DbSet<PsCmsRole> PsCmsRole { get; set; }
        public virtual DbSet<PsCmsRoleLang> PsCmsRoleLang { get; set; }
        public virtual DbSet<PsCmsShop> PsCmsShop { get; set; }
        public virtual DbSet<PsCompare> PsCompare { get; set; }
        public virtual DbSet<PsCompareProduct> PsCompareProduct { get; set; }
        public virtual DbSet<PsCondition> PsCondition { get; set; }
        public virtual DbSet<PsConditionAdvice> PsConditionAdvice { get; set; }
        public virtual DbSet<PsConditionBadge> PsConditionBadge { get; set; }
        public virtual DbSet<PsConfiguration> PsConfiguration { get; set; }
        public virtual DbSet<PsConfigurationKpi> PsConfigurationKpi { get; set; }
        public virtual DbSet<PsConfigurationKpiLang> PsConfigurationKpiLang { get; set; }
        public virtual DbSet<PsConfigurationLang> PsConfigurationLang { get; set; }
        public virtual DbSet<PsConnections> PsConnections { get; set; }
        public virtual DbSet<PsConnectionsPage> PsConnectionsPage { get; set; }
        public virtual DbSet<PsConnectionsSource> PsConnectionsSource { get; set; }
        public virtual DbSet<PsContact> PsContact { get; set; }
        public virtual DbSet<PsContactLang> PsContactLang { get; set; }
        public virtual DbSet<PsContactShop> PsContactShop { get; set; }
        public virtual DbSet<PsCountry> PsCountry { get; set; }
        public virtual DbSet<PsCountryLang> PsCountryLang { get; set; }
        public virtual DbSet<PsCountryShop> PsCountryShop { get; set; }
        public virtual DbSet<PsCronjobs> PsCronjobs { get; set; }
        public virtual DbSet<PsCurrency> PsCurrency { get; set; }
        public virtual DbSet<PsCurrencyShop> PsCurrencyShop { get; set; }
        public virtual DbSet<PsCustomer> PsCustomer { get; set; }
        public virtual DbSet<PsCustomerGroup> PsCustomerGroup { get; set; }
        public virtual DbSet<PsCustomerMessage> PsCustomerMessage { get; set; }
        public virtual DbSet<PsCustomerThread> PsCustomerThread { get; set; }
        public virtual DbSet<PsCustomization> PsCustomization { get; set; }
        public virtual DbSet<PsCustomizationField> PsCustomizationField { get; set; }
        public virtual DbSet<PsCustomizationFieldLang> PsCustomizationFieldLang { get; set; }
        public virtual DbSet<PsCustomizedData> PsCustomizedData { get; set; }
        public virtual DbSet<PsDateRange> PsDateRange { get; set; }
        public virtual DbSet<PsDelivery> PsDelivery { get; set; }
        public virtual DbSet<PsEmployee> PsEmployee { get; set; }
        public virtual DbSet<PsEmployeeShop> PsEmployeeShop { get; set; }
        public virtual DbSet<PsFeature> PsFeature { get; set; }
        public virtual DbSet<PsFeatureLang> PsFeatureLang { get; set; }
        public virtual DbSet<PsFeatureProduct> PsFeatureProduct { get; set; }
        public virtual DbSet<PsFeatureShop> PsFeatureShop { get; set; }
        public virtual DbSet<PsFeatureValue> PsFeatureValue { get; set; }
        public virtual DbSet<PsFeatureValueLang> PsFeatureValueLang { get; set; }
        public virtual DbSet<PsGender> PsGender { get; set; }
        public virtual DbSet<PsGenderLang> PsGenderLang { get; set; }
        public virtual DbSet<PsGroup> PsGroup { get; set; }
        public virtual DbSet<PsGroupLang> PsGroupLang { get; set; }
        public virtual DbSet<PsGroupReduction> PsGroupReduction { get; set; }
        public virtual DbSet<PsGroupShop> PsGroupShop { get; set; }
        public virtual DbSet<PsGuest> PsGuest { get; set; }
        public virtual DbSet<PsHomeslider> PsHomeslider { get; set; }
        public virtual DbSet<PsHomesliderSlides> PsHomesliderSlides { get; set; }
        public virtual DbSet<PsHomesliderSlidesLang> PsHomesliderSlidesLang { get; set; }
        public virtual DbSet<PsHook> PsHook { get; set; }
        public virtual DbSet<PsHookAlias> PsHookAlias { get; set; }
        public virtual DbSet<PsHookModule> PsHookModule { get; set; }
        public virtual DbSet<PsHookModuleExceptions> PsHookModuleExceptions { get; set; }
        public virtual DbSet<PsImage> PsImage { get; set; }
        public virtual DbSet<PsImageLang> PsImageLang { get; set; }
        public virtual DbSet<PsImageShop> PsImageShop { get; set; }
        public virtual DbSet<PsImageType> PsImageType { get; set; }
        public virtual DbSet<PsImportMatch> PsImportMatch { get; set; }
        public virtual DbSet<PsInfo> PsInfo { get; set; }
        public virtual DbSet<PsInfoLang> PsInfoLang { get; set; }
        public virtual DbSet<PsLang> PsLang { get; set; }
        public virtual DbSet<PsLangShop> PsLangShop { get; set; }
        public virtual DbSet<PsLayeredCategory> PsLayeredCategory { get; set; }
        public virtual DbSet<PsLayeredFilter> PsLayeredFilter { get; set; }
        public virtual DbSet<PsLayeredFilterShop> PsLayeredFilterShop { get; set; }
        public virtual DbSet<PsLayeredFriendlyUrl> PsLayeredFriendlyUrl { get; set; }
        public virtual DbSet<PsLayeredIndexableAttributeGroup> PsLayeredIndexableAttributeGroup { get; set; }
        public virtual DbSet<PsLayeredIndexableAttributeGroupLangValue> PsLayeredIndexableAttributeGroupLangValue { get; set; }
        public virtual DbSet<PsLayeredIndexableAttributeLangValue> PsLayeredIndexableAttributeLangValue { get; set; }
        public virtual DbSet<PsLayeredIndexableFeature> PsLayeredIndexableFeature { get; set; }
        public virtual DbSet<PsLayeredIndexableFeatureLangValue> PsLayeredIndexableFeatureLangValue { get; set; }
        public virtual DbSet<PsLayeredIndexableFeatureValueLangValue> PsLayeredIndexableFeatureValueLangValue { get; set; }
        public virtual DbSet<PsLayeredPriceIndex> PsLayeredPriceIndex { get; set; }
        public virtual DbSet<PsLayeredProductAttribute> PsLayeredProductAttribute { get; set; }
        public virtual DbSet<PsLinksmenutop> PsLinksmenutop { get; set; }
        public virtual DbSet<PsLog> PsLog { get; set; }
        public virtual DbSet<PsMail> PsMail { get; set; }
        public virtual DbSet<PsManufacturer> PsManufacturer { get; set; }
        public virtual DbSet<PsManufacturerLang> PsManufacturerLang { get; set; }
        public virtual DbSet<PsManufacturerShop> PsManufacturerShop { get; set; }
        public virtual DbSet<PsMemcachedServers> PsMemcachedServers { get; set; }
        public virtual DbSet<PsMessage> PsMessage { get; set; }
        public virtual DbSet<PsMessageReaded> PsMessageReaded { get; set; }
        public virtual DbSet<PsMeta> PsMeta { get; set; }
        public virtual DbSet<PsMetaLang> PsMetaLang { get; set; }
        public virtual DbSet<PsModule> PsModule { get; set; }
        public virtual DbSet<PsModuleAccess> PsModuleAccess { get; set; }
        public virtual DbSet<PsModuleCountry> PsModuleCountry { get; set; }
        public virtual DbSet<PsModuleCurrency> PsModuleCurrency { get; set; }
        public virtual DbSet<PsModuleGroup> PsModuleGroup { get; set; }
        public virtual DbSet<PsModulePreference> PsModulePreference { get; set; }
        public virtual DbSet<PsModuleShop> PsModuleShop { get; set; }
        public virtual DbSet<PsModulesPerfs> PsModulesPerfs { get; set; }
        public virtual DbSet<PsNewsletter> PsNewsletter { get; set; }
        public virtual DbSet<PsOperatingSystem> PsOperatingSystem { get; set; }
        public virtual DbSet<PsOrderCarrier> PsOrderCarrier { get; set; }
        public virtual DbSet<PsOrderCartRule> PsOrderCartRule { get; set; }
        public virtual DbSet<PsOrderDetail> PsOrderDetail { get; set; }
        public virtual DbSet<PsOrderHistory> PsOrderHistory { get; set; }
        public virtual DbSet<PsOrderInvoice> PsOrderInvoice { get; set; }
        public virtual DbSet<PsOrderInvoicePayment> PsOrderInvoicePayment { get; set; }
        public virtual DbSet<PsOrderMessage> PsOrderMessage { get; set; }
        public virtual DbSet<PsOrderMessageLang> PsOrderMessageLang { get; set; }
        public virtual DbSet<PsOrderPayment> PsOrderPayment { get; set; }
        public virtual DbSet<PsOrderReturn> PsOrderReturn { get; set; }
        public virtual DbSet<PsOrderReturnDetail> PsOrderReturnDetail { get; set; }
        public virtual DbSet<PsOrderReturnState> PsOrderReturnState { get; set; }
        public virtual DbSet<PsOrderReturnStateLang> PsOrderReturnStateLang { get; set; }
        public virtual DbSet<PsOrderSlip> PsOrderSlip { get; set; }
        public virtual DbSet<PsOrderSlipDetail> PsOrderSlipDetail { get; set; }
        public virtual DbSet<PsOrderState> PsOrderState { get; set; }
        public virtual DbSet<PsOrderStateLang> PsOrderStateLang { get; set; }
        public virtual DbSet<PsOrders> PsOrders { get; set; }
        public virtual DbSet<PsPack> PsPack { get; set; }
        public virtual DbSet<PsPage> PsPage { get; set; }
        public virtual DbSet<PsPageType> PsPageType { get; set; }
        public virtual DbSet<PsPageViewed> PsPageViewed { get; set; }
        public virtual DbSet<PsPagenotfound> PsPagenotfound { get; set; }
        public virtual DbSet<PsProduct> PsProduct { get; set; }
        public virtual DbSet<PsProductAttachment> PsProductAttachment { get; set; }
        public virtual DbSet<PsProductAttribute> PsProductAttribute { get; set; }
        public virtual DbSet<PsProductAttributeCombination> PsProductAttributeCombination { get; set; }
        public virtual DbSet<PsProductAttributeImage> PsProductAttributeImage { get; set; }
        public virtual DbSet<PsProductAttributeShop> PsProductAttributeShop { get; set; }
        public virtual DbSet<PsProductCarrier> PsProductCarrier { get; set; }
        public virtual DbSet<PsProductCountryTax> PsProductCountryTax { get; set; }
        public virtual DbSet<PsProductDownload> PsProductDownload { get; set; }
        public virtual DbSet<PsProductGroupReductionCache> PsProductGroupReductionCache { get; set; }
        public virtual DbSet<PsProductLang> PsProductLang { get; set; }
        public virtual DbSet<PsProductSale> PsProductSale { get; set; }
        public virtual DbSet<PsProductShop> PsProductShop { get; set; }
        public virtual DbSet<PsProductSupplier> PsProductSupplier { get; set; }
        public virtual DbSet<PsProductTag> PsProductTag { get; set; }
        public virtual DbSet<PsProfile> PsProfile { get; set; }
        public virtual DbSet<PsProfileLang> PsProfileLang { get; set; }
        public virtual DbSet<PsQuickAccess> PsQuickAccess { get; set; }
        public virtual DbSet<PsQuickAccessLang> PsQuickAccessLang { get; set; }
        public virtual DbSet<PsRangePrice> PsRangePrice { get; set; }
        public virtual DbSet<PsRangeWeight> PsRangeWeight { get; set; }
        public virtual DbSet<PsReferrer> PsReferrer { get; set; }
        public virtual DbSet<PsReferrerCache> PsReferrerCache { get; set; }
        public virtual DbSet<PsReferrerShop> PsReferrerShop { get; set; }
        public virtual DbSet<PsRequestSql> PsRequestSql { get; set; }
        public virtual DbSet<PsRequiredField> PsRequiredField { get; set; }
        public virtual DbSet<PsRisk> PsRisk { get; set; }
        public virtual DbSet<PsRiskLang> PsRiskLang { get; set; }
        public virtual DbSet<PsScene> PsScene { get; set; }
        public virtual DbSet<PsSceneCategory> PsSceneCategory { get; set; }
        public virtual DbSet<PsSceneLang> PsSceneLang { get; set; }
        public virtual DbSet<PsSceneProducts> PsSceneProducts { get; set; }
        public virtual DbSet<PsSceneShop> PsSceneShop { get; set; }
        public virtual DbSet<PsSearchEngine> PsSearchEngine { get; set; }
        public virtual DbSet<PsSearchIndex> PsSearchIndex { get; set; }
        public virtual DbSet<PsSearchWord> PsSearchWord { get; set; }
        public virtual DbSet<PsSekeyword> PsSekeyword { get; set; }
        public virtual DbSet<PsShop> PsShop { get; set; }
        public virtual DbSet<PsShopGroup> PsShopGroup { get; set; }
        public virtual DbSet<PsShopUrl> PsShopUrl { get; set; }
        public virtual DbSet<PsSmartyCache> PsSmartyCache { get; set; }
        public virtual DbSet<PsSmartyLazyCache> PsSmartyLazyCache { get; set; }
        public virtual DbSet<PsSpecificPrice> PsSpecificPrice { get; set; }
        public virtual DbSet<PsSpecificPricePriority> PsSpecificPricePriority { get; set; }
        public virtual DbSet<PsSpecificPriceRule> PsSpecificPriceRule { get; set; }
        public virtual DbSet<PsSpecificPriceRuleCondition> PsSpecificPriceRuleCondition { get; set; }
        public virtual DbSet<PsSpecificPriceRuleConditionGroup> PsSpecificPriceRuleConditionGroup { get; set; }
        public virtual DbSet<PsState> PsState { get; set; }
        public virtual DbSet<PsStatssearch> PsStatssearch { get; set; }
        public virtual DbSet<PsStock> PsStock { get; set; }
        public virtual DbSet<PsStockAvailable> PsStockAvailable { get; set; }
        public virtual DbSet<PsStockMvt> PsStockMvt { get; set; }
        public virtual DbSet<PsStockMvtReason> PsStockMvtReason { get; set; }
        public virtual DbSet<PsStockMvtReasonLang> PsStockMvtReasonLang { get; set; }
        public virtual DbSet<PsStore> PsStore { get; set; }
        public virtual DbSet<PsStoreShop> PsStoreShop { get; set; }
        public virtual DbSet<PsSupplier> PsSupplier { get; set; }
        public virtual DbSet<PsSupplierLang> PsSupplierLang { get; set; }
        public virtual DbSet<PsSupplierShop> PsSupplierShop { get; set; }
        public virtual DbSet<PsSupplyOrder> PsSupplyOrder { get; set; }
        public virtual DbSet<PsSupplyOrderDetail> PsSupplyOrderDetail { get; set; }
        public virtual DbSet<PsSupplyOrderHistory> PsSupplyOrderHistory { get; set; }
        public virtual DbSet<PsSupplyOrderReceiptHistory> PsSupplyOrderReceiptHistory { get; set; }
        public virtual DbSet<PsSupplyOrderState> PsSupplyOrderState { get; set; }
        public virtual DbSet<PsSupplyOrderStateLang> PsSupplyOrderStateLang { get; set; }
        public virtual DbSet<PsTab> PsTab { get; set; }
        public virtual DbSet<PsTabAdvice> PsTabAdvice { get; set; }
        public virtual DbSet<PsTabLang> PsTabLang { get; set; }
        public virtual DbSet<PsTabModulePreference> PsTabModulePreference { get; set; }
        public virtual DbSet<PsTag> PsTag { get; set; }
        public virtual DbSet<PsTagCount> PsTagCount { get; set; }
        public virtual DbSet<PsTax> PsTax { get; set; }
        public virtual DbSet<PsTaxLang> PsTaxLang { get; set; }
        public virtual DbSet<PsTaxRule> PsTaxRule { get; set; }
        public virtual DbSet<PsTaxRulesGroup> PsTaxRulesGroup { get; set; }
        public virtual DbSet<PsTaxRulesGroupShop> PsTaxRulesGroupShop { get; set; }
        public virtual DbSet<PsTheme> PsTheme { get; set; }
        public virtual DbSet<PsThemeMeta> PsThemeMeta { get; set; }
        public virtual DbSet<PsThemeSpecific> PsThemeSpecific { get; set; }
        public virtual DbSet<PsThemeconfigurator> PsThemeconfigurator { get; set; }
        public virtual DbSet<PsTimezone> PsTimezone { get; set; }
        public virtual DbSet<PsWarehouse> PsWarehouse { get; set; }
        public virtual DbSet<PsWarehouseCarrier> PsWarehouseCarrier { get; set; }
        public virtual DbSet<PsWarehouseProductLocation> PsWarehouseProductLocation { get; set; }
        public virtual DbSet<PsWarehouseShop> PsWarehouseShop { get; set; }
        public virtual DbSet<PsWebBrowser> PsWebBrowser { get; set; }
        public virtual DbSet<PsWebserviceAccount> PsWebserviceAccount { get; set; }
        public virtual DbSet<PsWebserviceAccountShop> PsWebserviceAccountShop { get; set; }
        public virtual DbSet<PsWebservicePermission> PsWebservicePermission { get; set; }
        public virtual DbSet<PsZone> PsZone { get; set; }
        public virtual DbSet<PsZoneShop> PsZoneShop { get; set; }

        // Unable to generate entity type for table 'ps_accessory'. Please see the warning messages.
        // Unable to generate entity type for table 'ps_customer_message_sync_imap'. Please see the warning messages.
        // Unable to generate entity type for table 'ps_linksmenutop_lang'. Please see the warning messages.
        // Unable to generate entity type for table 'ps_order_detail_tax'. Please see the warning messages.
        // Unable to generate entity type for table 'ps_order_invoice_tax'. Please see the warning messages.
        // Unable to generate entity type for table 'ps_order_slip_detail_tax'. Please see the warning messages.
        // Unable to generate entity type for table 'ps_smarty_last_flush'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
            optionsBuilder.UseMySql(@"server=localhost;user=root;password=123456;port=3306;database=prestashop");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PsAccess>(entity =>
            {
                entity.HasKey(e => new { e.IdProfile, e.IdTab })
                    .HasName("PK_ps_access");

                entity.ToTable("ps_access");

                entity.Property(e => e.IdProfile)
                    .HasColumnName("id_profile")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdTab)
                    .HasColumnName("id_tab")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Add)
                    .HasColumnName("add")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Delete)
                    .HasColumnName("delete")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Edit)
                    .HasColumnName("edit")
                    .HasColumnType("int(11)");

                entity.Property(e => e.View)
                    .HasColumnName("view")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<PsAddress>(entity =>
            {
                entity.HasKey(e => e.IdAddress)
                    .HasName("PK_ps_address");

                entity.ToTable("ps_address");

                entity.HasIndex(e => e.IdCountry)
                    .HasName("id_country");

                entity.HasIndex(e => e.IdCustomer)
                    .HasName("address_customer");

                entity.HasIndex(e => e.IdManufacturer)
                    .HasName("id_manufacturer");

                entity.HasIndex(e => e.IdState)
                    .HasName("id_state");

                entity.HasIndex(e => e.IdSupplier)
                    .HasName("id_supplier");

                entity.HasIndex(e => e.IdWarehouse)
                    .HasName("id_warehouse");

                entity.Property(e => e.IdAddress)
                    .HasColumnName("id_address")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Address1)
                    .IsRequired()
                    .HasColumnName("address1")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.Address2)
                    .HasColumnName("address2")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.Alias)
                    .IsRequired()
                    .HasColumnName("alias")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasColumnName("city")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deleted)
                    .HasColumnName("deleted")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Dni)
                    .HasColumnName("dni")
                    .HasColumnType("varchar(16)");

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasColumnName("firstname")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.IdCountry)
                    .HasColumnName("id_country")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCustomer)
                    .HasColumnName("id_customer")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdManufacturer)
                    .HasColumnName("id_manufacturer")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdState)
                    .HasColumnName("id_state")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdSupplier)
                    .HasColumnName("id_supplier")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdWarehouse)
                    .HasColumnName("id_warehouse")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasColumnName("lastname")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.Other)
                    .HasColumnName("other")
                    .HasColumnType("text");

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.PhoneMobile)
                    .HasColumnName("phone_mobile")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.Postcode)
                    .HasColumnName("postcode")
                    .HasColumnType("varchar(12)");

                entity.Property(e => e.VatNumber)
                    .HasColumnName("vat_number")
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<PsAddressFormat>(entity =>
            {
                entity.HasKey(e => e.IdCountry)
                    .HasName("PK_ps_address_format");

                entity.ToTable("ps_address_format");

                entity.Property(e => e.IdCountry)
                    .HasColumnName("id_country")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Format)
                    .IsRequired()
                    .HasColumnName("format")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<PsAdvice>(entity =>
            {
                entity.HasKey(e => e.IdAdvice)
                    .HasName("PK_ps_advice");

                entity.ToTable("ps_advice");

                entity.Property(e => e.IdAdvice)
                    .HasColumnName("id_advice")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Hide)
                    .HasColumnName("hide")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdPsAdvice)
                    .HasColumnName("id_ps_advice")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdTab)
                    .HasColumnName("id_tab")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdsTab)
                    .HasColumnName("ids_tab")
                    .HasColumnType("text");

                entity.Property(e => e.Selector)
                    .HasColumnName("selector")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.StartDay)
                    .HasColumnName("start_day")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.StopDay)
                    .HasColumnName("stop_day")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Validated)
                    .HasColumnName("validated")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Weight)
                    .HasColumnName("weight")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("1");
            });

            modelBuilder.Entity<PsAdviceLang>(entity =>
            {
                entity.HasKey(e => new { e.IdAdvice, e.IdLang })
                    .HasName("PK_ps_advice_lang");

                entity.ToTable("ps_advice_lang");

                entity.Property(e => e.IdAdvice)
                    .HasColumnName("id_advice")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Html)
                    .HasColumnName("html")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<PsAlias>(entity =>
            {
                entity.HasKey(e => e.IdAlias)
                    .HasName("PK_ps_alias");

                entity.ToTable("ps_alias");

                entity.HasIndex(e => e.Alias)
                    .HasName("alias")
                    .IsUnique();

                entity.Property(e => e.IdAlias)
                    .HasColumnName("id_alias")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Alias)
                    .IsRequired()
                    .HasColumnName("alias")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Search)
                    .IsRequired()
                    .HasColumnName("search")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<PsAttachment>(entity =>
            {
                entity.HasKey(e => e.IdAttachment)
                    .HasName("PK_ps_attachment");

                entity.ToTable("ps_attachment");

                entity.Property(e => e.IdAttachment)
                    .HasColumnName("id_attachment")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.File)
                    .IsRequired()
                    .HasColumnName("file")
                    .HasColumnType("varchar(40)");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasColumnName("file_name")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.FileSize)
                    .HasColumnName("file_size")
                    .HasColumnType("bigint(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Mime)
                    .IsRequired()
                    .HasColumnName("mime")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsAttachmentLang>(entity =>
            {
                entity.HasKey(e => new { e.IdAttachment, e.IdLang })
                    .HasName("PK_ps_attachment_lang");

                entity.ToTable("ps_attachment_lang");

                entity.Property(e => e.IdAttachment)
                    .HasColumnName("id_attachment")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<PsAttribute>(entity =>
            {
                entity.HasKey(e => e.IdAttribute)
                    .HasName("PK_ps_attribute");

                entity.ToTable("ps_attribute");

                entity.HasIndex(e => e.IdAttributeGroup)
                    .HasName("attribute_group");

                entity.Property(e => e.IdAttribute)
                    .HasColumnName("id_attribute")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.IdAttributeGroup)
                    .HasColumnName("id_attribute_group")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsAttributeGroup>(entity =>
            {
                entity.HasKey(e => e.IdAttributeGroup)
                    .HasName("PK_ps_attribute_group");

                entity.ToTable("ps_attribute_group");

                entity.Property(e => e.IdAttributeGroup)
                    .HasColumnName("id_attribute_group")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IsColorGroup)
                    .HasColumnName("is_color_group")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsAttributeGroupLang>(entity =>
            {
                entity.HasKey(e => new { e.IdAttributeGroup, e.IdLang })
                    .HasName("PK_ps_attribute_group_lang");

                entity.ToTable("ps_attribute_group_lang");

                entity.Property(e => e.IdAttributeGroup)
                    .HasColumnName("id_attribute_group")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.PublicName)
                    .IsRequired()
                    .HasColumnName("public_name")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsAttributeGroupShop>(entity =>
            {
                entity.HasKey(e => new { e.IdAttributeGroup, e.IdShop })
                    .HasName("PK_ps_attribute_group_shop");

                entity.ToTable("ps_attribute_group_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdAttributeGroup)
                    .HasColumnName("id_attribute_group")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsAttributeImpact>(entity =>
            {
                entity.HasKey(e => e.IdAttributeImpact)
                    .HasName("PK_ps_attribute_impact");

                entity.ToTable("ps_attribute_impact");

                entity.HasIndex(e => new { e.IdProduct, e.IdAttribute })
                    .HasName("id_product")
                    .IsUnique();

                entity.Property(e => e.IdAttributeImpact)
                    .HasColumnName("id_attribute_impact")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdAttribute)
                    .HasColumnName("id_attribute")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(17,2)");

                entity.Property(e => e.Weight)
                    .HasColumnName("weight")
                    .HasColumnType("decimal(20,6)");
            });

            modelBuilder.Entity<PsAttributeLang>(entity =>
            {
                entity.HasKey(e => new { e.IdAttribute, e.IdLang })
                    .HasName("PK_ps_attribute_lang");

                entity.ToTable("ps_attribute_lang");

                entity.HasIndex(e => new { e.IdLang, e.Name })
                    .HasName("id_lang");

                entity.Property(e => e.IdAttribute)
                    .HasColumnName("id_attribute")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsAttributeShop>(entity =>
            {
                entity.HasKey(e => new { e.IdAttribute, e.IdShop })
                    .HasName("PK_ps_attribute_shop");

                entity.ToTable("ps_attribute_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdAttribute)
                    .HasColumnName("id_attribute")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsBadge>(entity =>
            {
                entity.HasKey(e => e.IdBadge)
                    .HasName("PK_ps_badge");

                entity.ToTable("ps_badge");

                entity.Property(e => e.IdBadge)
                    .HasColumnName("id_badge")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Awb)
                    .HasColumnName("awb")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.GroupPosition)
                    .HasColumnName("group_position")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdGroup)
                    .HasColumnName("id_group")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdPsBadge)
                    .HasColumnName("id_ps_badge")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Scoring)
                    .HasColumnName("scoring")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.Validated)
                    .HasColumnName("validated")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsBadgeLang>(entity =>
            {
                entity.HasKey(e => new { e.IdBadge, e.IdLang })
                    .HasName("PK_ps_badge_lang");

                entity.ToTable("ps_badge_lang");

                entity.Property(e => e.IdBadge)
                    .HasColumnName("id_badge")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.GroupName)
                    .HasColumnName("group_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsCarrier>(entity =>
            {
                entity.HasKey(e => e.IdCarrier)
                    .HasName("PK_ps_carrier");

                entity.ToTable("ps_carrier");

                entity.HasIndex(e => e.IdTaxRulesGroup)
                    .HasName("id_tax_rules_group");

                entity.HasIndex(e => new { e.Deleted, e.Active })
                    .HasName("deleted");

                entity.HasIndex(e => new { e.IdReference, e.Deleted, e.Active })
                    .HasName("reference");

                entity.Property(e => e.IdCarrier)
                    .HasColumnName("id_carrier")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Deleted)
                    .HasColumnName("deleted")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ExternalModuleName)
                    .HasColumnName("external_module_name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Grade)
                    .HasColumnName("grade")
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdReference)
                    .HasColumnName("id_reference")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdTaxRulesGroup)
                    .HasColumnName("id_tax_rules_group")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IsFree)
                    .HasColumnName("is_free")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IsModule)
                    .HasColumnName("is_module")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.MaxDepth)
                    .HasColumnName("max_depth")
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.MaxHeight)
                    .HasColumnName("max_height")
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.MaxWeight)
                    .HasColumnName("max_weight")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.MaxWidth)
                    .HasColumnName("max_width")
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.NeedRange)
                    .HasColumnName("need_range")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.RangeBehavior)
                    .HasColumnName("range_behavior")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ShippingExternal)
                    .HasColumnName("shipping_external")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ShippingHandling)
                    .HasColumnName("shipping_handling")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.ShippingMethod)
                    .HasColumnName("shipping_method")
                    .HasColumnType("int(2)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<PsCarrierGroup>(entity =>
            {
                entity.HasKey(e => new { e.IdCarrier, e.IdGroup })
                    .HasName("PK_ps_carrier_group");

                entity.ToTable("ps_carrier_group");

                entity.Property(e => e.IdCarrier)
                    .HasColumnName("id_carrier")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdGroup)
                    .HasColumnName("id_group")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsCarrierLang>(entity =>
            {
                entity.HasKey(e => new { e.IdCarrier, e.IdShop, e.IdLang })
                    .HasName("PK_ps_carrier_lang");

                entity.ToTable("ps_carrier_lang");

                entity.Property(e => e.IdCarrier)
                    .HasColumnName("id_carrier")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Delay)
                    .HasColumnName("delay")
                    .HasColumnType("varchar(512)");
            });

            modelBuilder.Entity<PsCarrierShop>(entity =>
            {
                entity.HasKey(e => new { e.IdCarrier, e.IdShop })
                    .HasName("PK_ps_carrier_shop");

                entity.ToTable("ps_carrier_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdCarrier)
                    .HasColumnName("id_carrier")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsCarrierTaxRulesGroupShop>(entity =>
            {
                entity.HasKey(e => new { e.IdCarrier, e.IdTaxRulesGroup, e.IdShop })
                    .HasName("PK_ps_carrier_tax_rules_group_shop");

                entity.ToTable("ps_carrier_tax_rules_group_shop");

                entity.Property(e => e.IdCarrier)
                    .HasColumnName("id_carrier")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdTaxRulesGroup)
                    .HasColumnName("id_tax_rules_group")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsCarrierZone>(entity =>
            {
                entity.HasKey(e => new { e.IdCarrier, e.IdZone })
                    .HasName("PK_ps_carrier_zone");

                entity.ToTable("ps_carrier_zone");

                entity.Property(e => e.IdCarrier)
                    .HasColumnName("id_carrier")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdZone)
                    .HasColumnName("id_zone")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsCart>(entity =>
            {
                entity.HasKey(e => e.IdCart)
                    .HasName("PK_ps_cart");

                entity.ToTable("ps_cart");

                entity.HasIndex(e => e.IdAddressDelivery)
                    .HasName("id_address_delivery");

                entity.HasIndex(e => e.IdAddressInvoice)
                    .HasName("id_address_invoice");

                entity.HasIndex(e => e.IdCarrier)
                    .HasName("id_carrier");

                entity.HasIndex(e => e.IdCurrency)
                    .HasName("id_currency");

                entity.HasIndex(e => e.IdCustomer)
                    .HasName("cart_customer");

                entity.HasIndex(e => e.IdGuest)
                    .HasName("id_guest");

                entity.HasIndex(e => e.IdLang)
                    .HasName("id_lang");

                entity.HasIndex(e => e.IdShopGroup)
                    .HasName("id_shop_group");

                entity.HasIndex(e => new { e.IdShop, e.DateAdd })
                    .HasName("id_shop");

                entity.HasIndex(e => new { e.IdShop, e.DateUpd })
                    .HasName("id_shop_2");

                entity.Property(e => e.IdCart)
                    .HasColumnName("id_cart")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.AllowSeperatedPackage)
                    .HasColumnName("allow_seperated_package")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeliveryOption)
                    .IsRequired()
                    .HasColumnName("delivery_option")
                    .HasColumnType("text");

                entity.Property(e => e.Gift)
                    .HasColumnName("gift")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.GiftMessage)
                    .HasColumnName("gift_message")
                    .HasColumnType("text");

                entity.Property(e => e.IdAddressDelivery)
                    .HasColumnName("id_address_delivery")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdAddressInvoice)
                    .HasColumnName("id_address_invoice")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCarrier)
                    .HasColumnName("id_carrier")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCurrency)
                    .HasColumnName("id_currency")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCustomer)
                    .HasColumnName("id_customer")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdGuest)
                    .HasColumnName("id_guest")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdShopGroup)
                    .HasColumnName("id_shop_group")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.MobileTheme)
                    .HasColumnName("mobile_theme")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Recyclable)
                    .HasColumnName("recyclable")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.SecureKey)
                    .IsRequired()
                    .HasColumnName("secure_key")
                    .HasColumnType("varchar(32)")
                    .HasDefaultValueSql("-1");
            });

            modelBuilder.Entity<PsCartCartRule>(entity =>
            {
                entity.HasKey(e => new { e.IdCart, e.IdCartRule })
                    .HasName("PK_ps_cart_cart_rule");

                entity.ToTable("ps_cart_cart_rule");

                entity.HasIndex(e => e.IdCartRule)
                    .HasName("id_cart_rule");

                entity.Property(e => e.IdCart)
                    .HasColumnName("id_cart")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCartRule)
                    .HasColumnName("id_cart_rule")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsCartProduct>(entity =>
            {
                entity.HasKey(e => new { e.IdCart, e.IdProduct, e.IdAddressDelivery, e.IdProductAttribute })
                    .HasName("PK_ps_cart_product");

                entity.ToTable("ps_cart_product");

                entity.HasIndex(e => e.IdProductAttribute)
                    .HasName("id_product_attribute");

                entity.HasIndex(e => new { e.IdCart, e.DateAdd, e.IdProduct, e.IdProductAttribute })
                    .HasName("id_cart_order");

                entity.Property(e => e.IdCart)
                    .HasColumnName("id_cart")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdAddressDelivery)
                    .HasColumnName("id_address_delivery")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdProductAttribute)
                    .HasColumnName("id_product_attribute")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsCartRule>(entity =>
            {
                entity.HasKey(e => e.IdCartRule)
                    .HasName("PK_ps_cart_rule");

                entity.ToTable("ps_cart_rule");

                entity.HasIndex(e => new { e.GroupRestriction, e.Active, e.DateTo })
                    .HasName("group_restriction");

                entity.HasIndex(e => new { e.IdCustomer, e.Active, e.DateTo })
                    .HasName("id_customer");

                entity.HasIndex(e => new { e.GroupRestriction, e.Active, e.Highlight, e.DateTo })
                    .HasName("group_restriction_2");

                entity.HasIndex(e => new { e.IdCustomer, e.Active, e.Highlight, e.DateTo })
                    .HasName("id_customer_2");

                entity.Property(e => e.IdCartRule)
                    .HasColumnName("id_cart_rule")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.CarrierRestriction)
                    .HasColumnName("carrier_restriction")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.CartRuleRestriction)
                    .HasColumnName("cart_rule_restriction")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasColumnType("varchar(254)");

                entity.Property(e => e.CountryRestriction)
                    .HasColumnName("country_restriction")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateFrom)
                    .HasColumnName("date_from")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateTo)
                    .HasColumnName("date_to")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.FreeShipping)
                    .HasColumnName("free_shipping")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.GiftProduct)
                    .HasColumnName("gift_product")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.GiftProductAttribute)
                    .HasColumnName("gift_product_attribute")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.GroupRestriction)
                    .HasColumnName("group_restriction")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Highlight)
                    .HasColumnName("highlight")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdCustomer)
                    .HasColumnName("id_customer")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.MinimumAmount)
                    .HasColumnName("minimum_amount")
                    .HasColumnType("decimal(17,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.MinimumAmountCurrency)
                    .HasColumnName("minimum_amount_currency")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.MinimumAmountShipping)
                    .HasColumnName("minimum_amount_shipping")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.MinimumAmountTax)
                    .HasColumnName("minimum_amount_tax")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.PartialUse)
                    .HasColumnName("partial_use")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Priority)
                    .HasColumnName("priority")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.ProductRestriction)
                    .HasColumnName("product_restriction")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.QuantityPerUser)
                    .HasColumnName("quantity_per_user")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ReductionAmount)
                    .HasColumnName("reduction_amount")
                    .HasColumnType("decimal(17,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.ReductionCurrency)
                    .HasColumnName("reduction_currency")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ReductionPercent)
                    .HasColumnName("reduction_percent")
                    .HasColumnType("decimal(5,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.ReductionProduct)
                    .HasColumnName("reduction_product")
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ReductionTax)
                    .HasColumnName("reduction_tax")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ShopRestriction)
                    .HasColumnName("shop_restriction")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsCartRuleCarrier>(entity =>
            {
                entity.HasKey(e => new { e.IdCartRule, e.IdCarrier })
                    .HasName("PK_ps_cart_rule_carrier");

                entity.ToTable("ps_cart_rule_carrier");

                entity.Property(e => e.IdCartRule)
                    .HasColumnName("id_cart_rule")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCarrier)
                    .HasColumnName("id_carrier")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsCartRuleCombination>(entity =>
            {
                entity.HasKey(e => new { e.IdCartRule1, e.IdCartRule2 })
                    .HasName("PK_ps_cart_rule_combination");

                entity.ToTable("ps_cart_rule_combination");

                entity.HasIndex(e => e.IdCartRule1)
                    .HasName("id_cart_rule_1");

                entity.HasIndex(e => e.IdCartRule2)
                    .HasName("id_cart_rule_2");

                entity.Property(e => e.IdCartRule1)
                    .HasColumnName("id_cart_rule_1")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCartRule2)
                    .HasColumnName("id_cart_rule_2")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsCartRuleCountry>(entity =>
            {
                entity.HasKey(e => new { e.IdCartRule, e.IdCountry })
                    .HasName("PK_ps_cart_rule_country");

                entity.ToTable("ps_cart_rule_country");

                entity.Property(e => e.IdCartRule)
                    .HasColumnName("id_cart_rule")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCountry)
                    .HasColumnName("id_country")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsCartRuleGroup>(entity =>
            {
                entity.HasKey(e => new { e.IdCartRule, e.IdGroup })
                    .HasName("PK_ps_cart_rule_group");

                entity.ToTable("ps_cart_rule_group");

                entity.Property(e => e.IdCartRule)
                    .HasColumnName("id_cart_rule")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdGroup)
                    .HasColumnName("id_group")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsCartRuleLang>(entity =>
            {
                entity.HasKey(e => new { e.IdCartRule, e.IdLang })
                    .HasName("PK_ps_cart_rule_lang");

                entity.ToTable("ps_cart_rule_lang");

                entity.Property(e => e.IdCartRule)
                    .HasColumnName("id_cart_rule")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(254)");
            });

            modelBuilder.Entity<PsCartRuleProductRule>(entity =>
            {
                entity.HasKey(e => e.IdProductRule)
                    .HasName("PK_ps_cart_rule_product_rule");

                entity.ToTable("ps_cart_rule_product_rule");

                entity.Property(e => e.IdProductRule)
                    .HasColumnName("id_product_rule")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProductRuleGroup)
                    .HasColumnName("id_product_rule_group")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsCartRuleProductRuleGroup>(entity =>
            {
                entity.HasKey(e => e.IdProductRuleGroup)
                    .HasName("PK_ps_cart_rule_product_rule_group");

                entity.ToTable("ps_cart_rule_product_rule_group");

                entity.Property(e => e.IdProductRuleGroup)
                    .HasColumnName("id_product_rule_group")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCartRule)
                    .HasColumnName("id_cart_rule")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");
            });

            modelBuilder.Entity<PsCartRuleProductRuleValue>(entity =>
            {
                entity.HasKey(e => new { e.IdProductRule, e.IdItem })
                    .HasName("PK_ps_cart_rule_product_rule_value");

                entity.ToTable("ps_cart_rule_product_rule_value");

                entity.Property(e => e.IdProductRule)
                    .HasColumnName("id_product_rule")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdItem)
                    .HasColumnName("id_item")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsCartRuleShop>(entity =>
            {
                entity.HasKey(e => new { e.IdCartRule, e.IdShop })
                    .HasName("PK_ps_cart_rule_shop");

                entity.ToTable("ps_cart_rule_shop");

                entity.Property(e => e.IdCartRule)
                    .HasColumnName("id_cart_rule")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsCategory>(entity =>
            {
                entity.HasKey(e => e.IdCategory)
                    .HasName("PK_ps_category");

                entity.ToTable("ps_category");

                entity.HasIndex(e => e.IdParent)
                    .HasName("category_parent");

                entity.HasIndex(e => e.LevelDepth)
                    .HasName("level_depth");

                entity.HasIndex(e => e.Nright)
                    .HasName("nright");

                entity.HasIndex(e => new { e.Active, e.Nleft })
                    .HasName("activenleft");

                entity.HasIndex(e => new { e.Active, e.Nright })
                    .HasName("activenright");

                entity.HasIndex(e => new { e.Nleft, e.Nright, e.Active })
                    .HasName("nleftrightactive");

                entity.Property(e => e.IdCategory)
                    .HasColumnName("id_category")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdParent)
                    .HasColumnName("id_parent")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShopDefault)
                    .HasColumnName("id_shop_default")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IsRootCategory)
                    .HasColumnName("is_root_category")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.LevelDepth)
                    .HasColumnName("level_depth")
                    .HasColumnType("tinyint(3) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Nleft)
                    .HasColumnName("nleft")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Nright)
                    .HasColumnName("nright")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsCategoryGroup>(entity =>
            {
                entity.HasKey(e => new { e.IdCategory, e.IdGroup })
                    .HasName("PK_ps_category_group");

                entity.ToTable("ps_category_group");

                entity.HasIndex(e => e.IdCategory)
                    .HasName("id_category");

                entity.HasIndex(e => e.IdGroup)
                    .HasName("id_group");

                entity.Property(e => e.IdCategory)
                    .HasColumnName("id_category")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdGroup)
                    .HasColumnName("id_group")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsCategoryLang>(entity =>
            {
                entity.HasKey(e => new { e.IdCategory, e.IdShop, e.IdLang })
                    .HasName("PK_ps_category_lang");

                entity.ToTable("ps_category_lang");

                entity.HasIndex(e => e.Name)
                    .HasName("category_name");

                entity.Property(e => e.IdCategory)
                    .HasColumnName("id_category")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.LinkRewrite)
                    .IsRequired()
                    .HasColumnName("link_rewrite")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.MetaDescription)
                    .HasColumnName("meta_description")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.MetaKeywords)
                    .HasColumnName("meta_keywords")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.MetaTitle)
                    .HasColumnName("meta_title")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsCategoryProduct>(entity =>
            {
                entity.HasKey(e => new { e.IdCategory, e.IdProduct })
                    .HasName("PK_ps_category_product");

                entity.ToTable("ps_category_product");

                entity.HasIndex(e => e.IdProduct)
                    .HasName("id_product");

                entity.HasIndex(e => new { e.IdCategory, e.Position })
                    .HasName("id_category");

                entity.Property(e => e.IdCategory)
                    .HasColumnName("id_category")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsCategoryShop>(entity =>
            {
                entity.HasKey(e => new { e.IdCategory, e.IdShop })
                    .HasName("PK_ps_category_shop");

                entity.ToTable("ps_category_shop");

                entity.Property(e => e.IdCategory)
                    .HasColumnName("id_category")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsCms>(entity =>
            {
                entity.HasKey(e => e.IdCms)
                    .HasName("PK_ps_cms");

                entity.ToTable("ps_cms");

                entity.Property(e => e.IdCms)
                    .HasColumnName("id_cms")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdCmsCategory)
                    .HasColumnName("id_cms_category")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Indexation)
                    .HasColumnName("indexation")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsCmsBlock>(entity =>
            {
                entity.HasKey(e => e.IdCmsBlock)
                    .HasName("PK_ps_cms_block");

                entity.ToTable("ps_cms_block");

                entity.Property(e => e.IdCmsBlock)
                    .HasColumnName("id_cms_block")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DisplayStore)
                    .HasColumnName("display_store")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdCmsCategory)
                    .HasColumnName("id_cms_category")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Location)
                    .HasColumnName("location")
                    .HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsCmsBlockLang>(entity =>
            {
                entity.HasKey(e => new { e.IdCmsBlock, e.IdLang })
                    .HasName("PK_ps_cms_block_lang");

                entity.ToTable("ps_cms_block_lang");

                entity.Property(e => e.IdCmsBlock)
                    .HasColumnName("id_cms_block")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(40)");
            });

            modelBuilder.Entity<PsCmsBlockPage>(entity =>
            {
                entity.HasKey(e => e.IdCmsBlockPage)
                    .HasName("PK_ps_cms_block_page");

                entity.ToTable("ps_cms_block_page");

                entity.Property(e => e.IdCmsBlockPage)
                    .HasColumnName("id_cms_block_page")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCms)
                    .HasColumnName("id_cms")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCmsBlock)
                    .HasColumnName("id_cms_block")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IsCategory)
                    .HasColumnName("is_category")
                    .HasColumnType("tinyint(1) unsigned");
            });

            modelBuilder.Entity<PsCmsBlockShop>(entity =>
            {
                entity.HasKey(e => new { e.IdCmsBlock, e.IdShop })
                    .HasName("PK_ps_cms_block_shop");

                entity.ToTable("ps_cms_block_shop");

                entity.Property(e => e.IdCmsBlock)
                    .HasColumnName("id_cms_block")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsCmsCategory>(entity =>
            {
                entity.HasKey(e => e.IdCmsCategory)
                    .HasName("PK_ps_cms_category");

                entity.ToTable("ps_cms_category");

                entity.HasIndex(e => e.IdParent)
                    .HasName("category_parent");

                entity.Property(e => e.IdCmsCategory)
                    .HasColumnName("id_cms_category")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdParent)
                    .HasColumnName("id_parent")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.LevelDepth)
                    .HasColumnName("level_depth")
                    .HasColumnType("tinyint(3) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsCmsCategoryLang>(entity =>
            {
                entity.HasKey(e => new { e.IdCmsCategory, e.IdLang, e.IdShop })
                    .HasName("PK_ps_cms_category_lang");

                entity.ToTable("ps_cms_category_lang");

                entity.HasIndex(e => e.Name)
                    .HasName("category_name");

                entity.Property(e => e.IdCmsCategory)
                    .HasColumnName("id_cms_category")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.LinkRewrite)
                    .IsRequired()
                    .HasColumnName("link_rewrite")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.MetaDescription)
                    .HasColumnName("meta_description")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.MetaKeywords)
                    .HasColumnName("meta_keywords")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.MetaTitle)
                    .HasColumnName("meta_title")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsCmsCategoryShop>(entity =>
            {
                entity.HasKey(e => new { e.IdCmsCategory, e.IdShop })
                    .HasName("PK_ps_cms_category_shop");

                entity.ToTable("ps_cms_category_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdCmsCategory)
                    .HasColumnName("id_cms_category")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsCmsLang>(entity =>
            {
                entity.HasKey(e => new { e.IdCms, e.IdLang, e.IdShop })
                    .HasName("PK_ps_cms_lang");

                entity.ToTable("ps_cms_lang");

                entity.Property(e => e.IdCms)
                    .HasColumnName("id_cms")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Content).HasColumnName("content");

                entity.Property(e => e.LinkRewrite)
                    .IsRequired()
                    .HasColumnName("link_rewrite")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.MetaDescription)
                    .HasColumnName("meta_description")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.MetaKeywords)
                    .HasColumnName("meta_keywords")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.MetaTitle)
                    .IsRequired()
                    .HasColumnName("meta_title")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsCmsRole>(entity =>
            {
                entity.HasKey(e => new { e.IdCmsRole, e.IdCms })
                    .HasName("PK_ps_cms_role");

                entity.ToTable("ps_cms_role");

                entity.HasIndex(e => e.Name)
                    .HasName("name")
                    .IsUnique();

                entity.Property(e => e.IdCmsRole)
                    .HasColumnName("id_cms_role")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdCms)
                    .HasColumnName("id_cms")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<PsCmsRoleLang>(entity =>
            {
                entity.HasKey(e => new { e.IdCmsRole, e.IdLang, e.IdShop })
                    .HasName("PK_ps_cms_role_lang");

                entity.ToTable("ps_cms_role_lang");

                entity.Property(e => e.IdCmsRole)
                    .HasColumnName("id_cms_role")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsCmsShop>(entity =>
            {
                entity.HasKey(e => new { e.IdCms, e.IdShop })
                    .HasName("PK_ps_cms_shop");

                entity.ToTable("ps_cms_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdCms)
                    .HasColumnName("id_cms")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsCompare>(entity =>
            {
                entity.HasKey(e => e.IdCompare)
                    .HasName("PK_ps_compare");

                entity.ToTable("ps_compare");

                entity.Property(e => e.IdCompare)
                    .HasColumnName("id_compare")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCustomer)
                    .HasColumnName("id_customer")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsCompareProduct>(entity =>
            {
                entity.HasKey(e => new { e.IdCompare, e.IdProduct })
                    .HasName("PK_ps_compare_product");

                entity.ToTable("ps_compare_product");

                entity.Property(e => e.IdCompare)
                    .HasColumnName("id_compare")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PsCondition>(entity =>
            {
                entity.HasKey(e => new { e.IdCondition, e.IdPsCondition })
                    .HasName("PK_ps_condition");

                entity.ToTable("ps_condition");

                entity.Property(e => e.IdCondition)
                    .HasColumnName("id_condition")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdPsCondition)
                    .HasColumnName("id_ps_condition")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CalculationDetail)
                    .HasColumnName("calculation_detail")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.Operator)
                    .HasColumnName("operator")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.Request)
                    .HasColumnName("request")
                    .HasColumnType("text");

                entity.Property(e => e.Result)
                    .HasColumnName("result")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Validated)
                    .HasColumnName("validated")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsConditionAdvice>(entity =>
            {
                entity.HasKey(e => new { e.IdCondition, e.IdAdvice })
                    .HasName("PK_ps_condition_advice");

                entity.ToTable("ps_condition_advice");

                entity.Property(e => e.IdCondition)
                    .HasColumnName("id_condition")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdAdvice)
                    .HasColumnName("id_advice")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Display)
                    .HasColumnName("display")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsConditionBadge>(entity =>
            {
                entity.HasKey(e => new { e.IdCondition, e.IdBadge })
                    .HasName("PK_ps_condition_badge");

                entity.ToTable("ps_condition_badge");

                entity.Property(e => e.IdCondition)
                    .HasColumnName("id_condition")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdBadge)
                    .HasColumnName("id_badge")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<PsConfiguration>(entity =>
            {
                entity.HasKey(e => e.IdConfiguration)
                    .HasName("PK_ps_configuration");

                entity.ToTable("ps_configuration");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.HasIndex(e => e.IdShopGroup)
                    .HasName("id_shop_group");

                entity.HasIndex(e => e.Name)
                    .HasName("name");

                entity.Property(e => e.IdConfiguration)
                    .HasColumnName("id_configuration")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShopGroup)
                    .HasColumnName("id_shop_group")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(254)");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<PsConfigurationKpi>(entity =>
            {
                entity.HasKey(e => e.IdConfigurationKpi)
                    .HasName("PK_ps_configuration_kpi");

                entity.ToTable("ps_configuration_kpi");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.HasIndex(e => e.IdShopGroup)
                    .HasName("id_shop_group");

                entity.HasIndex(e => e.Name)
                    .HasName("name");

                entity.Property(e => e.IdConfigurationKpi)
                    .HasColumnName("id_configuration_kpi")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShopGroup)
                    .HasColumnName("id_shop_group")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<PsConfigurationKpiLang>(entity =>
            {
                entity.HasKey(e => new { e.IdConfigurationKpi, e.IdLang })
                    .HasName("PK_ps_configuration_kpi_lang");

                entity.ToTable("ps_configuration_kpi_lang");

                entity.Property(e => e.IdConfigurationKpi)
                    .HasColumnName("id_configuration_kpi")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<PsConfigurationLang>(entity =>
            {
                entity.HasKey(e => new { e.IdConfiguration, e.IdLang })
                    .HasName("PK_ps_configuration_lang");

                entity.ToTable("ps_configuration_lang");

                entity.Property(e => e.IdConfiguration)
                    .HasColumnName("id_configuration")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<PsConnections>(entity =>
            {
                entity.HasKey(e => e.IdConnections)
                    .HasName("PK_ps_connections");

                entity.ToTable("ps_connections");

                entity.HasIndex(e => e.DateAdd)
                    .HasName("date_add");

                entity.HasIndex(e => e.IdGuest)
                    .HasName("id_guest");

                entity.HasIndex(e => e.IdPage)
                    .HasName("id_page");

                entity.Property(e => e.IdConnections)
                    .HasColumnName("id_connections")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.HttpReferer)
                    .HasColumnName("http_referer")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.IdGuest)
                    .HasColumnName("id_guest")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdPage)
                    .HasColumnName("id_page")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdShopGroup)
                    .HasColumnName("id_shop_group")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IpAddress)
                    .HasColumnName("ip_address")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<PsConnectionsPage>(entity =>
            {
                entity.HasKey(e => new { e.IdConnections, e.IdPage, e.TimeStart })
                    .HasName("PK_ps_connections_page");

                entity.ToTable("ps_connections_page");

                entity.Property(e => e.IdConnections)
                    .HasColumnName("id_connections")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdPage)
                    .HasColumnName("id_page")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.TimeStart)
                    .HasColumnName("time_start")
                    .HasColumnType("datetime");

                entity.Property(e => e.TimeEnd)
                    .HasColumnName("time_end")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PsConnectionsSource>(entity =>
            {
                entity.HasKey(e => e.IdConnectionsSource)
                    .HasName("PK_ps_connections_source");

                entity.ToTable("ps_connections_source");

                entity.HasIndex(e => e.DateAdd)
                    .HasName("orderby");

                entity.HasIndex(e => e.HttpReferer)
                    .HasName("http_referer");

                entity.HasIndex(e => e.IdConnections)
                    .HasName("connections");

                entity.HasIndex(e => e.RequestUri)
                    .HasName("request_uri");

                entity.Property(e => e.IdConnectionsSource)
                    .HasColumnName("id_connections_source")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.HttpReferer)
                    .HasColumnName("http_referer")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.IdConnections)
                    .HasColumnName("id_connections")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Keywords)
                    .HasColumnName("keywords")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.RequestUri)
                    .HasColumnName("request_uri")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<PsContact>(entity =>
            {
                entity.HasKey(e => e.IdContact)
                    .HasName("PK_ps_contact");

                entity.ToTable("ps_contact");

                entity.Property(e => e.IdContact)
                    .HasColumnName("id_contact")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.CustomerService)
                    .HasColumnName("customer_service")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasColumnType("tinyint(2) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsContactLang>(entity =>
            {
                entity.HasKey(e => new { e.IdContact, e.IdLang })
                    .HasName("PK_ps_contact_lang");

                entity.ToTable("ps_contact_lang");

                entity.Property(e => e.IdContact)
                    .HasColumnName("id_contact")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<PsContactShop>(entity =>
            {
                entity.HasKey(e => new { e.IdContact, e.IdShop })
                    .HasName("PK_ps_contact_shop");

                entity.ToTable("ps_contact_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdContact)
                    .HasColumnName("id_contact")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsCountry>(entity =>
            {
                entity.HasKey(e => e.IdCountry)
                    .HasName("PK_ps_country");

                entity.ToTable("ps_country");

                entity.HasIndex(e => e.IdZone)
                    .HasName("country_");

                entity.HasIndex(e => e.IsoCode)
                    .HasName("country_iso_code");

                entity.Property(e => e.IdCountry)
                    .HasColumnName("id_country")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.CallPrefix)
                    .HasColumnName("call_prefix")
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ContainsStates)
                    .HasColumnName("contains_states")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DisplayTaxLabel)
                    .HasColumnName("display_tax_label")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.IdCurrency)
                    .HasColumnName("id_currency")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdZone)
                    .HasColumnName("id_zone")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IsoCode)
                    .IsRequired()
                    .HasColumnName("iso_code")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.NeedIdentificationNumber)
                    .HasColumnName("need_identification_number")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.NeedZipCode)
                    .HasColumnName("need_zip_code")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.ZipCodeFormat)
                    .IsRequired()
                    .HasColumnName("zip_code_format")
                    .HasColumnType("varchar(12)");
            });

            modelBuilder.Entity<PsCountryLang>(entity =>
            {
                entity.HasKey(e => new { e.IdCountry, e.IdLang })
                    .HasName("PK_ps_country_lang");

                entity.ToTable("ps_country_lang");

                entity.Property(e => e.IdCountry)
                    .HasColumnName("id_country")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsCountryShop>(entity =>
            {
                entity.HasKey(e => new { e.IdCountry, e.IdShop })
                    .HasName("PK_ps_country_shop");

                entity.ToTable("ps_country_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdCountry)
                    .HasColumnName("id_country")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsCronjobs>(entity =>
            {
                entity.HasKey(e => e.IdCronjob)
                    .HasName("PK_ps_cronjobs");

                entity.ToTable("ps_cronjobs");

                entity.HasIndex(e => e.IdModule)
                    .HasName("id_module");

                entity.Property(e => e.IdCronjob)
                    .HasColumnName("id_cronjob")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Day)
                    .HasColumnName("day")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("-1");

                entity.Property(e => e.DayOfWeek)
                    .HasColumnName("day_of_week")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("-1");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.Hour)
                    .HasColumnName("hour")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("-1");

                entity.Property(e => e.IdModule)
                    .HasColumnName("id_module")
                    .HasColumnType("int(10)");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdShopGroup)
                    .HasColumnName("id_shop_group")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Month)
                    .HasColumnName("month")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("-1");

                entity.Property(e => e.OneShot)
                    .HasColumnName("one_shot")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Task)
                    .HasColumnName("task")
                    .HasColumnType("text");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PsCurrency>(entity =>
            {
                entity.HasKey(e => e.IdCurrency)
                    .HasName("PK_ps_currency");

                entity.ToTable("ps_currency");

                entity.Property(e => e.IdCurrency)
                    .HasColumnName("id_currency")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Blank)
                    .HasColumnName("blank")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ConversionRate)
                    .HasColumnName("conversion_rate")
                    .HasColumnType("decimal(13,6)");

                entity.Property(e => e.Decimals)
                    .HasColumnName("decimals")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Deleted)
                    .HasColumnName("deleted")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Format)
                    .HasColumnName("format")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IsoCode)
                    .IsRequired()
                    .HasColumnName("iso_code")
                    .HasColumnType("varchar(3)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IsoCodeNum)
                    .IsRequired()
                    .HasColumnName("iso_code_num")
                    .HasColumnType("varchar(3)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.Sign)
                    .IsRequired()
                    .HasColumnName("sign")
                    .HasColumnType("varchar(8)");
            });

            modelBuilder.Entity<PsCurrencyShop>(entity =>
            {
                entity.HasKey(e => new { e.IdCurrency, e.IdShop })
                    .HasName("PK_ps_currency_shop");

                entity.ToTable("ps_currency_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdCurrency)
                    .HasColumnName("id_currency")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.ConversionRate)
                    .HasColumnName("conversion_rate")
                    .HasColumnType("decimal(13,6)");
            });

            modelBuilder.Entity<PsCustomer>(entity =>
            {
                entity.HasKey(e => e.IdCustomer)
                    .HasName("PK_ps_customer");

                entity.ToTable("ps_customer");

                entity.HasIndex(e => e.Email)
                    .HasName("customer_email");

                entity.HasIndex(e => e.IdGender)
                    .HasName("id_gender");

                entity.HasIndex(e => e.IdShopGroup)
                    .HasName("id_shop_group");

                entity.HasIndex(e => new { e.Email, e.Passwd })
                    .HasName("customer_login");

                entity.HasIndex(e => new { e.IdCustomer, e.Passwd })
                    .HasName("id_customer_passwd");

                entity.HasIndex(e => new { e.IdShop, e.DateAdd })
                    .HasName("id_shop");

                entity.Property(e => e.IdCustomer)
                    .HasColumnName("id_customer")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Ape)
                    .HasColumnName("ape")
                    .HasColumnType("varchar(5)");

                entity.Property(e => e.Birthday)
                    .HasColumnName("birthday")
                    .HasColumnType("date");

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deleted)
                    .HasColumnName("deleted")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasColumnName("firstname")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.IdDefaultGroup)
                    .HasColumnName("id_default_group")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdGender)
                    .HasColumnName("id_gender")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdRisk)
                    .HasColumnName("id_risk")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdShopGroup)
                    .HasColumnName("id_shop_group")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IpRegistrationNewsletter)
                    .HasColumnName("ip_registration_newsletter")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.IsGuest)
                    .HasColumnName("is_guest")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.LastPasswdGen)
                    .HasColumnName("last_passwd_gen")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasColumnName("lastname")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.MaxPaymentDays)
                    .HasColumnName("max_payment_days")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("60");

                entity.Property(e => e.Newsletter)
                    .HasColumnName("newsletter")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.NewsletterDateAdd)
                    .HasColumnName("newsletter_date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasColumnType("text");

                entity.Property(e => e.Optin)
                    .HasColumnName("optin")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.OutstandingAllowAmount)
                    .HasColumnName("outstanding_allow_amount")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.Passwd)
                    .IsRequired()
                    .HasColumnName("passwd")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.SecureKey)
                    .IsRequired()
                    .HasColumnName("secure_key")
                    .HasColumnType("varchar(32)")
                    .HasDefaultValueSql("-1");

                entity.Property(e => e.ShowPublicPrices)
                    .HasColumnName("show_public_prices")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Siret)
                    .HasColumnName("siret")
                    .HasColumnType("varchar(14)");

                entity.Property(e => e.Website)
                    .HasColumnName("website")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsCustomerGroup>(entity =>
            {
                entity.HasKey(e => new { e.IdCustomer, e.IdGroup })
                    .HasName("PK_ps_customer_group");

                entity.ToTable("ps_customer_group");

                entity.HasIndex(e => e.IdCustomer)
                    .HasName("id_customer");

                entity.HasIndex(e => e.IdGroup)
                    .HasName("customer_login");

                entity.Property(e => e.IdCustomer)
                    .HasColumnName("id_customer")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdGroup)
                    .HasColumnName("id_group")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsCustomerMessage>(entity =>
            {
                entity.HasKey(e => e.IdCustomerMessage)
                    .HasName("PK_ps_customer_message");

                entity.ToTable("ps_customer_message");

                entity.HasIndex(e => e.IdCustomerThread)
                    .HasName("id_customer_thread");

                entity.HasIndex(e => e.IdEmployee)
                    .HasName("id_employee");

                entity.Property(e => e.IdCustomerMessage)
                    .HasColumnName("id_customer_message")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.FileName)
                    .HasColumnName("file_name")
                    .HasColumnType("varchar(18)");

                entity.Property(e => e.IdCustomerThread)
                    .HasColumnName("id_customer_thread")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdEmployee)
                    .HasColumnName("id_employee")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IpAddress)
                    .HasColumnName("ip_address")
                    .HasColumnType("varchar(16)");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasColumnName("message")
                    .HasColumnType("mediumtext");

                entity.Property(e => e.Private)
                    .HasColumnName("private")
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Read)
                    .HasColumnName("read")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.UserAgent)
                    .HasColumnName("user_agent")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsCustomerThread>(entity =>
            {
                entity.HasKey(e => e.IdCustomerThread)
                    .HasName("PK_ps_customer_thread");

                entity.ToTable("ps_customer_thread");

                entity.HasIndex(e => e.IdContact)
                    .HasName("id_contact");

                entity.HasIndex(e => e.IdCustomer)
                    .HasName("id_customer");

                entity.HasIndex(e => e.IdLang)
                    .HasName("id_lang");

                entity.HasIndex(e => e.IdOrder)
                    .HasName("id_order");

                entity.HasIndex(e => e.IdProduct)
                    .HasName("id_product");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdCustomerThread)
                    .HasColumnName("id_customer_thread")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.IdContact)
                    .HasColumnName("id_contact")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCustomer)
                    .HasColumnName("id_customer")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdOrder)
                    .HasColumnName("id_order")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Token)
                    .HasColumnName("token")
                    .HasColumnType("varchar(12)");
            });

            modelBuilder.Entity<PsCustomization>(entity =>
            {
                entity.HasKey(e => new { e.IdCustomization, e.IdAddressDelivery, e.IdCart, e.IdProduct })
                    .HasName("PK_ps_customization");

                entity.ToTable("ps_customization");

                entity.HasIndex(e => e.IdProductAttribute)
                    .HasName("id_product_attribute");

                entity.HasIndex(e => new { e.IdCart, e.IdProduct, e.IdProductAttribute })
                    .HasName("id_cart_product");

                entity.Property(e => e.IdCustomization)
                    .HasColumnName("id_customization")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdAddressDelivery)
                    .HasColumnName("id_address_delivery")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdCart)
                    .HasColumnName("id_cart")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10)");

                entity.Property(e => e.IdProductAttribute)
                    .HasColumnName("id_product_attribute")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.InCart)
                    .HasColumnName("in_cart")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(10)");

                entity.Property(e => e.QuantityRefunded)
                    .HasColumnName("quantity_refunded")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.QuantityReturned)
                    .HasColumnName("quantity_returned")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsCustomizationField>(entity =>
            {
                entity.HasKey(e => e.IdCustomizationField)
                    .HasName("PK_ps_customization_field");

                entity.ToTable("ps_customization_field");

                entity.HasIndex(e => e.IdProduct)
                    .HasName("id_product");

                entity.Property(e => e.IdCustomizationField)
                    .HasColumnName("id_customization_field")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Required)
                    .HasColumnName("required")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(1)");
            });

            modelBuilder.Entity<PsCustomizationFieldLang>(entity =>
            {
                entity.HasKey(e => new { e.IdCustomizationField, e.IdLang, e.IdShop })
                    .HasName("PK_ps_customization_field_lang");

                entity.ToTable("ps_customization_field_lang");

                entity.Property(e => e.IdCustomizationField)
                    .HasColumnName("id_customization_field")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<PsCustomizedData>(entity =>
            {
                entity.HasKey(e => new { e.IdCustomization, e.Type, e.Index })
                    .HasName("PK_ps_customized_data");

                entity.ToTable("ps_customized_data");

                entity.Property(e => e.IdCustomization)
                    .HasColumnName("id_customization")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.Index)
                    .HasColumnName("index")
                    .HasColumnType("int(3)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnName("value")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<PsDateRange>(entity =>
            {
                entity.HasKey(e => e.IdDateRange)
                    .HasName("PK_ps_date_range");

                entity.ToTable("ps_date_range");

                entity.Property(e => e.IdDateRange)
                    .HasColumnName("id_date_range")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.TimeEnd)
                    .HasColumnName("time_end")
                    .HasColumnType("datetime");

                entity.Property(e => e.TimeStart)
                    .HasColumnName("time_start")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PsDelivery>(entity =>
            {
                entity.HasKey(e => e.IdDelivery)
                    .HasName("PK_ps_delivery");

                entity.ToTable("ps_delivery");

                entity.HasIndex(e => e.IdRangePrice)
                    .HasName("id_range_price");

                entity.HasIndex(e => e.IdRangeWeight)
                    .HasName("id_range_weight");

                entity.HasIndex(e => e.IdZone)
                    .HasName("id_zone");

                entity.HasIndex(e => new { e.IdCarrier, e.IdZone })
                    .HasName("id_carrier");

                entity.Property(e => e.IdDelivery)
                    .HasColumnName("id_delivery")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCarrier)
                    .HasColumnName("id_carrier")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdRangePrice)
                    .HasColumnName("id_range_price")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdRangeWeight)
                    .HasColumnName("id_range_weight")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShopGroup)
                    .HasColumnName("id_shop_group")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdZone)
                    .HasColumnName("id_zone")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(20,6)");
            });

            modelBuilder.Entity<PsEmployee>(entity =>
            {
                entity.HasKey(e => e.IdEmployee)
                    .HasName("PK_ps_employee");

                entity.ToTable("ps_employee");

                entity.HasIndex(e => e.IdProfile)
                    .HasName("id_profile");

                entity.HasIndex(e => new { e.Email, e.Passwd })
                    .HasName("employee_login");

                entity.HasIndex(e => new { e.IdEmployee, e.Passwd })
                    .HasName("id_employee_passwd");

                entity.Property(e => e.IdEmployee)
                    .HasColumnName("id_employee")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.BoColor)
                    .HasColumnName("bo_color")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.BoCss)
                    .HasColumnName("bo_css")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.BoMenu)
                    .HasColumnName("bo_menu")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.BoTheme)
                    .HasColumnName("bo_theme")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.BoWidth)
                    .HasColumnName("bo_width")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DefaultTab)
                    .HasColumnName("default_tab")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasColumnName("firstname")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdLastCustomer)
                    .HasColumnName("id_last_customer")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdLastCustomerMessage)
                    .HasColumnName("id_last_customer_message")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdLastOrder)
                    .HasColumnName("id_last_order")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdProfile)
                    .HasColumnName("id_profile")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.LastConnectionDate)
                    .HasColumnName("last_connection_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("0000-00-00");

                entity.Property(e => e.LastPasswdGen)
                    .HasColumnName("last_passwd_gen")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasColumnName("lastname")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.Optin)
                    .HasColumnName("optin")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Passwd)
                    .IsRequired()
                    .HasColumnName("passwd")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.PreselectDateRange)
                    .HasColumnName("preselect_date_range")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.StatsCompareFrom)
                    .HasColumnName("stats_compare_from")
                    .HasColumnType("date");

                entity.Property(e => e.StatsCompareOption)
                    .HasColumnName("stats_compare_option")
                    .HasColumnType("int(1) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.StatsCompareTo)
                    .HasColumnName("stats_compare_to")
                    .HasColumnType("date");

                entity.Property(e => e.StatsDateFrom)
                    .HasColumnName("stats_date_from")
                    .HasColumnType("date");

                entity.Property(e => e.StatsDateTo)
                    .HasColumnName("stats_date_to")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<PsEmployeeShop>(entity =>
            {
                entity.HasKey(e => new { e.IdEmployee, e.IdShop })
                    .HasName("PK_ps_employee_shop");

                entity.ToTable("ps_employee_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdEmployee)
                    .HasColumnName("id_employee")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsFeature>(entity =>
            {
                entity.HasKey(e => e.IdFeature)
                    .HasName("PK_ps_feature");

                entity.ToTable("ps_feature");

                entity.Property(e => e.IdFeature)
                    .HasColumnName("id_feature")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsFeatureLang>(entity =>
            {
                entity.HasKey(e => new { e.IdFeature, e.IdLang })
                    .HasName("PK_ps_feature_lang");

                entity.ToTable("ps_feature_lang");

                entity.HasIndex(e => new { e.IdLang, e.Name })
                    .HasName("id_lang");

                entity.Property(e => e.IdFeature)
                    .HasColumnName("id_feature")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsFeatureProduct>(entity =>
            {
                entity.HasKey(e => new { e.IdFeature, e.IdProduct })
                    .HasName("PK_ps_feature_product");

                entity.ToTable("ps_feature_product");

                entity.HasIndex(e => e.IdFeatureValue)
                    .HasName("id_feature_value");

                entity.HasIndex(e => e.IdProduct)
                    .HasName("id_product");

                entity.Property(e => e.IdFeature)
                    .HasColumnName("id_feature")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdFeatureValue)
                    .HasColumnName("id_feature_value")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsFeatureShop>(entity =>
            {
                entity.HasKey(e => new { e.IdFeature, e.IdShop })
                    .HasName("PK_ps_feature_shop");

                entity.ToTable("ps_feature_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdFeature)
                    .HasColumnName("id_feature")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsFeatureValue>(entity =>
            {
                entity.HasKey(e => e.IdFeatureValue)
                    .HasName("PK_ps_feature_value");

                entity.ToTable("ps_feature_value");

                entity.HasIndex(e => e.IdFeature)
                    .HasName("feature");

                entity.Property(e => e.IdFeatureValue)
                    .HasColumnName("id_feature_value")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Custom)
                    .HasColumnName("custom")
                    .HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.IdFeature)
                    .HasColumnName("id_feature")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsFeatureValueLang>(entity =>
            {
                entity.HasKey(e => new { e.IdFeatureValue, e.IdLang })
                    .HasName("PK_ps_feature_value_lang");

                entity.ToTable("ps_feature_value_lang");

                entity.Property(e => e.IdFeatureValue)
                    .HasColumnName("id_feature_value")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<PsGender>(entity =>
            {
                entity.HasKey(e => e.IdGender)
                    .HasName("PK_ps_gender");

                entity.ToTable("ps_gender");

                entity.Property(e => e.IdGender)
                    .HasColumnName("id_gender")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(1)");
            });

            modelBuilder.Entity<PsGenderLang>(entity =>
            {
                entity.HasKey(e => new { e.IdGender, e.IdLang })
                    .HasName("PK_ps_gender_lang");

                entity.ToTable("ps_gender_lang");

                entity.HasIndex(e => e.IdGender)
                    .HasName("id_gender");

                entity.Property(e => e.IdGender)
                    .HasColumnName("id_gender")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<PsGroup>(entity =>
            {
                entity.HasKey(e => e.IdGroup)
                    .HasName("PK_ps_group");

                entity.ToTable("ps_group");

                entity.Property(e => e.IdGroup)
                    .HasColumnName("id_group")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.PriceDisplayMethod)
                    .HasColumnName("price_display_method")
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Reduction)
                    .HasColumnName("reduction")
                    .HasColumnType("decimal(17,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.ShowPrices)
                    .HasColumnName("show_prices")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("1");
            });

            modelBuilder.Entity<PsGroupLang>(entity =>
            {
                entity.HasKey(e => new { e.IdGroup, e.IdLang })
                    .HasName("PK_ps_group_lang");

                entity.ToTable("ps_group_lang");

                entity.Property(e => e.IdGroup)
                    .HasColumnName("id_group")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<PsGroupReduction>(entity =>
            {
                entity.HasKey(e => e.IdGroupReduction)
                    .HasName("PK_ps_group_reduction");

                entity.ToTable("ps_group_reduction");

                entity.HasIndex(e => new { e.IdGroup, e.IdCategory })
                    .HasName("id_group")
                    .IsUnique();

                entity.Property(e => e.IdGroupReduction)
                    .HasColumnName("id_group_reduction")
                    .HasColumnType("mediumint(8) unsigned");

                entity.Property(e => e.IdCategory)
                    .HasColumnName("id_category")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdGroup)
                    .HasColumnName("id_group")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Reduction)
                    .HasColumnName("reduction")
                    .HasColumnType("decimal(4,3)");
            });

            modelBuilder.Entity<PsGroupShop>(entity =>
            {
                entity.HasKey(e => new { e.IdGroup, e.IdShop })
                    .HasName("PK_ps_group_shop");

                entity.ToTable("ps_group_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdGroup)
                    .HasColumnName("id_group")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsGuest>(entity =>
            {
                entity.HasKey(e => e.IdGuest)
                    .HasName("PK_ps_guest");

                entity.ToTable("ps_guest");

                entity.HasIndex(e => e.IdCustomer)
                    .HasName("id_customer");

                entity.HasIndex(e => e.IdOperatingSystem)
                    .HasName("id_operating_system");

                entity.HasIndex(e => e.IdWebBrowser)
                    .HasName("id_web_browser");

                entity.Property(e => e.IdGuest)
                    .HasColumnName("id_guest")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.AcceptLanguage)
                    .HasColumnName("accept_language")
                    .HasColumnType("varchar(8)");

                entity.Property(e => e.AdobeDirector)
                    .HasColumnName("adobe_director")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.AdobeFlash)
                    .HasColumnName("adobe_flash")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.AppleQuicktime)
                    .HasColumnName("apple_quicktime")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.IdCustomer)
                    .HasColumnName("id_customer")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdOperatingSystem)
                    .HasColumnName("id_operating_system")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdWebBrowser)
                    .HasColumnName("id_web_browser")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Javascript)
                    .HasColumnName("javascript")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.MobileTheme)
                    .HasColumnName("mobile_theme")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.RealPlayer)
                    .HasColumnName("real_player")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.ScreenColor)
                    .HasColumnName("screen_color")
                    .HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.ScreenResolutionX)
                    .HasColumnName("screen_resolution_x")
                    .HasColumnType("smallint(5) unsigned");

                entity.Property(e => e.ScreenResolutionY)
                    .HasColumnName("screen_resolution_y")
                    .HasColumnType("smallint(5) unsigned");

                entity.Property(e => e.SunJava)
                    .HasColumnName("sun_java")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.WindowsMedia)
                    .HasColumnName("windows_media")
                    .HasColumnType("tinyint(1)");
            });

            modelBuilder.Entity<PsHomeslider>(entity =>
            {
                entity.HasKey(e => new { e.IdHomesliderSlides, e.IdShop })
                    .HasName("PK_ps_homeslider");

                entity.ToTable("ps_homeslider");

                entity.Property(e => e.IdHomesliderSlides)
                    .HasColumnName("id_homeslider_slides")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsHomesliderSlides>(entity =>
            {
                entity.HasKey(e => e.IdHomesliderSlides)
                    .HasName("PK_ps_homeslider_slides");

                entity.ToTable("ps_homeslider_slides");

                entity.Property(e => e.IdHomesliderSlides)
                    .HasColumnName("id_homeslider_slides")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsHomesliderSlidesLang>(entity =>
            {
                entity.HasKey(e => new { e.IdHomesliderSlides, e.IdLang })
                    .HasName("PK_ps_homeslider_slides_lang");

                entity.ToTable("ps_homeslider_slides_lang");

                entity.Property(e => e.IdHomesliderSlides)
                    .HasColumnName("id_homeslider_slides")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.Image)
                    .IsRequired()
                    .HasColumnName("image")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Legend)
                    .IsRequired()
                    .HasColumnName("legend")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasColumnName("url")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<PsHook>(entity =>
            {
                entity.HasKey(e => e.IdHook)
                    .HasName("PK_ps_hook");

                entity.ToTable("ps_hook");

                entity.HasIndex(e => e.Name)
                    .HasName("hook_name")
                    .IsUnique();

                entity.Property(e => e.IdHook)
                    .HasColumnName("id_hook")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.LiveEdit)
                    .HasColumnName("live_edit")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsHookAlias>(entity =>
            {
                entity.HasKey(e => e.IdHookAlias)
                    .HasName("PK_ps_hook_alias");

                entity.ToTable("ps_hook_alias");

                entity.HasIndex(e => e.Alias)
                    .HasName("alias")
                    .IsUnique();

                entity.Property(e => e.IdHookAlias)
                    .HasColumnName("id_hook_alias")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Alias)
                    .IsRequired()
                    .HasColumnName("alias")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsHookModule>(entity =>
            {
                entity.HasKey(e => new { e.IdModule, e.IdShop, e.IdHook })
                    .HasName("PK_ps_hook_module");

                entity.ToTable("ps_hook_module");

                entity.HasIndex(e => e.IdHook)
                    .HasName("id_hook");

                entity.HasIndex(e => e.IdModule)
                    .HasName("id_module");

                entity.HasIndex(e => new { e.IdShop, e.Position })
                    .HasName("position");

                entity.Property(e => e.IdModule)
                    .HasColumnName("id_module")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdHook)
                    .HasColumnName("id_hook")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasColumnType("tinyint(2) unsigned");
            });

            modelBuilder.Entity<PsHookModuleExceptions>(entity =>
            {
                entity.HasKey(e => e.IdHookModuleExceptions)
                    .HasName("PK_ps_hook_module_exceptions");

                entity.ToTable("ps_hook_module_exceptions");

                entity.HasIndex(e => e.IdHook)
                    .HasName("id_hook");

                entity.HasIndex(e => e.IdModule)
                    .HasName("id_module");

                entity.Property(e => e.IdHookModuleExceptions)
                    .HasColumnName("id_hook_module_exceptions")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.FileName)
                    .HasColumnName("file_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.IdHook)
                    .HasColumnName("id_hook")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdModule)
                    .HasColumnName("id_module")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");
            });

            modelBuilder.Entity<PsImage>(entity =>
            {
                entity.HasKey(e => e.IdImage)
                    .HasName("PK_ps_image");

                entity.ToTable("ps_image");

                entity.HasIndex(e => e.IdProduct)
                    .HasName("image_product");

                entity.HasIndex(e => new { e.IdProduct, e.Cover })
                    .HasName("id_product_cover")
                    .IsUnique();

                entity.HasIndex(e => new { e.IdImage, e.IdProduct, e.Cover })
                    .HasName("idx_product_image")
                    .IsUnique();

                entity.Property(e => e.IdImage)
                    .HasColumnName("id_image")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Cover)
                    .IsRequired()
                    .HasColumnName("cover")
                    .HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasColumnType("smallint(2) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsImageLang>(entity =>
            {
                entity.HasKey(e => new { e.IdImage, e.IdLang })
                    .HasName("PK_ps_image_lang");

                entity.ToTable("ps_image_lang");

                entity.HasIndex(e => e.IdImage)
                    .HasName("id_image");

                entity.Property(e => e.IdImage)
                    .HasColumnName("id_image")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Legend)
                    .HasColumnName("legend")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsImageShop>(entity =>
            {
                entity.HasKey(e => new { e.IdImage, e.IdShop })
                    .HasName("PK_ps_image_shop");

                entity.ToTable("ps_image_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.HasIndex(e => new { e.IdProduct, e.IdShop, e.Cover })
                    .HasName("id_product")
                    .IsUnique();

                entity.Property(e => e.IdImage)
                    .HasColumnName("id_image")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Cover)
                    .IsRequired()
                    .HasColumnName("cover")
                    .HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsImageType>(entity =>
            {
                entity.HasKey(e => e.IdImageType)
                    .HasName("PK_ps_image_type");

                entity.ToTable("ps_image_type");

                entity.HasIndex(e => e.Name)
                    .HasName("image_type_name");

                entity.Property(e => e.IdImageType)
                    .HasColumnName("id_image_type")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Categories)
                    .HasColumnName("categories")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Height)
                    .HasColumnName("height")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Manufacturers)
                    .HasColumnName("manufacturers")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Products)
                    .HasColumnName("products")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Scenes)
                    .HasColumnName("scenes")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Stores)
                    .HasColumnName("stores")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Suppliers)
                    .HasColumnName("suppliers")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Width)
                    .HasColumnName("width")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsImportMatch>(entity =>
            {
                entity.HasKey(e => e.IdImportMatch)
                    .HasName("PK_ps_import_match");

                entity.ToTable("ps_import_match");

                entity.Property(e => e.IdImportMatch)
                    .HasColumnName("id_import_match")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Match)
                    .IsRequired()
                    .HasColumnName("match")
                    .HasColumnType("text");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.Skip)
                    .HasColumnName("skip")
                    .HasColumnType("int(2)");
            });

            modelBuilder.Entity<PsInfo>(entity =>
            {
                entity.HasKey(e => e.IdInfo)
                    .HasName("PK_ps_info");

                entity.ToTable("ps_info");

                entity.Property(e => e.IdInfo)
                    .HasColumnName("id_info")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsInfoLang>(entity =>
            {
                entity.HasKey(e => new { e.IdInfo, e.IdLang })
                    .HasName("PK_ps_info_lang");

                entity.ToTable("ps_info_lang");

                entity.Property(e => e.IdInfo)
                    .HasColumnName("id_info")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasColumnName("text")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<PsLang>(entity =>
            {
                entity.HasKey(e => e.IdLang)
                    .HasName("PK_ps_lang");

                entity.ToTable("ps_lang");

                entity.HasIndex(e => e.IsoCode)
                    .HasName("lang_iso_code");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(3) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DateFormatFull)
                    .HasColumnName("date_format_full")
                    .HasColumnType("char(32)")
                    .HasDefaultValueSql("Y-m-d H:i:s");

                entity.Property(e => e.DateFormatLite)
                    .HasColumnName("date_format_lite")
                    .HasColumnType("char(32)")
                    .HasDefaultValueSql("Y-m-d");

                entity.Property(e => e.IsRtl)
                    .HasColumnName("is_rtl")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IsoCode)
                    .HasColumnName("iso_code")
                    .HasColumnType("char(2)");

                entity.Property(e => e.LanguageCode)
                    .HasColumnName("language_code")
                    .HasColumnType("char(5)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<PsLangShop>(entity =>
            {
                entity.HasKey(e => new { e.IdLang, e.IdShop })
                    .HasName("PK_ps_lang_shop");

                entity.ToTable("ps_lang_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsLayeredCategory>(entity =>
            {
                entity.HasKey(e => e.IdLayeredCategory)
                    .HasName("PK_ps_layered_category");

                entity.ToTable("ps_layered_category");

                entity.Property(e => e.IdLayeredCategory)
                    .HasColumnName("id_layered_category")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.FilterShowLimit)
                    .HasColumnName("filter_show_limit")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.FilterType)
                    .HasColumnName("filter_type")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdCategory)
                    .HasColumnName("id_category")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdValue)
                    .HasColumnName("id_value")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsLayeredFilter>(entity =>
            {
                entity.HasKey(e => e.IdLayeredFilter)
                    .HasName("PK_ps_layered_filter");

                entity.ToTable("ps_layered_filter");

                entity.Property(e => e.IdLayeredFilter)
                    .HasColumnName("id_layered_filter")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.Filters)
                    .HasColumnName("filters")
                    .HasColumnType("text");

                entity.Property(e => e.NCategories)
                    .HasColumnName("n_categories")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsLayeredFilterShop>(entity =>
            {
                entity.HasKey(e => new { e.IdLayeredFilter, e.IdShop })
                    .HasName("PK_ps_layered_filter_shop");

                entity.ToTable("ps_layered_filter_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdLayeredFilter)
                    .HasColumnName("id_layered_filter")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsLayeredFriendlyUrl>(entity =>
            {
                entity.HasKey(e => e.IdLayeredFriendlyUrl)
                    .HasName("PK_ps_layered_friendly_url");

                entity.ToTable("ps_layered_friendly_url");

                entity.HasIndex(e => e.IdLang)
                    .HasName("id_lang");

                entity.HasIndex(e => e.UrlKey)
                    .HasName("url_key");

                entity.Property(e => e.IdLayeredFriendlyUrl)
                    .HasColumnName("id_layered_friendly_url")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Data)
                    .IsRequired()
                    .HasColumnName("data")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UrlKey)
                    .IsRequired()
                    .HasColumnName("url_key")
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<PsLayeredIndexableAttributeGroup>(entity =>
            {
                entity.HasKey(e => e.IdAttributeGroup)
                    .HasName("PK_ps_layered_indexable_attribute_group");

                entity.ToTable("ps_layered_indexable_attribute_group");

                entity.Property(e => e.IdAttributeGroup)
                    .HasColumnName("id_attribute_group")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Indexable)
                    .HasColumnName("indexable")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsLayeredIndexableAttributeGroupLangValue>(entity =>
            {
                entity.HasKey(e => new { e.IdAttributeGroup, e.IdLang })
                    .HasName("PK_ps_layered_indexable_attribute_group_lang_value");

                entity.ToTable("ps_layered_indexable_attribute_group_lang_value");

                entity.Property(e => e.IdAttributeGroup)
                    .HasColumnName("id_attribute_group")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MetaTitle)
                    .HasColumnName("meta_title")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.UrlName)
                    .HasColumnName("url_name")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsLayeredIndexableAttributeLangValue>(entity =>
            {
                entity.HasKey(e => new { e.IdAttribute, e.IdLang })
                    .HasName("PK_ps_layered_indexable_attribute_lang_value");

                entity.ToTable("ps_layered_indexable_attribute_lang_value");

                entity.Property(e => e.IdAttribute)
                    .HasColumnName("id_attribute")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MetaTitle)
                    .HasColumnName("meta_title")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.UrlName)
                    .HasColumnName("url_name")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsLayeredIndexableFeature>(entity =>
            {
                entity.HasKey(e => e.IdFeature)
                    .HasName("PK_ps_layered_indexable_feature");

                entity.ToTable("ps_layered_indexable_feature");

                entity.Property(e => e.IdFeature)
                    .HasColumnName("id_feature")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Indexable)
                    .HasColumnName("indexable")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsLayeredIndexableFeatureLangValue>(entity =>
            {
                entity.HasKey(e => new { e.IdFeature, e.IdLang })
                    .HasName("PK_ps_layered_indexable_feature_lang_value");

                entity.ToTable("ps_layered_indexable_feature_lang_value");

                entity.Property(e => e.IdFeature)
                    .HasColumnName("id_feature")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MetaTitle)
                    .HasColumnName("meta_title")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.UrlName)
                    .IsRequired()
                    .HasColumnName("url_name")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsLayeredIndexableFeatureValueLangValue>(entity =>
            {
                entity.HasKey(e => new { e.IdFeatureValue, e.IdLang })
                    .HasName("PK_ps_layered_indexable_feature_value_lang_value");

                entity.ToTable("ps_layered_indexable_feature_value_lang_value");

                entity.Property(e => e.IdFeatureValue)
                    .HasColumnName("id_feature_value")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MetaTitle)
                    .HasColumnName("meta_title")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.UrlName)
                    .HasColumnName("url_name")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsLayeredPriceIndex>(entity =>
            {
                entity.HasKey(e => new { e.IdProduct, e.IdCurrency, e.IdShop })
                    .HasName("PK_ps_layered_price_index");

                entity.ToTable("ps_layered_price_index");

                entity.HasIndex(e => e.IdCurrency)
                    .HasName("id_currency");

                entity.HasIndex(e => e.PriceMax)
                    .HasName("price_max");

                entity.HasIndex(e => e.PriceMin)
                    .HasName("price_min");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdCurrency)
                    .HasColumnName("id_currency")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PriceMax)
                    .HasColumnName("price_max")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PriceMin)
                    .HasColumnName("price_min")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<PsLayeredProductAttribute>(entity =>
            {
                entity.HasKey(e => new { e.IdAttribute, e.IdProduct, e.IdShop })
                    .HasName("PK_ps_layered_product_attribute");

                entity.ToTable("ps_layered_product_attribute");

                entity.HasIndex(e => new { e.IdAttributeGroup, e.IdAttribute, e.IdProduct, e.IdShop })
                    .HasName("id_attribute_group")
                    .IsUnique();

                entity.Property(e => e.IdAttribute)
                    .HasColumnName("id_attribute")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdAttributeGroup)
                    .HasColumnName("id_attribute_group")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsLinksmenutop>(entity =>
            {
                entity.HasKey(e => e.IdLinksmenutop)
                    .HasName("PK_ps_linksmenutop");

                entity.ToTable("ps_linksmenutop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdLinksmenutop)
                    .HasColumnName("id_linksmenutop")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.NewWindow)
                    .HasColumnName("new_window")
                    .HasColumnType("tinyint(1)");
            });

            modelBuilder.Entity<PsLog>(entity =>
            {
                entity.HasKey(e => e.IdLog)
                    .HasName("PK_ps_log");

                entity.ToTable("ps_log");

                entity.Property(e => e.IdLog)
                    .HasColumnName("id_log")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.ErrorCode)
                    .HasColumnName("error_code")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdEmployee)
                    .HasColumnName("id_employee")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasColumnName("message")
                    .HasColumnType("text");

                entity.Property(e => e.ObjectId)
                    .HasColumnName("object_id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.ObjectType)
                    .HasColumnName("object_type")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.Severity)
                    .HasColumnName("severity")
                    .HasColumnType("tinyint(1)");
            });

            modelBuilder.Entity<PsMail>(entity =>
            {
                entity.HasKey(e => e.IdMail)
                    .HasName("PK_ps_mail");

                entity.ToTable("ps_mail");

                entity.HasIndex(e => e.Recipient)
                    .HasName("recipient");

                entity.Property(e => e.IdMail)
                    .HasColumnName("id_mail")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Recipient)
                    .IsRequired()
                    .HasColumnName("recipient")
                    .HasColumnType("varchar(126)");

                entity.Property(e => e.Subject)
                    .IsRequired()
                    .HasColumnName("subject")
                    .HasColumnType("varchar(254)");

                entity.Property(e => e.Template)
                    .IsRequired()
                    .HasColumnName("template")
                    .HasColumnType("varchar(62)");
            });

            modelBuilder.Entity<PsManufacturer>(entity =>
            {
                entity.HasKey(e => e.IdManufacturer)
                    .HasName("PK_ps_manufacturer");

                entity.ToTable("ps_manufacturer");

                entity.Property(e => e.IdManufacturer)
                    .HasColumnName("id_manufacturer")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsManufacturerLang>(entity =>
            {
                entity.HasKey(e => new { e.IdManufacturer, e.IdLang })
                    .HasName("PK_ps_manufacturer_lang");

                entity.ToTable("ps_manufacturer_lang");

                entity.Property(e => e.IdManufacturer)
                    .HasColumnName("id_manufacturer")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.MetaDescription)
                    .HasColumnName("meta_description")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.MetaKeywords)
                    .HasColumnName("meta_keywords")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.MetaTitle)
                    .HasColumnName("meta_title")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.ShortDescription)
                    .HasColumnName("short_description")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<PsManufacturerShop>(entity =>
            {
                entity.HasKey(e => new { e.IdManufacturer, e.IdShop })
                    .HasName("PK_ps_manufacturer_shop");

                entity.ToTable("ps_manufacturer_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdManufacturer)
                    .HasColumnName("id_manufacturer")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsMemcachedServers>(entity =>
            {
                entity.HasKey(e => e.IdMemcachedServer)
                    .HasName("PK_ps_memcached_servers");

                entity.ToTable("ps_memcached_servers");

                entity.Property(e => e.IdMemcachedServer)
                    .HasColumnName("id_memcached_server")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Ip)
                    .IsRequired()
                    .HasColumnName("ip")
                    .HasColumnType("varchar(254)");

                entity.Property(e => e.Port)
                    .HasColumnName("port")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Weight)
                    .HasColumnName("weight")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsMessage>(entity =>
            {
                entity.HasKey(e => e.IdMessage)
                    .HasName("PK_ps_message");

                entity.ToTable("ps_message");

                entity.HasIndex(e => e.IdCart)
                    .HasName("id_cart");

                entity.HasIndex(e => e.IdCustomer)
                    .HasName("id_customer");

                entity.HasIndex(e => e.IdEmployee)
                    .HasName("id_employee");

                entity.HasIndex(e => e.IdOrder)
                    .HasName("message_order");

                entity.Property(e => e.IdMessage)
                    .HasColumnName("id_message")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdCart)
                    .HasColumnName("id_cart")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCustomer)
                    .HasColumnName("id_customer")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdEmployee)
                    .HasColumnName("id_employee")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdOrder)
                    .HasColumnName("id_order")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasColumnName("message")
                    .HasColumnType("text");

                entity.Property(e => e.Private)
                    .HasColumnName("private")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("1");
            });

            modelBuilder.Entity<PsMessageReaded>(entity =>
            {
                entity.HasKey(e => new { e.IdMessage, e.IdEmployee })
                    .HasName("PK_ps_message_readed");

                entity.ToTable("ps_message_readed");

                entity.Property(e => e.IdMessage)
                    .HasColumnName("id_message")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdEmployee)
                    .HasColumnName("id_employee")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PsMeta>(entity =>
            {
                entity.HasKey(e => e.IdMeta)
                    .HasName("PK_ps_meta");

                entity.ToTable("ps_meta");

                entity.HasIndex(e => e.Page)
                    .HasName("page")
                    .IsUnique();

                entity.Property(e => e.IdMeta)
                    .HasColumnName("id_meta")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Configurable)
                    .HasColumnName("configurable")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Page)
                    .IsRequired()
                    .HasColumnName("page")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsMetaLang>(entity =>
            {
                entity.HasKey(e => new { e.IdMeta, e.IdShop, e.IdLang })
                    .HasName("PK_ps_meta_lang");

                entity.ToTable("ps_meta_lang");

                entity.HasIndex(e => e.IdLang)
                    .HasName("id_lang");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdMeta)
                    .HasColumnName("id_meta")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Keywords)
                    .HasColumnName("keywords")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.UrlRewrite)
                    .IsRequired()
                    .HasColumnName("url_rewrite")
                    .HasColumnType("varchar(254)");
            });

            modelBuilder.Entity<PsModule>(entity =>
            {
                entity.HasKey(e => e.IdModule)
                    .HasName("PK_ps_module");

                entity.ToTable("ps_module");

                entity.HasIndex(e => e.Name)
                    .HasName("name");

                entity.Property(e => e.IdModule)
                    .HasColumnName("id_module")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Version)
                    .IsRequired()
                    .HasColumnName("version")
                    .HasColumnType("varchar(8)");
            });

            modelBuilder.Entity<PsModuleAccess>(entity =>
            {
                entity.HasKey(e => new { e.IdProfile, e.IdModule })
                    .HasName("PK_ps_module_access");

                entity.ToTable("ps_module_access");

                entity.Property(e => e.IdProfile)
                    .HasColumnName("id_profile")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdModule)
                    .HasColumnName("id_module")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Configure)
                    .HasColumnName("configure")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Uninstall)
                    .HasColumnName("uninstall")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.View)
                    .HasColumnName("view")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsModuleCountry>(entity =>
            {
                entity.HasKey(e => new { e.IdModule, e.IdShop, e.IdCountry })
                    .HasName("PK_ps_module_country");

                entity.ToTable("ps_module_country");

                entity.Property(e => e.IdModule)
                    .HasColumnName("id_module")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdCountry)
                    .HasColumnName("id_country")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsModuleCurrency>(entity =>
            {
                entity.HasKey(e => new { e.IdModule, e.IdShop, e.IdCurrency })
                    .HasName("PK_ps_module_currency");

                entity.ToTable("ps_module_currency");

                entity.HasIndex(e => e.IdModule)
                    .HasName("id_module");

                entity.Property(e => e.IdModule)
                    .HasColumnName("id_module")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdCurrency)
                    .HasColumnName("id_currency")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<PsModuleGroup>(entity =>
            {
                entity.HasKey(e => new { e.IdModule, e.IdShop, e.IdGroup })
                    .HasName("PK_ps_module_group");

                entity.ToTable("ps_module_group");

                entity.Property(e => e.IdModule)
                    .HasColumnName("id_module")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdGroup)
                    .HasColumnName("id_group")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsModulePreference>(entity =>
            {
                entity.HasKey(e => e.IdModulePreference)
                    .HasName("PK_ps_module_preference");

                entity.ToTable("ps_module_preference");

                entity.HasIndex(e => new { e.IdEmployee, e.Module })
                    .HasName("employee_module")
                    .IsUnique();

                entity.Property(e => e.IdModulePreference)
                    .HasColumnName("id_module_preference")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Favorite)
                    .HasColumnName("favorite")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.IdEmployee)
                    .HasColumnName("id_employee")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Interest)
                    .HasColumnName("interest")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.Module)
                    .IsRequired()
                    .HasColumnName("module")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<PsModuleShop>(entity =>
            {
                entity.HasKey(e => new { e.IdModule, e.IdShop })
                    .HasName("PK_ps_module_shop");

                entity.ToTable("ps_module_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdModule)
                    .HasColumnName("id_module")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.EnableDevice)
                    .HasColumnName("enable_device")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("7");
            });

            modelBuilder.Entity<PsModulesPerfs>(entity =>
            {
                entity.HasKey(e => e.IdModulesPerfs)
                    .HasName("PK_ps_modules_perfs");

                entity.ToTable("ps_modules_perfs");

                entity.HasIndex(e => e.Session)
                    .HasName("session");

                entity.Property(e => e.IdModulesPerfs)
                    .HasColumnName("id_modules_perfs")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.MemoryEnd)
                    .HasColumnName("memory_end")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.MemoryStart)
                    .HasColumnName("memory_start")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Method)
                    .IsRequired()
                    .HasColumnName("method")
                    .HasColumnType("varchar(126)");

                entity.Property(e => e.Module)
                    .IsRequired()
                    .HasColumnName("module")
                    .HasColumnType("varchar(62)");

                entity.Property(e => e.Session)
                    .HasColumnName("session")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.TimeEnd)
                    .HasColumnName("time_end")
                    .HasColumnType("double unsigned");

                entity.Property(e => e.TimeStart)
                    .HasColumnName("time_start")
                    .HasColumnType("double unsigned");
            });

            modelBuilder.Entity<PsNewsletter>(entity =>
            {
                entity.ToTable("ps_newsletter");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(6)");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.HttpReferer)
                    .HasColumnName("http_referer")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdShopGroup)
                    .HasColumnName("id_shop_group")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IpRegistrationNewsletter)
                    .IsRequired()
                    .HasColumnName("ip_registration_newsletter")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.NewsletterDateAdd)
                    .HasColumnName("newsletter_date_add")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PsOperatingSystem>(entity =>
            {
                entity.HasKey(e => e.IdOperatingSystem)
                    .HasName("PK_ps_operating_system");

                entity.ToTable("ps_operating_system");

                entity.Property(e => e.IdOperatingSystem)
                    .HasColumnName("id_operating_system")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsOrderCarrier>(entity =>
            {
                entity.HasKey(e => e.IdOrderCarrier)
                    .HasName("PK_ps_order_carrier");

                entity.ToTable("ps_order_carrier");

                entity.HasIndex(e => e.IdCarrier)
                    .HasName("id_carrier");

                entity.HasIndex(e => e.IdOrder)
                    .HasName("id_order");

                entity.HasIndex(e => e.IdOrderInvoice)
                    .HasName("id_order_invoice");

                entity.Property(e => e.IdOrderCarrier)
                    .HasColumnName("id_order_carrier")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdCarrier)
                    .HasColumnName("id_carrier")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdOrder)
                    .HasColumnName("id_order")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdOrderInvoice)
                    .HasColumnName("id_order_invoice")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.ShippingCostTaxExcl)
                    .HasColumnName("shipping_cost_tax_excl")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.ShippingCostTaxIncl)
                    .HasColumnName("shipping_cost_tax_incl")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.TrackingNumber)
                    .HasColumnName("tracking_number")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Weight)
                    .HasColumnName("weight")
                    .HasColumnType("decimal(20,6)");
            });

            modelBuilder.Entity<PsOrderCartRule>(entity =>
            {
                entity.HasKey(e => e.IdOrderCartRule)
                    .HasName("PK_ps_order_cart_rule");

                entity.ToTable("ps_order_cart_rule");

                entity.HasIndex(e => e.IdCartRule)
                    .HasName("id_cart_rule");

                entity.HasIndex(e => e.IdOrder)
                    .HasName("id_order");

                entity.Property(e => e.IdOrderCartRule)
                    .HasColumnName("id_order_cart_rule")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.FreeShipping)
                    .HasColumnName("free_shipping")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdCartRule)
                    .HasColumnName("id_cart_rule")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdOrder)
                    .HasColumnName("id_order")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdOrderInvoice)
                    .HasColumnName("id_order_invoice")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(254)");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasColumnType("decimal(17,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.ValueTaxExcl)
                    .HasColumnName("value_tax_excl")
                    .HasColumnType("decimal(17,2)")
                    .HasDefaultValueSql("0.00");
            });

            modelBuilder.Entity<PsOrderDetail>(entity =>
            {
                entity.HasKey(e => e.IdOrderDetail)
                    .HasName("PK_ps_order_detail");

                entity.ToTable("ps_order_detail");

                entity.HasIndex(e => e.IdOrder)
                    .HasName("order_detail_order");

                entity.HasIndex(e => e.IdTaxRulesGroup)
                    .HasName("id_tax_rules_group");

                entity.HasIndex(e => e.ProductAttributeId)
                    .HasName("product_attribute_id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.HasIndex(e => new { e.IdOrder, e.IdOrderDetail })
                    .HasName("id_order_id_order_detail");

                entity.Property(e => e.IdOrderDetail)
                    .HasColumnName("id_order_detail")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DiscountQuantityApplied)
                    .HasColumnName("discount_quantity_applied")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DownloadDeadline)
                    .HasColumnName("download_deadline")
                    .HasColumnType("datetime");

                entity.Property(e => e.DownloadHash)
                    .HasColumnName("download_hash")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.DownloadNb)
                    .HasColumnName("download_nb")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Ecotax)
                    .HasColumnName("ecotax")
                    .HasColumnType("decimal(21,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.EcotaxTaxRate)
                    .HasColumnName("ecotax_tax_rate")
                    .HasColumnType("decimal(5,3)")
                    .HasDefaultValueSql("0.000");

                entity.Property(e => e.GroupReduction)
                    .HasColumnName("group_reduction")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.IdOrder)
                    .HasColumnName("id_order")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdOrderInvoice)
                    .HasColumnName("id_order_invoice")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdTaxRulesGroup)
                    .HasColumnName("id_tax_rules_group")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdWarehouse)
                    .HasColumnName("id_warehouse")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.OriginalProductPrice)
                    .HasColumnName("original_product_price")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.OriginalWholesalePrice)
                    .HasColumnName("original_wholesale_price")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.ProductAttributeId)
                    .HasColumnName("product_attribute_id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.ProductEan13)
                    .HasColumnName("product_ean13")
                    .HasColumnType("varchar(13)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.ProductName)
                    .IsRequired()
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ProductPrice)
                    .HasColumnName("product_price")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.ProductQuantity)
                    .HasColumnName("product_quantity")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ProductQuantityDiscount)
                    .HasColumnName("product_quantity_discount")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.ProductQuantityInStock)
                    .HasColumnName("product_quantity_in_stock")
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ProductQuantityRefunded)
                    .HasColumnName("product_quantity_refunded")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ProductQuantityReinjected)
                    .HasColumnName("product_quantity_reinjected")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ProductQuantityReturn)
                    .HasColumnName("product_quantity_return")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ProductReference)
                    .HasColumnName("product_reference")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.ProductSupplierReference)
                    .HasColumnName("product_supplier_reference")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.ProductUpc)
                    .HasColumnName("product_upc")
                    .HasColumnType("varchar(12)");

                entity.Property(e => e.ProductWeight)
                    .HasColumnName("product_weight")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.PurchaseSupplierPrice)
                    .HasColumnName("purchase_supplier_price")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.ReductionAmount)
                    .HasColumnName("reduction_amount")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.ReductionAmountTaxExcl)
                    .HasColumnName("reduction_amount_tax_excl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.ReductionAmountTaxIncl)
                    .HasColumnName("reduction_amount_tax_incl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.ReductionPercent)
                    .HasColumnName("reduction_percent")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.TaxComputationMethod)
                    .HasColumnName("tax_computation_method")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.TaxName)
                    .IsRequired()
                    .HasColumnName("tax_name")
                    .HasColumnType("varchar(16)");

                entity.Property(e => e.TaxRate)
                    .HasColumnName("tax_rate")
                    .HasColumnType("decimal(10,3)")
                    .HasDefaultValueSql("0.000");

                entity.Property(e => e.TotalPriceTaxExcl)
                    .HasColumnName("total_price_tax_excl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalPriceTaxIncl)
                    .HasColumnName("total_price_tax_incl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalShippingPriceTaxExcl)
                    .HasColumnName("total_shipping_price_tax_excl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalShippingPriceTaxIncl)
                    .HasColumnName("total_shipping_price_tax_incl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.UnitPriceTaxExcl)
                    .HasColumnName("unit_price_tax_excl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.UnitPriceTaxIncl)
                    .HasColumnName("unit_price_tax_incl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");
            });

            modelBuilder.Entity<PsOrderHistory>(entity =>
            {
                entity.HasKey(e => e.IdOrderHistory)
                    .HasName("PK_ps_order_history");

                entity.ToTable("ps_order_history");

                entity.HasIndex(e => e.IdEmployee)
                    .HasName("id_employee");

                entity.HasIndex(e => e.IdOrder)
                    .HasName("order_history_order");

                entity.HasIndex(e => e.IdOrderState)
                    .HasName("id_order_state");

                entity.Property(e => e.IdOrderHistory)
                    .HasColumnName("id_order_history")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdEmployee)
                    .HasColumnName("id_employee")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdOrder)
                    .HasColumnName("id_order")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdOrderState)
                    .HasColumnName("id_order_state")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsOrderInvoice>(entity =>
            {
                entity.HasKey(e => e.IdOrderInvoice)
                    .HasName("PK_ps_order_invoice");

                entity.ToTable("ps_order_invoice");

                entity.HasIndex(e => e.IdOrder)
                    .HasName("id_order");

                entity.Property(e => e.IdOrderInvoice)
                    .HasColumnName("id_order_invoice")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeliveryAddress)
                    .HasColumnName("delivery_address")
                    .HasColumnType("text");

                entity.Property(e => e.DeliveryDate)
                    .HasColumnName("delivery_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeliveryNumber)
                    .HasColumnName("delivery_number")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdOrder)
                    .HasColumnName("id_order")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InvoiceAddress)
                    .HasColumnName("invoice_address")
                    .HasColumnType("text");

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasColumnType("text");

                entity.Property(e => e.Number)
                    .HasColumnName("number")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ShippingTaxComputationMethod)
                    .HasColumnName("shipping_tax_computation_method")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.ShopAddress)
                    .HasColumnName("shop_address")
                    .HasColumnType("text");

                entity.Property(e => e.TotalDiscountTaxExcl)
                    .HasColumnName("total_discount_tax_excl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalDiscountTaxIncl)
                    .HasColumnName("total_discount_tax_incl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalPaidTaxExcl)
                    .HasColumnName("total_paid_tax_excl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalPaidTaxIncl)
                    .HasColumnName("total_paid_tax_incl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalProducts)
                    .HasColumnName("total_products")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalProductsWt)
                    .HasColumnName("total_products_wt")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalShippingTaxExcl)
                    .HasColumnName("total_shipping_tax_excl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalShippingTaxIncl)
                    .HasColumnName("total_shipping_tax_incl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalWrappingTaxExcl)
                    .HasColumnName("total_wrapping_tax_excl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalWrappingTaxIncl)
                    .HasColumnName("total_wrapping_tax_incl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");
            });

            modelBuilder.Entity<PsOrderInvoicePayment>(entity =>
            {
                entity.HasKey(e => new { e.IdOrderInvoice, e.IdOrderPayment })
                    .HasName("PK_ps_order_invoice_payment");

                entity.ToTable("ps_order_invoice_payment");

                entity.HasIndex(e => e.IdOrder)
                    .HasName("id_order");

                entity.HasIndex(e => e.IdOrderPayment)
                    .HasName("order_payment");

                entity.Property(e => e.IdOrderInvoice)
                    .HasColumnName("id_order_invoice")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdOrderPayment)
                    .HasColumnName("id_order_payment")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdOrder)
                    .HasColumnName("id_order")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsOrderMessage>(entity =>
            {
                entity.HasKey(e => e.IdOrderMessage)
                    .HasName("PK_ps_order_message");

                entity.ToTable("ps_order_message");

                entity.Property(e => e.IdOrderMessage)
                    .HasColumnName("id_order_message")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PsOrderMessageLang>(entity =>
            {
                entity.HasKey(e => new { e.IdOrderMessage, e.IdLang })
                    .HasName("PK_ps_order_message_lang");

                entity.ToTable("ps_order_message_lang");

                entity.Property(e => e.IdOrderMessage)
                    .HasColumnName("id_order_message")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasColumnName("message")
                    .HasColumnType("text");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsOrderPayment>(entity =>
            {
                entity.HasKey(e => e.IdOrderPayment)
                    .HasName("PK_ps_order_payment");

                entity.ToTable("ps_order_payment");

                entity.HasIndex(e => e.OrderReference)
                    .HasName("order_reference");

                entity.Property(e => e.IdOrderPayment)
                    .HasColumnName("id_order_payment")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CardBrand)
                    .HasColumnName("card_brand")
                    .HasColumnType("varchar(254)");

                entity.Property(e => e.CardExpiration)
                    .HasColumnName("card_expiration")
                    .HasColumnType("char(7)");

                entity.Property(e => e.CardHolder)
                    .HasColumnName("card_holder")
                    .HasColumnType("varchar(254)");

                entity.Property(e => e.CardNumber)
                    .HasColumnName("card_number")
                    .HasColumnType("varchar(254)");

                entity.Property(e => e.ConversionRate)
                    .HasColumnName("conversion_rate")
                    .HasColumnType("decimal(13,6)")
                    .HasDefaultValueSql("1.000000");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdCurrency)
                    .HasColumnName("id_currency")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.OrderReference)
                    .HasColumnName("order_reference")
                    .HasColumnType("varchar(9)");

                entity.Property(e => e.PaymentMethod)
                    .IsRequired()
                    .HasColumnName("payment_method")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.TransactionId)
                    .HasColumnName("transaction_id")
                    .HasColumnType("varchar(254)");
            });

            modelBuilder.Entity<PsOrderReturn>(entity =>
            {
                entity.HasKey(e => e.IdOrderReturn)
                    .HasName("PK_ps_order_return");

                entity.ToTable("ps_order_return");

                entity.HasIndex(e => e.IdCustomer)
                    .HasName("order_return_customer");

                entity.HasIndex(e => e.IdOrder)
                    .HasName("id_order");

                entity.Property(e => e.IdOrderReturn)
                    .HasColumnName("id_order_return")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdCustomer)
                    .HasColumnName("id_customer")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdOrder)
                    .HasColumnName("id_order")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Question)
                    .IsRequired()
                    .HasColumnName("question")
                    .HasColumnType("text");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("1");
            });

            modelBuilder.Entity<PsOrderReturnDetail>(entity =>
            {
                entity.HasKey(e => new { e.IdOrderReturn, e.IdOrderDetail, e.IdCustomization })
                    .HasName("PK_ps_order_return_detail");

                entity.ToTable("ps_order_return_detail");

                entity.Property(e => e.IdOrderReturn)
                    .HasColumnName("id_order_return")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdOrderDetail)
                    .HasColumnName("id_order_detail")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCustomization)
                    .HasColumnName("id_customization")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ProductQuantity)
                    .HasColumnName("product_quantity")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsOrderReturnState>(entity =>
            {
                entity.HasKey(e => e.IdOrderReturnState)
                    .HasName("PK_ps_order_return_state");

                entity.ToTable("ps_order_return_state");

                entity.Property(e => e.IdOrderReturnState)
                    .HasColumnName("id_order_return_state")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<PsOrderReturnStateLang>(entity =>
            {
                entity.HasKey(e => new { e.IdOrderReturnState, e.IdLang })
                    .HasName("PK_ps_order_return_state_lang");

                entity.ToTable("ps_order_return_state_lang");

                entity.Property(e => e.IdOrderReturnState)
                    .HasColumnName("id_order_return_state")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsOrderSlip>(entity =>
            {
                entity.HasKey(e => e.IdOrderSlip)
                    .HasName("PK_ps_order_slip");

                entity.ToTable("ps_order_slip");

                entity.HasIndex(e => e.IdCustomer)
                    .HasName("order_slip_customer");

                entity.HasIndex(e => e.IdOrder)
                    .HasName("id_order");

                entity.Property(e => e.IdOrderSlip)
                    .HasColumnName("id_order_slip")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ConversionRate)
                    .HasColumnName("conversion_rate")
                    .HasColumnType("decimal(13,6)")
                    .HasDefaultValueSql("1.000000");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdCustomer)
                    .HasColumnName("id_customer")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdOrder)
                    .HasColumnName("id_order")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.OrderSlipType)
                    .HasColumnName("order_slip_type")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Partial)
                    .HasColumnName("partial")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.ShippingCost)
                    .HasColumnName("shipping_cost")
                    .HasColumnType("tinyint(3) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ShippingCostAmount)
                    .HasColumnName("shipping_cost_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.TotalProductsTaxExcl)
                    .HasColumnName("total_products_tax_excl")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.TotalProductsTaxIncl)
                    .HasColumnName("total_products_tax_incl")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.TotalShippingTaxExcl)
                    .HasColumnName("total_shipping_tax_excl")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.TotalShippingTaxIncl)
                    .HasColumnName("total_shipping_tax_incl")
                    .HasColumnType("decimal(20,6)");
            });

            modelBuilder.Entity<PsOrderSlipDetail>(entity =>
            {
                entity.HasKey(e => new { e.IdOrderSlip, e.IdOrderDetail })
                    .HasName("PK_ps_order_slip_detail");

                entity.ToTable("ps_order_slip_detail");

                entity.Property(e => e.IdOrderSlip)
                    .HasColumnName("id_order_slip")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdOrderDetail)
                    .HasColumnName("id_order_detail")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.AmountTaxExcl)
                    .HasColumnName("amount_tax_excl")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.AmountTaxIncl)
                    .HasColumnName("amount_tax_incl")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.ProductQuantity)
                    .HasColumnName("product_quantity")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.TotalPriceTaxExcl)
                    .HasColumnName("total_price_tax_excl")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.TotalPriceTaxIncl)
                    .HasColumnName("total_price_tax_incl")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.UnitPriceTaxExcl)
                    .HasColumnName("unit_price_tax_excl")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.UnitPriceTaxIncl)
                    .HasColumnName("unit_price_tax_incl")
                    .HasColumnType("decimal(20,6)");
            });

            modelBuilder.Entity<PsOrderState>(entity =>
            {
                entity.HasKey(e => e.IdOrderState)
                    .HasName("PK_ps_order_state");

                entity.ToTable("ps_order_state");

                entity.HasIndex(e => e.ModuleName)
                    .HasName("module_name");

                entity.Property(e => e.IdOrderState)
                    .HasColumnName("id_order_state")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.Deleted)
                    .HasColumnName("deleted")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Delivery)
                    .HasColumnName("delivery")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Hidden)
                    .HasColumnName("hidden")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Invoice)
                    .HasColumnName("invoice")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Logable)
                    .HasColumnName("logable")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ModuleName)
                    .HasColumnName("module_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Paid)
                    .HasColumnName("paid")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.PdfDelivery)
                    .HasColumnName("pdf_delivery")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.PdfInvoice)
                    .HasColumnName("pdf_invoice")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.SendEmail)
                    .HasColumnName("send_email")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Shipped)
                    .HasColumnName("shipped")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Unremovable)
                    .HasColumnName("unremovable")
                    .HasColumnType("tinyint(1) unsigned");
            });

            modelBuilder.Entity<PsOrderStateLang>(entity =>
            {
                entity.HasKey(e => new { e.IdOrderState, e.IdLang })
                    .HasName("PK_ps_order_state_lang");

                entity.ToTable("ps_order_state_lang");

                entity.Property(e => e.IdOrderState)
                    .HasColumnName("id_order_state")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Template)
                    .IsRequired()
                    .HasColumnName("template")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsOrders>(entity =>
            {
                entity.HasKey(e => e.IdOrder)
                    .HasName("PK_ps_orders");

                entity.ToTable("ps_orders");

                entity.HasIndex(e => e.CurrentState)
                    .HasName("current_state");

                entity.HasIndex(e => e.DateAdd)
                    .HasName("date_add");

                entity.HasIndex(e => e.IdAddressDelivery)
                    .HasName("id_address_delivery");

                entity.HasIndex(e => e.IdAddressInvoice)
                    .HasName("id_address_invoice");

                entity.HasIndex(e => e.IdCarrier)
                    .HasName("id_carrier");

                entity.HasIndex(e => e.IdCart)
                    .HasName("id_cart");

                entity.HasIndex(e => e.IdCurrency)
                    .HasName("id_currency");

                entity.HasIndex(e => e.IdCustomer)
                    .HasName("id_customer");

                entity.HasIndex(e => e.IdLang)
                    .HasName("id_lang");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.HasIndex(e => e.IdShopGroup)
                    .HasName("id_shop_group");

                entity.HasIndex(e => e.InvoiceNumber)
                    .HasName("invoice_number");

                entity.HasIndex(e => e.Reference)
                    .HasName("reference");

                entity.Property(e => e.IdOrder)
                    .HasColumnName("id_order")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.CarrierTaxRate)
                    .HasColumnName("carrier_tax_rate")
                    .HasColumnType("decimal(10,3)")
                    .HasDefaultValueSql("0.000");

                entity.Property(e => e.ConversionRate)
                    .HasColumnName("conversion_rate")
                    .HasColumnType("decimal(13,6)")
                    .HasDefaultValueSql("1.000000");

                entity.Property(e => e.CurrentState)
                    .HasColumnName("current_state")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeliveryDate)
                    .HasColumnName("delivery_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeliveryNumber)
                    .HasColumnName("delivery_number")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Gift)
                    .HasColumnName("gift")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.GiftMessage)
                    .HasColumnName("gift_message")
                    .HasColumnType("text");

                entity.Property(e => e.IdAddressDelivery)
                    .HasColumnName("id_address_delivery")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdAddressInvoice)
                    .HasColumnName("id_address_invoice")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCarrier)
                    .HasColumnName("id_carrier")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCart)
                    .HasColumnName("id_cart")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCurrency)
                    .HasColumnName("id_currency")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCustomer)
                    .HasColumnName("id_customer")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdShopGroup)
                    .HasColumnName("id_shop_group")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.InvoiceDate)
                    .HasColumnName("invoice_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.InvoiceNumber)
                    .HasColumnName("invoice_number")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.MobileTheme)
                    .HasColumnName("mobile_theme")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Module)
                    .HasColumnName("module")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Payment)
                    .IsRequired()
                    .HasColumnName("payment")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Recyclable)
                    .HasColumnName("recyclable")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Reference)
                    .HasColumnName("reference")
                    .HasColumnType("varchar(9)");

                entity.Property(e => e.RoundMode)
                    .HasColumnName("round_mode")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("2");

                entity.Property(e => e.RoundType)
                    .HasColumnName("round_type")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.SecureKey)
                    .IsRequired()
                    .HasColumnName("secure_key")
                    .HasColumnType("varchar(32)")
                    .HasDefaultValueSql("-1");

                entity.Property(e => e.ShippingNumber)
                    .HasColumnName("shipping_number")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.TotalDiscounts)
                    .HasColumnName("total_discounts")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalDiscountsTaxExcl)
                    .HasColumnName("total_discounts_tax_excl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalDiscountsTaxIncl)
                    .HasColumnName("total_discounts_tax_incl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalPaid)
                    .HasColumnName("total_paid")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalPaidReal)
                    .HasColumnName("total_paid_real")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalPaidTaxExcl)
                    .HasColumnName("total_paid_tax_excl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalPaidTaxIncl)
                    .HasColumnName("total_paid_tax_incl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalProducts)
                    .HasColumnName("total_products")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalProductsWt)
                    .HasColumnName("total_products_wt")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalShipping)
                    .HasColumnName("total_shipping")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalShippingTaxExcl)
                    .HasColumnName("total_shipping_tax_excl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalShippingTaxIncl)
                    .HasColumnName("total_shipping_tax_incl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalWrapping)
                    .HasColumnName("total_wrapping")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalWrappingTaxExcl)
                    .HasColumnName("total_wrapping_tax_excl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalWrappingTaxIncl)
                    .HasColumnName("total_wrapping_tax_incl")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.Valid)
                    .HasColumnName("valid")
                    .HasColumnType("int(1) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsPack>(entity =>
            {
                entity.HasKey(e => new { e.IdProductPack, e.IdProductItem, e.IdProductAttributeItem })
                    .HasName("PK_ps_pack");

                entity.ToTable("ps_pack");

                entity.HasIndex(e => new { e.IdProductItem, e.IdProductAttributeItem })
                    .HasName("product_item");

                entity.Property(e => e.IdProductPack)
                    .HasColumnName("id_product_pack")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProductItem)
                    .HasColumnName("id_product_item")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProductAttributeItem)
                    .HasColumnName("id_product_attribute_item")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");
            });

            modelBuilder.Entity<PsPage>(entity =>
            {
                entity.HasKey(e => e.IdPage)
                    .HasName("PK_ps_page");

                entity.ToTable("ps_page");

                entity.HasIndex(e => e.IdObject)
                    .HasName("id_object");

                entity.HasIndex(e => e.IdPageType)
                    .HasName("id_page_type");

                entity.Property(e => e.IdPage)
                    .HasColumnName("id_page")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdObject)
                    .HasColumnName("id_object")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdPageType)
                    .HasColumnName("id_page_type")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsPageType>(entity =>
            {
                entity.HasKey(e => e.IdPageType)
                    .HasName("PK_ps_page_type");

                entity.ToTable("ps_page_type");

                entity.HasIndex(e => e.Name)
                    .HasName("name");

                entity.Property(e => e.IdPageType)
                    .HasColumnName("id_page_type")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<PsPageViewed>(entity =>
            {
                entity.HasKey(e => new { e.IdPage, e.IdShop, e.IdDateRange })
                    .HasName("PK_ps_page_viewed");

                entity.ToTable("ps_page_viewed");

                entity.Property(e => e.IdPage)
                    .HasColumnName("id_page")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdDateRange)
                    .HasColumnName("id_date_range")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Counter)
                    .HasColumnName("counter")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShopGroup)
                    .HasColumnName("id_shop_group")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");
            });

            modelBuilder.Entity<PsPagenotfound>(entity =>
            {
                entity.HasKey(e => e.IdPagenotfound)
                    .HasName("PK_ps_pagenotfound");

                entity.ToTable("ps_pagenotfound");

                entity.HasIndex(e => e.DateAdd)
                    .HasName("date_add");

                entity.Property(e => e.IdPagenotfound)
                    .HasColumnName("id_pagenotfound")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.HttpReferer)
                    .IsRequired()
                    .HasColumnName("http_referer")
                    .HasColumnType("varchar(256)");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdShopGroup)
                    .HasColumnName("id_shop_group")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.RequestUri)
                    .IsRequired()
                    .HasColumnName("request_uri")
                    .HasColumnType("varchar(256)");
            });

            modelBuilder.Entity<PsProduct>(entity =>
            {
                entity.HasKey(e => e.IdProduct)
                    .HasName("PK_ps_product");

                entity.ToTable("ps_product");

                entity.HasIndex(e => e.DateAdd)
                    .HasName("date_add");

                entity.HasIndex(e => e.IdCategoryDefault)
                    .HasName("id_category_default");

                entity.HasIndex(e => e.IdSupplier)
                    .HasName("product_supplier");

                entity.HasIndex(e => e.Indexed)
                    .HasName("indexed");

                entity.HasIndex(e => new { e.IdManufacturer, e.IdProduct })
                    .HasName("product_manufacturer");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.AdditionalShippingCost)
                    .HasColumnName("additional_shipping_cost")
                    .HasColumnType("decimal(20,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.AdvancedStockManagement)
                    .HasColumnName("advanced_stock_management")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.AvailableDate)
                    .HasColumnName("available_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("0000-00-00");

                entity.Property(e => e.AvailableForOrder)
                    .HasColumnName("available_for_order")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.CacheDefaultAttribute)
                    .HasColumnName("cache_default_attribute")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.CacheHasAttachments)
                    .HasColumnName("cache_has_attachments")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.CacheIsPack)
                    .HasColumnName("cache_is_pack")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Customizable)
                    .HasColumnName("customizable")
                    .HasColumnType("tinyint(2)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.Depth)
                    .HasColumnName("depth")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.Ean13)
                    .HasColumnName("ean13")
                    .HasColumnType("varchar(13)");

                entity.Property(e => e.Ecotax)
                    .HasColumnName("ecotax")
                    .HasColumnType("decimal(17,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.Height)
                    .HasColumnName("height")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.IdCategoryDefault)
                    .HasColumnName("id_category_default")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdManufacturer)
                    .HasColumnName("id_manufacturer")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProductRedirected)
                    .HasColumnName("id_product_redirected")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdShopDefault)
                    .HasColumnName("id_shop_default")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdSupplier)
                    .HasColumnName("id_supplier")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdTaxRulesGroup)
                    .HasColumnName("id_tax_rules_group")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Indexed)
                    .HasColumnName("indexed")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IsVirtual)
                    .HasColumnName("is_virtual")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Location)
                    .HasColumnName("location")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.MinimalQuantity)
                    .HasColumnName("minimal_quantity")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.OnSale)
                    .HasColumnName("on_sale")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.OnlineOnly)
                    .HasColumnName("online_only")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.OutOfStock)
                    .HasColumnName("out_of_stock")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("2");

                entity.Property(e => e.PackStockType)
                    .HasColumnName("pack_stock_type")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("3");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.QuantityDiscount)
                    .HasColumnName("quantity_discount")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Reference)
                    .HasColumnName("reference")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.ShowPrice)
                    .HasColumnName("show_price")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.SupplierReference)
                    .HasColumnName("supplier_reference")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.TextFields)
                    .HasColumnName("text_fields")
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.UnitPriceRatio)
                    .HasColumnName("unit_price_ratio")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.Unity)
                    .HasColumnName("unity")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Upc)
                    .HasColumnName("upc")
                    .HasColumnType("varchar(12)");

                entity.Property(e => e.UploadableFiles)
                    .HasColumnName("uploadable_files")
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Weight)
                    .HasColumnName("weight")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.WholesalePrice)
                    .HasColumnName("wholesale_price")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.Width)
                    .HasColumnName("width")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");
            });

            modelBuilder.Entity<PsProductAttachment>(entity =>
            {
                entity.HasKey(e => new { e.IdProduct, e.IdAttachment })
                    .HasName("PK_ps_product_attachment");

                entity.ToTable("ps_product_attachment");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdAttachment)
                    .HasColumnName("id_attachment")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsProductAttribute>(entity =>
            {
                entity.HasKey(e => e.IdProductAttribute)
                    .HasName("PK_ps_product_attribute");

                entity.ToTable("ps_product_attribute");

                entity.HasIndex(e => e.IdProduct)
                    .HasName("product_attribute_product");

                entity.HasIndex(e => e.Reference)
                    .HasName("reference");

                entity.HasIndex(e => e.SupplierReference)
                    .HasName("supplier_reference");

                entity.HasIndex(e => new { e.IdProduct, e.DefaultOn })
                    .HasName("product_default")
                    .IsUnique();

                entity.HasIndex(e => new { e.IdProductAttribute, e.IdProduct })
                    .HasName("id_product_id_product_attribute");

                entity.Property(e => e.IdProductAttribute)
                    .HasColumnName("id_product_attribute")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.AvailableDate)
                    .HasColumnName("available_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("0000-00-00");

                entity.Property(e => e.DefaultOn)
                    .IsRequired()
                    .HasColumnName("default_on")
                    .HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.Ean13)
                    .HasColumnName("ean13")
                    .HasColumnType("varchar(13)");

                entity.Property(e => e.Ecotax)
                    .HasColumnName("ecotax")
                    .HasColumnType("decimal(17,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Location)
                    .HasColumnName("location")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.MinimalQuantity)
                    .HasColumnName("minimal_quantity")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Reference)
                    .HasColumnName("reference")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.SupplierReference)
                    .HasColumnName("supplier_reference")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.UnitPriceImpact)
                    .HasColumnName("unit_price_impact")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.Upc)
                    .HasColumnName("upc")
                    .HasColumnType("varchar(12)");

                entity.Property(e => e.Weight)
                    .HasColumnName("weight")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.WholesalePrice)
                    .HasColumnName("wholesale_price")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");
            });

            modelBuilder.Entity<PsProductAttributeCombination>(entity =>
            {
                entity.HasKey(e => new { e.IdAttribute, e.IdProductAttribute })
                    .HasName("PK_ps_product_attribute_combination");

                entity.ToTable("ps_product_attribute_combination");

                entity.HasIndex(e => e.IdProductAttribute)
                    .HasName("id_product_attribute");

                entity.Property(e => e.IdAttribute)
                    .HasColumnName("id_attribute")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProductAttribute)
                    .HasColumnName("id_product_attribute")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsProductAttributeImage>(entity =>
            {
                entity.HasKey(e => new { e.IdProductAttribute, e.IdImage })
                    .HasName("PK_ps_product_attribute_image");

                entity.ToTable("ps_product_attribute_image");

                entity.HasIndex(e => e.IdImage)
                    .HasName("id_image");

                entity.Property(e => e.IdProductAttribute)
                    .HasColumnName("id_product_attribute")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdImage)
                    .HasColumnName("id_image")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsProductAttributeShop>(entity =>
            {
                entity.HasKey(e => new { e.IdProductAttribute, e.IdShop })
                    .HasName("PK_ps_product_attribute_shop");

                entity.ToTable("ps_product_attribute_shop");

                entity.HasIndex(e => new { e.IdProduct, e.IdShop, e.DefaultOn })
                    .HasName("id_product")
                    .IsUnique();

                entity.Property(e => e.IdProductAttribute)
                    .HasColumnName("id_product_attribute")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.AvailableDate)
                    .HasColumnName("available_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("0000-00-00");

                entity.Property(e => e.DefaultOn)
                    .IsRequired()
                    .HasColumnName("default_on")
                    .HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.Ecotax)
                    .HasColumnName("ecotax")
                    .HasColumnType("decimal(17,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.MinimalQuantity)
                    .HasColumnName("minimal_quantity")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.UnitPriceImpact)
                    .HasColumnName("unit_price_impact")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.Weight)
                    .HasColumnName("weight")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.WholesalePrice)
                    .HasColumnName("wholesale_price")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");
            });

            modelBuilder.Entity<PsProductCarrier>(entity =>
            {
                entity.HasKey(e => new { e.IdProduct, e.IdCarrierReference, e.IdShop })
                    .HasName("PK_ps_product_carrier");

                entity.ToTable("ps_product_carrier");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCarrierReference)
                    .HasColumnName("id_carrier_reference")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsProductCountryTax>(entity =>
            {
                entity.HasKey(e => new { e.IdProduct, e.IdCountry })
                    .HasName("PK_ps_product_country_tax");

                entity.ToTable("ps_product_country_tax");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdCountry)
                    .HasColumnName("id_country")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdTax)
                    .HasColumnName("id_tax")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<PsProductDownload>(entity =>
            {
                entity.HasKey(e => e.IdProductDownload)
                    .HasName("PK_ps_product_download");

                entity.ToTable("ps_product_download");

                entity.HasIndex(e => e.IdProduct)
                    .HasName("id_product")
                    .IsUnique();

                entity.HasIndex(e => new { e.IdProduct, e.Active })
                    .HasName("product_active");

                entity.Property(e => e.IdProductDownload)
                    .HasColumnName("id_product_download")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateExpiration)
                    .HasColumnName("date_expiration")
                    .HasColumnType("datetime");

                entity.Property(e => e.DisplayFilename)
                    .HasColumnName("display_filename")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Filename)
                    .HasColumnName("filename")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IsShareable)
                    .HasColumnName("is_shareable")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.NbDaysAccessible)
                    .HasColumnName("nb_days_accessible")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.NbDownloadable)
                    .HasColumnName("nb_downloadable")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");
            });

            modelBuilder.Entity<PsProductGroupReductionCache>(entity =>
            {
                entity.HasKey(e => new { e.IdProduct, e.IdGroup })
                    .HasName("PK_ps_product_group_reduction_cache");

                entity.ToTable("ps_product_group_reduction_cache");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdGroup)
                    .HasColumnName("id_group")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Reduction)
                    .HasColumnName("reduction")
                    .HasColumnType("decimal(4,3)");
            });

            modelBuilder.Entity<PsProductLang>(entity =>
            {
                entity.HasKey(e => new { e.IdProduct, e.IdShop, e.IdLang })
                    .HasName("PK_ps_product_lang");

                entity.ToTable("ps_product_lang");

                entity.HasIndex(e => e.IdLang)
                    .HasName("id_lang");

                entity.HasIndex(e => e.Name)
                    .HasName("name");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.AvailableLater)
                    .HasColumnName("available_later")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.AvailableNow)
                    .HasColumnName("available_now")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.DescriptionShort)
                    .HasColumnName("description_short")
                    .HasColumnType("text");

                entity.Property(e => e.LinkRewrite)
                    .IsRequired()
                    .HasColumnName("link_rewrite")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.MetaDescription)
                    .HasColumnName("meta_description")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.MetaKeywords)
                    .HasColumnName("meta_keywords")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.MetaTitle)
                    .HasColumnName("meta_title")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsProductSale>(entity =>
            {
                entity.HasKey(e => e.IdProduct)
                    .HasName("PK_ps_product_sale");

                entity.ToTable("ps_product_sale");

                entity.HasIndex(e => e.Quantity)
                    .HasName("quantity");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("date");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.SaleNbr)
                    .HasColumnName("sale_nbr")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsProductShop>(entity =>
            {
                entity.HasKey(e => new { e.IdProduct, e.IdShop })
                    .HasName("PK_ps_product_shop");

                entity.ToTable("ps_product_shop");

                entity.HasIndex(e => e.IdCategoryDefault)
                    .HasName("id_category_default");

                entity.HasIndex(e => new { e.Indexed, e.Active, e.IdProduct })
                    .HasName("indexed");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.AdditionalShippingCost)
                    .HasColumnName("additional_shipping_cost")
                    .HasColumnType("decimal(20,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.AdvancedStockManagement)
                    .HasColumnName("advanced_stock_management")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.AvailableDate)
                    .HasColumnName("available_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("0000-00-00");

                entity.Property(e => e.AvailableForOrder)
                    .HasColumnName("available_for_order")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.CacheDefaultAttribute)
                    .HasColumnName("cache_default_attribute")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Customizable)
                    .HasColumnName("customizable")
                    .HasColumnType("tinyint(2)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.Ecotax)
                    .HasColumnName("ecotax")
                    .HasColumnType("decimal(17,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.IdCategoryDefault)
                    .HasColumnName("id_category_default")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProductRedirected)
                    .HasColumnName("id_product_redirected")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdTaxRulesGroup)
                    .HasColumnName("id_tax_rules_group")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Indexed)
                    .HasColumnName("indexed")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.MinimalQuantity)
                    .HasColumnName("minimal_quantity")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.OnSale)
                    .HasColumnName("on_sale")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.OnlineOnly)
                    .HasColumnName("online_only")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.PackStockType)
                    .HasColumnName("pack_stock_type")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("3");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.ShowPrice)
                    .HasColumnName("show_price")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.TextFields)
                    .HasColumnName("text_fields")
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.UnitPriceRatio)
                    .HasColumnName("unit_price_ratio")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.Unity)
                    .HasColumnName("unity")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.UploadableFiles)
                    .HasColumnName("uploadable_files")
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.WholesalePrice)
                    .HasColumnName("wholesale_price")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");
            });

            modelBuilder.Entity<PsProductSupplier>(entity =>
            {
                entity.HasKey(e => e.IdProductSupplier)
                    .HasName("PK_ps_product_supplier");

                entity.ToTable("ps_product_supplier");

                entity.HasIndex(e => new { e.IdSupplier, e.IdProduct })
                    .HasName("id_supplier");

                entity.HasIndex(e => new { e.IdProduct, e.IdProductAttribute, e.IdSupplier })
                    .HasName("id_product")
                    .IsUnique();

                entity.Property(e => e.IdProductSupplier)
                    .HasColumnName("id_product_supplier")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdCurrency)
                    .HasColumnName("id_currency")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdProductAttribute)
                    .HasColumnName("id_product_attribute")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdSupplier)
                    .HasColumnName("id_supplier")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.ProductSupplierPriceTe)
                    .HasColumnName("product_supplier_price_te")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.ProductSupplierReference)
                    .HasColumnName("product_supplier_reference")
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<PsProductTag>(entity =>
            {
                entity.HasKey(e => new { e.IdProduct, e.IdTag })
                    .HasName("PK_ps_product_tag");

                entity.ToTable("ps_product_tag");

                entity.HasIndex(e => e.IdTag)
                    .HasName("id_tag");

                entity.HasIndex(e => new { e.IdLang, e.IdTag })
                    .HasName("id_lang");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdTag)
                    .HasColumnName("id_tag")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsProfile>(entity =>
            {
                entity.HasKey(e => e.IdProfile)
                    .HasName("PK_ps_profile");

                entity.ToTable("ps_profile");

                entity.Property(e => e.IdProfile)
                    .HasColumnName("id_profile")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsProfileLang>(entity =>
            {
                entity.HasKey(e => new { e.IdLang, e.IdProfile })
                    .HasName("PK_ps_profile_lang");

                entity.ToTable("ps_profile_lang");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProfile)
                    .HasColumnName("id_profile")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsQuickAccess>(entity =>
            {
                entity.HasKey(e => e.IdQuickAccess)
                    .HasName("PK_ps_quick_access");

                entity.ToTable("ps_quick_access");

                entity.Property(e => e.IdQuickAccess)
                    .HasColumnName("id_quick_access")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Link)
                    .IsRequired()
                    .HasColumnName("link")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.NewWindow)
                    .HasColumnName("new_window")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsQuickAccessLang>(entity =>
            {
                entity.HasKey(e => new { e.IdQuickAccess, e.IdLang })
                    .HasName("PK_ps_quick_access_lang");

                entity.ToTable("ps_quick_access_lang");

                entity.Property(e => e.IdQuickAccess)
                    .HasColumnName("id_quick_access")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<PsRangePrice>(entity =>
            {
                entity.HasKey(e => e.IdRangePrice)
                    .HasName("PK_ps_range_price");

                entity.ToTable("ps_range_price");

                entity.HasIndex(e => new { e.IdCarrier, e.Delimiter1, e.Delimiter2 })
                    .HasName("id_carrier")
                    .IsUnique();

                entity.Property(e => e.IdRangePrice)
                    .HasColumnName("id_range_price")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Delimiter1)
                    .HasColumnName("delimiter1")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.Delimiter2)
                    .HasColumnName("delimiter2")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.IdCarrier)
                    .HasColumnName("id_carrier")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsRangeWeight>(entity =>
            {
                entity.HasKey(e => e.IdRangeWeight)
                    .HasName("PK_ps_range_weight");

                entity.ToTable("ps_range_weight");

                entity.HasIndex(e => new { e.IdCarrier, e.Delimiter1, e.Delimiter2 })
                    .HasName("id_carrier")
                    .IsUnique();

                entity.Property(e => e.IdRangeWeight)
                    .HasColumnName("id_range_weight")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Delimiter1)
                    .HasColumnName("delimiter1")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.Delimiter2)
                    .HasColumnName("delimiter2")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.IdCarrier)
                    .HasColumnName("id_carrier")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsReferrer>(entity =>
            {
                entity.HasKey(e => e.IdReferrer)
                    .HasName("PK_ps_referrer");

                entity.ToTable("ps_referrer");

                entity.Property(e => e.IdReferrer)
                    .HasColumnName("id_referrer")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.BaseFee)
                    .HasColumnName("base_fee")
                    .HasColumnType("decimal(5,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.ClickFee)
                    .HasColumnName("click_fee")
                    .HasColumnType("decimal(5,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.HttpRefererLike)
                    .HasColumnName("http_referer_like")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.HttpRefererLikeNot)
                    .HasColumnName("http_referer_like_not")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.HttpRefererRegexp)
                    .HasColumnName("http_referer_regexp")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.HttpRefererRegexpNot)
                    .HasColumnName("http_referer_regexp_not")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Passwd)
                    .HasColumnName("passwd")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.PercentFee)
                    .HasColumnName("percent_fee")
                    .HasColumnType("decimal(5,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.RequestUriLike)
                    .HasColumnName("request_uri_like")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.RequestUriLikeNot)
                    .HasColumnName("request_uri_like_not")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.RequestUriRegexp)
                    .HasColumnName("request_uri_regexp")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.RequestUriRegexpNot)
                    .HasColumnName("request_uri_regexp_not")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsReferrerCache>(entity =>
            {
                entity.HasKey(e => new { e.IdConnectionsSource, e.IdReferrer })
                    .HasName("PK_ps_referrer_cache");

                entity.ToTable("ps_referrer_cache");

                entity.Property(e => e.IdConnectionsSource)
                    .HasColumnName("id_connections_source")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdReferrer)
                    .HasColumnName("id_referrer")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsReferrerShop>(entity =>
            {
                entity.HasKey(e => new { e.IdReferrer, e.IdShop })
                    .HasName("PK_ps_referrer_shop");

                entity.ToTable("ps_referrer_shop");

                entity.Property(e => e.IdReferrer)
                    .HasColumnName("id_referrer")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.CacheOrderRate)
                    .HasColumnName("cache_order_rate")
                    .HasColumnType("decimal(5,4)");

                entity.Property(e => e.CacheOrders)
                    .HasColumnName("cache_orders")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CachePages)
                    .HasColumnName("cache_pages")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CacheRegRate)
                    .HasColumnName("cache_reg_rate")
                    .HasColumnType("decimal(5,4)");

                entity.Property(e => e.CacheRegistrations)
                    .HasColumnName("cache_registrations")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CacheSales)
                    .HasColumnName("cache_sales")
                    .HasColumnType("decimal(17,2)");

                entity.Property(e => e.CacheVisitors)
                    .HasColumnName("cache_visitors")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CacheVisits)
                    .HasColumnName("cache_visits")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<PsRequestSql>(entity =>
            {
                entity.HasKey(e => e.IdRequestSql)
                    .HasName("PK_ps_request_sql");

                entity.ToTable("ps_request_sql");

                entity.Property(e => e.IdRequestSql)
                    .HasColumnName("id_request_sql")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.Sql)
                    .IsRequired()
                    .HasColumnName("sql")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<PsRequiredField>(entity =>
            {
                entity.HasKey(e => e.IdRequiredField)
                    .HasName("PK_ps_required_field");

                entity.ToTable("ps_required_field");

                entity.HasIndex(e => e.ObjectName)
                    .HasName("object_name");

                entity.Property(e => e.IdRequiredField)
                    .HasColumnName("id_required_field")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FieldName)
                    .IsRequired()
                    .HasColumnName("field_name")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.ObjectName)
                    .IsRequired()
                    .HasColumnName("object_name")
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<PsRisk>(entity =>
            {
                entity.HasKey(e => e.IdRisk)
                    .HasName("PK_ps_risk");

                entity.ToTable("ps_risk");

                entity.Property(e => e.IdRisk)
                    .HasColumnName("id_risk")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.Percent)
                    .HasColumnName("percent")
                    .HasColumnType("tinyint(3)");
            });

            modelBuilder.Entity<PsRiskLang>(entity =>
            {
                entity.HasKey(e => new { e.IdRisk, e.IdLang })
                    .HasName("PK_ps_risk_lang");

                entity.ToTable("ps_risk_lang");

                entity.HasIndex(e => e.IdRisk)
                    .HasName("id_risk");

                entity.Property(e => e.IdRisk)
                    .HasColumnName("id_risk")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<PsScene>(entity =>
            {
                entity.HasKey(e => e.IdScene)
                    .HasName("PK_ps_scene");

                entity.ToTable("ps_scene");

                entity.Property(e => e.IdScene)
                    .HasColumnName("id_scene")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");
            });

            modelBuilder.Entity<PsSceneCategory>(entity =>
            {
                entity.HasKey(e => new { e.IdScene, e.IdCategory })
                    .HasName("PK_ps_scene_category");

                entity.ToTable("ps_scene_category");

                entity.Property(e => e.IdScene)
                    .HasColumnName("id_scene")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCategory)
                    .HasColumnName("id_category")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsSceneLang>(entity =>
            {
                entity.HasKey(e => new { e.IdScene, e.IdLang })
                    .HasName("PK_ps_scene_lang");

                entity.ToTable("ps_scene_lang");

                entity.Property(e => e.IdScene)
                    .HasColumnName("id_scene")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<PsSceneProducts>(entity =>
            {
                entity.HasKey(e => new { e.IdScene, e.IdProduct, e.XAxis, e.YAxis })
                    .HasName("PK_ps_scene_products");

                entity.ToTable("ps_scene_products");

                entity.Property(e => e.IdScene)
                    .HasColumnName("id_scene")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.XAxis)
                    .HasColumnName("x_axis")
                    .HasColumnType("int(4)");

                entity.Property(e => e.YAxis)
                    .HasColumnName("y_axis")
                    .HasColumnType("int(4)");

                entity.Property(e => e.ZoneHeight)
                    .HasColumnName("zone_height")
                    .HasColumnType("int(3)");

                entity.Property(e => e.ZoneWidth)
                    .HasColumnName("zone_width")
                    .HasColumnType("int(3)");
            });

            modelBuilder.Entity<PsSceneShop>(entity =>
            {
                entity.HasKey(e => new { e.IdScene, e.IdShop })
                    .HasName("PK_ps_scene_shop");

                entity.ToTable("ps_scene_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdScene)
                    .HasColumnName("id_scene")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsSearchEngine>(entity =>
            {
                entity.HasKey(e => e.IdSearchEngine)
                    .HasName("PK_ps_search_engine");

                entity.ToTable("ps_search_engine");

                entity.Property(e => e.IdSearchEngine)
                    .HasColumnName("id_search_engine")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Getvar)
                    .IsRequired()
                    .HasColumnName("getvar")
                    .HasColumnType("varchar(16)");

                entity.Property(e => e.Server)
                    .IsRequired()
                    .HasColumnName("server")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsSearchIndex>(entity =>
            {
                entity.HasKey(e => new { e.IdProduct, e.IdWord })
                    .HasName("PK_ps_search_index");

                entity.ToTable("ps_search_index");

                entity.HasIndex(e => e.IdProduct)
                    .HasName("id_product");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdWord)
                    .HasColumnName("id_word")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Weight)
                    .HasColumnName("weight")
                    .HasColumnType("smallint(4) unsigned")
                    .HasDefaultValueSql("1");
            });

            modelBuilder.Entity<PsSearchWord>(entity =>
            {
                entity.HasKey(e => e.IdWord)
                    .HasName("PK_ps_search_word");

                entity.ToTable("ps_search_word");

                entity.HasIndex(e => new { e.IdLang, e.IdShop, e.Word })
                    .HasName("id_lang")
                    .IsUnique();

                entity.Property(e => e.IdWord)
                    .HasColumnName("id_word")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Word)
                    .IsRequired()
                    .HasColumnName("word")
                    .HasColumnType("varchar(15)");
            });

            modelBuilder.Entity<PsSekeyword>(entity =>
            {
                entity.HasKey(e => e.IdSekeyword)
                    .HasName("PK_ps_sekeyword");

                entity.ToTable("ps_sekeyword");

                entity.Property(e => e.IdSekeyword)
                    .HasColumnName("id_sekeyword")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdShopGroup)
                    .HasColumnName("id_shop_group")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Keyword)
                    .IsRequired()
                    .HasColumnName("keyword")
                    .HasColumnType("varchar(256)");
            });

            modelBuilder.Entity<PsShop>(entity =>
            {
                entity.HasKey(e => e.IdShop)
                    .HasName("PK_ps_shop");

                entity.ToTable("ps_shop");

                entity.HasIndex(e => e.IdCategory)
                    .HasName("id_category");

                entity.HasIndex(e => e.IdTheme)
                    .HasName("id_theme");

                entity.HasIndex(e => new { e.IdShopGroup, e.Deleted })
                    .HasName("id_shop_group");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Deleted)
                    .HasColumnName("deleted")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdCategory)
                    .HasColumnName("id_category")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdShopGroup)
                    .HasColumnName("id_shop_group")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdTheme)
                    .HasColumnName("id_theme")
                    .HasColumnType("int(1) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsShopGroup>(entity =>
            {
                entity.HasKey(e => e.IdShopGroup)
                    .HasName("PK_ps_shop_group");

                entity.ToTable("ps_shop_group");

                entity.HasIndex(e => new { e.Deleted, e.Name })
                    .HasName("deleted");

                entity.Property(e => e.IdShopGroup)
                    .HasColumnName("id_shop_group")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Deleted)
                    .HasColumnName("deleted")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.ShareCustomer)
                    .HasColumnName("share_customer")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.ShareOrder)
                    .HasColumnName("share_order")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.ShareStock)
                    .HasColumnName("share_stock")
                    .HasColumnType("tinyint(1)");
            });

            modelBuilder.Entity<PsShopUrl>(entity =>
            {
                entity.HasKey(e => e.IdShopUrl)
                    .HasName("PK_ps_shop_url");

                entity.ToTable("ps_shop_url");

                entity.HasIndex(e => new { e.IdShop, e.Main })
                    .HasName("id_shop");

                entity.HasIndex(e => new { e.Domain, e.PhysicalUri, e.VirtualUri })
                    .HasName("full_shop_url")
                    .IsUnique();

                entity.HasIndex(e => new { e.DomainSsl, e.PhysicalUri, e.VirtualUri })
                    .HasName("full_shop_url_ssl")
                    .IsUnique();

                entity.Property(e => e.IdShopUrl)
                    .HasColumnName("id_shop_url")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.Domain)
                    .IsRequired()
                    .HasColumnName("domain")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.DomainSsl)
                    .IsRequired()
                    .HasColumnName("domain_ssl")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Main)
                    .HasColumnName("main")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.PhysicalUri)
                    .IsRequired()
                    .HasColumnName("physical_uri")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.VirtualUri)
                    .IsRequired()
                    .HasColumnName("virtual_uri")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsSmartyCache>(entity =>
            {
                entity.HasKey(e => e.IdSmartyCache)
                    .HasName("PK_ps_smarty_cache");

                entity.ToTable("ps_smarty_cache");

                entity.HasIndex(e => e.CacheId)
                    .HasName("cache_id");

                entity.HasIndex(e => e.Modified)
                    .HasName("modified");

                entity.HasIndex(e => e.Name)
                    .HasName("name");

                entity.Property(e => e.IdSmartyCache)
                    .HasColumnName("id_smarty_cache")
                    .HasColumnType("char(40)");

                entity.Property(e => e.CacheId)
                    .HasColumnName("cache_id")
                    .HasColumnType("varchar(254)");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasColumnName("content");

                entity.Property(e => e.Modified)
                    .HasColumnName("modified")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("char(40)");
            });

            modelBuilder.Entity<PsSmartyLazyCache>(entity =>
            {
                entity.HasKey(e => new { e.TemplateHash, e.CacheId, e.CompileId })
                    .HasName("PK_ps_smarty_lazy_cache");

                entity.ToTable("ps_smarty_lazy_cache");

                entity.Property(e => e.TemplateHash)
                    .HasColumnName("template_hash")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.CacheId)
                    .HasColumnName("cache_id")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.CompileId)
                    .HasColumnName("compile_id")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.Filepath)
                    .IsRequired()
                    .HasColumnName("filepath")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.LastUpdate)
                    .HasColumnName("last_update")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("0000-00-00 00:00:00");
            });

            modelBuilder.Entity<PsSpecificPrice>(entity =>
            {
                entity.HasKey(e => e.IdSpecificPrice)
                    .HasName("PK_ps_specific_price");

                entity.ToTable("ps_specific_price");

                entity.HasIndex(e => e.From)
                    .HasName("from");

                entity.HasIndex(e => e.FromQuantity)
                    .HasName("from_quantity");

                entity.HasIndex(e => e.IdCart)
                    .HasName("id_cart");

                entity.HasIndex(e => e.IdCustomer)
                    .HasName("id_customer");

                entity.HasIndex(e => e.IdProductAttribute)
                    .HasName("id_product_attribute");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.HasIndex(e => e.IdSpecificPriceRule)
                    .HasName("id_specific_price_rule");

                entity.HasIndex(e => e.To)
                    .HasName("to");

                entity.HasIndex(e => new { e.IdProduct, e.IdShop, e.IdCurrency, e.IdCountry, e.IdGroup, e.IdCustomer, e.FromQuantity, e.From, e.To })
                    .HasName("id_product");

                entity.HasIndex(e => new { e.IdProduct, e.IdProductAttribute, e.IdCustomer, e.IdCart, e.From, e.To, e.IdShop, e.IdShopGroup, e.IdCurrency, e.IdCountry, e.IdGroup, e.FromQuantity, e.IdSpecificPriceRule })
                    .HasName("id_product_2")
                    .IsUnique();

                entity.Property(e => e.IdSpecificPrice)
                    .HasColumnName("id_specific_price")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.From)
                    .HasColumnName("from")
                    .HasColumnType("datetime");

                entity.Property(e => e.FromQuantity)
                    .HasColumnName("from_quantity")
                    .HasColumnType("mediumint(8) unsigned");

                entity.Property(e => e.IdCart)
                    .HasColumnName("id_cart")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdCountry)
                    .HasColumnName("id_country")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCurrency)
                    .HasColumnName("id_currency")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCustomer)
                    .HasColumnName("id_customer")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdGroup)
                    .HasColumnName("id_group")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdProductAttribute)
                    .HasColumnName("id_product_attribute")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdShopGroup)
                    .HasColumnName("id_shop_group")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdSpecificPriceRule)
                    .HasColumnName("id_specific_price_rule")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.Reduction)
                    .HasColumnName("reduction")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.ReductionTax)
                    .HasColumnName("reduction_tax")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.To)
                    .HasColumnName("to")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PsSpecificPricePriority>(entity =>
            {
                entity.HasKey(e => new { e.IdSpecificPricePriority, e.IdProduct })
                    .HasName("PK_ps_specific_price_priority");

                entity.ToTable("ps_specific_price_priority");

                entity.HasIndex(e => e.IdProduct)
                    .HasName("id_product")
                    .IsUnique();

                entity.Property(e => e.IdSpecificPricePriority)
                    .HasColumnName("id_specific_price_priority")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Priority)
                    .IsRequired()
                    .HasColumnName("priority")
                    .HasColumnType("varchar(80)");
            });

            modelBuilder.Entity<PsSpecificPriceRule>(entity =>
            {
                entity.HasKey(e => e.IdSpecificPriceRule)
                    .HasName("PK_ps_specific_price_rule");

                entity.ToTable("ps_specific_price_rule");

                entity.HasIndex(e => new { e.IdShop, e.IdCurrency, e.IdCountry, e.IdGroup, e.FromQuantity, e.From, e.To })
                    .HasName("id_product");

                entity.Property(e => e.IdSpecificPriceRule)
                    .HasColumnName("id_specific_price_rule")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.From)
                    .HasColumnName("from")
                    .HasColumnType("datetime");

                entity.Property(e => e.FromQuantity)
                    .HasColumnName("from_quantity")
                    .HasColumnType("mediumint(8) unsigned");

                entity.Property(e => e.IdCountry)
                    .HasColumnName("id_country")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdCurrency)
                    .HasColumnName("id_currency")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdGroup)
                    .HasColumnName("id_group")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.Reduction)
                    .HasColumnName("reduction")
                    .HasColumnType("decimal(20,6)");

                entity.Property(e => e.ReductionTax)
                    .HasColumnName("reduction_tax")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.To)
                    .HasColumnName("to")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PsSpecificPriceRuleCondition>(entity =>
            {
                entity.HasKey(e => e.IdSpecificPriceRuleCondition)
                    .HasName("PK_ps_specific_price_rule_condition");

                entity.ToTable("ps_specific_price_rule_condition");

                entity.HasIndex(e => e.IdSpecificPriceRuleConditionGroup)
                    .HasName("id_specific_price_rule_condition_group");

                entity.Property(e => e.IdSpecificPriceRuleCondition)
                    .HasColumnName("id_specific_price_rule_condition")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdSpecificPriceRuleConditionGroup)
                    .HasColumnName("id_specific_price_rule_condition_group")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnName("value")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<PsSpecificPriceRuleConditionGroup>(entity =>
            {
                entity.HasKey(e => new { e.IdSpecificPriceRuleConditionGroup, e.IdSpecificPriceRule })
                    .HasName("PK_ps_specific_price_rule_condition_group");

                entity.ToTable("ps_specific_price_rule_condition_group");

                entity.Property(e => e.IdSpecificPriceRuleConditionGroup)
                    .HasColumnName("id_specific_price_rule_condition_group")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdSpecificPriceRule)
                    .HasColumnName("id_specific_price_rule")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsState>(entity =>
            {
                entity.HasKey(e => e.IdState)
                    .HasName("PK_ps_state");

                entity.ToTable("ps_state");

                entity.HasIndex(e => e.IdCountry)
                    .HasName("id_country");

                entity.HasIndex(e => e.IdZone)
                    .HasName("id_zone");

                entity.HasIndex(e => e.Name)
                    .HasName("name");

                entity.Property(e => e.IdState)
                    .HasColumnName("id_state")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdCountry)
                    .HasColumnName("id_country")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdZone)
                    .HasColumnName("id_zone")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IsoCode)
                    .IsRequired()
                    .HasColumnName("iso_code")
                    .HasColumnType("varchar(7)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.TaxBehavior)
                    .HasColumnName("tax_behavior")
                    .HasColumnType("smallint(1)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsStatssearch>(entity =>
            {
                entity.HasKey(e => e.IdStatssearch)
                    .HasName("PK_ps_statssearch");

                entity.ToTable("ps_statssearch");

                entity.Property(e => e.IdStatssearch)
                    .HasColumnName("id_statssearch")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.IdShopGroup)
                    .HasColumnName("id_shop_group")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Keywords)
                    .IsRequired()
                    .HasColumnName("keywords")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Results)
                    .HasColumnName("results")
                    .HasColumnType("int(6)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsStock>(entity =>
            {
                entity.HasKey(e => e.IdStock)
                    .HasName("PK_ps_stock");

                entity.ToTable("ps_stock");

                entity.HasIndex(e => e.IdProduct)
                    .HasName("id_product");

                entity.HasIndex(e => e.IdProductAttribute)
                    .HasName("id_product_attribute");

                entity.HasIndex(e => e.IdWarehouse)
                    .HasName("id_warehouse");

                entity.Property(e => e.IdStock)
                    .HasColumnName("id_stock")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Ean13)
                    .HasColumnName("ean13")
                    .HasColumnType("varchar(13)");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdProductAttribute)
                    .HasColumnName("id_product_attribute")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdWarehouse)
                    .HasColumnName("id_warehouse")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.PhysicalQuantity)
                    .HasColumnName("physical_quantity")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.PriceTe)
                    .HasColumnName("price_te")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.Reference)
                    .IsRequired()
                    .HasColumnName("reference")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.Upc)
                    .HasColumnName("upc")
                    .HasColumnType("varchar(12)");

                entity.Property(e => e.UsableQuantity)
                    .HasColumnName("usable_quantity")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsStockAvailable>(entity =>
            {
                entity.HasKey(e => e.IdStockAvailable)
                    .HasName("PK_ps_stock_available");

                entity.ToTable("ps_stock_available");

                entity.HasIndex(e => e.IdProduct)
                    .HasName("id_product");

                entity.HasIndex(e => e.IdProductAttribute)
                    .HasName("id_product_attribute");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.HasIndex(e => e.IdShopGroup)
                    .HasName("id_shop_group");

                entity.HasIndex(e => new { e.IdProduct, e.IdProductAttribute, e.IdShop, e.IdShopGroup })
                    .HasName("product_sqlstock")
                    .IsUnique();

                entity.Property(e => e.IdStockAvailable)
                    .HasColumnName("id_stock_available")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.DependsOnStock)
                    .HasColumnName("depends_on_stock")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdProductAttribute)
                    .HasColumnName("id_product_attribute")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShopGroup)
                    .HasColumnName("id_shop_group")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.OutOfStock)
                    .HasColumnName("out_of_stock")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsStockMvt>(entity =>
            {
                entity.HasKey(e => e.IdStockMvt)
                    .HasName("PK_ps_stock_mvt");

                entity.ToTable("ps_stock_mvt");

                entity.HasIndex(e => e.IdStock)
                    .HasName("id_stock");

                entity.HasIndex(e => e.IdStockMvtReason)
                    .HasName("id_stock_mvt_reason");

                entity.Property(e => e.IdStockMvt)
                    .HasColumnName("id_stock_mvt")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.CurrentWa)
                    .HasColumnName("current_wa")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.EmployeeFirstname)
                    .HasColumnName("employee_firstname")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.EmployeeLastname)
                    .HasColumnName("employee_lastname")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.IdEmployee)
                    .HasColumnName("id_employee")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdOrder)
                    .HasColumnName("id_order")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdStock)
                    .HasColumnName("id_stock")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdStockMvtReason)
                    .HasColumnName("id_stock_mvt_reason")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdSupplyOrder)
                    .HasColumnName("id_supply_order")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.LastWa)
                    .HasColumnName("last_wa")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.PhysicalQuantity)
                    .HasColumnName("physical_quantity")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.PriceTe)
                    .HasColumnName("price_te")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.Referer)
                    .HasColumnName("referer")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Sign)
                    .HasColumnName("sign")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");
            });

            modelBuilder.Entity<PsStockMvtReason>(entity =>
            {
                entity.HasKey(e => e.IdStockMvtReason)
                    .HasName("PK_ps_stock_mvt_reason");

                entity.ToTable("ps_stock_mvt_reason");

                entity.Property(e => e.IdStockMvtReason)
                    .HasColumnName("id_stock_mvt_reason")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deleted)
                    .HasColumnName("deleted")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Sign)
                    .HasColumnName("sign")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");
            });

            modelBuilder.Entity<PsStockMvtReasonLang>(entity =>
            {
                entity.HasKey(e => new { e.IdStockMvtReason, e.IdLang })
                    .HasName("PK_ps_stock_mvt_reason_lang");

                entity.ToTable("ps_stock_mvt_reason_lang");

                entity.Property(e => e.IdStockMvtReason)
                    .HasColumnName("id_stock_mvt_reason")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<PsStore>(entity =>
            {
                entity.HasKey(e => e.IdStore)
                    .HasName("PK_ps_store");

                entity.ToTable("ps_store");

                entity.Property(e => e.IdStore)
                    .HasColumnName("id_store")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Address1)
                    .IsRequired()
                    .HasColumnName("address1")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.Address2)
                    .HasColumnName("address2")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasColumnName("city")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.Fax)
                    .HasColumnName("fax")
                    .HasColumnType("varchar(16)");

                entity.Property(e => e.Hours)
                    .HasColumnName("hours")
                    .HasColumnType("varchar(254)");

                entity.Property(e => e.IdCountry)
                    .HasColumnName("id_country")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdState)
                    .HasColumnName("id_state")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Latitude)
                    .HasColumnName("latitude")
                    .HasColumnType("decimal(13,8)");

                entity.Property(e => e.Longitude)
                    .HasColumnName("longitude")
                    .HasColumnType("decimal(13,8)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasColumnType("text");

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasColumnType("varchar(16)");

                entity.Property(e => e.Postcode)
                    .IsRequired()
                    .HasColumnName("postcode")
                    .HasColumnType("varchar(12)");
            });

            modelBuilder.Entity<PsStoreShop>(entity =>
            {
                entity.HasKey(e => new { e.IdStore, e.IdShop })
                    .HasName("PK_ps_store_shop");

                entity.ToTable("ps_store_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdStore)
                    .HasColumnName("id_store")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsSupplier>(entity =>
            {
                entity.HasKey(e => e.IdSupplier)
                    .HasName("PK_ps_supplier");

                entity.ToTable("ps_supplier");

                entity.Property(e => e.IdSupplier)
                    .HasColumnName("id_supplier")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsSupplierLang>(entity =>
            {
                entity.HasKey(e => new { e.IdSupplier, e.IdLang })
                    .HasName("PK_ps_supplier_lang");

                entity.ToTable("ps_supplier_lang");

                entity.Property(e => e.IdSupplier)
                    .HasColumnName("id_supplier")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.MetaDescription)
                    .HasColumnName("meta_description")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.MetaKeywords)
                    .HasColumnName("meta_keywords")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.MetaTitle)
                    .HasColumnName("meta_title")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsSupplierShop>(entity =>
            {
                entity.HasKey(e => new { e.IdSupplier, e.IdShop })
                    .HasName("PK_ps_supplier_shop");

                entity.ToTable("ps_supplier_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdSupplier)
                    .HasColumnName("id_supplier")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsSupplyOrder>(entity =>
            {
                entity.HasKey(e => e.IdSupplyOrder)
                    .HasName("PK_ps_supply_order");

                entity.ToTable("ps_supply_order");

                entity.HasIndex(e => e.IdSupplier)
                    .HasName("id_supplier");

                entity.HasIndex(e => e.IdWarehouse)
                    .HasName("id_warehouse");

                entity.HasIndex(e => e.Reference)
                    .HasName("reference");

                entity.Property(e => e.IdSupplyOrder)
                    .HasColumnName("id_supply_order")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateDeliveryExpected)
                    .HasColumnName("date_delivery_expected")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.DiscountRate)
                    .HasColumnName("discount_rate")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.DiscountValueTe)
                    .HasColumnName("discount_value_te")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.IdCurrency)
                    .HasColumnName("id_currency")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdRefCurrency)
                    .HasColumnName("id_ref_currency")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdSupplier)
                    .HasColumnName("id_supplier")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdSupplyOrderState)
                    .HasColumnName("id_supply_order_state")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdWarehouse)
                    .HasColumnName("id_warehouse")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IsTemplate)
                    .HasColumnName("is_template")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Reference)
                    .IsRequired()
                    .HasColumnName("reference")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.SupplierName)
                    .IsRequired()
                    .HasColumnName("supplier_name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.TotalTax)
                    .HasColumnName("total_tax")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalTe)
                    .HasColumnName("total_te")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalTi)
                    .HasColumnName("total_ti")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TotalWithDiscountTe)
                    .HasColumnName("total_with_discount_te")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");
            });

            modelBuilder.Entity<PsSupplyOrderDetail>(entity =>
            {
                entity.HasKey(e => e.IdSupplyOrderDetail)
                    .HasName("PK_ps_supply_order_detail");

                entity.ToTable("ps_supply_order_detail");

                entity.HasIndex(e => e.IdProductAttribute)
                    .HasName("id_product_attribute");

                entity.HasIndex(e => new { e.IdProduct, e.IdProductAttribute })
                    .HasName("id_product_product_attribute");

                entity.HasIndex(e => new { e.IdSupplyOrder, e.IdProduct })
                    .HasName("id_supply_order");

                entity.Property(e => e.IdSupplyOrderDetail)
                    .HasColumnName("id_supply_order_detail")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.DiscountRate)
                    .HasColumnName("discount_rate")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.DiscountValueTe)
                    .HasColumnName("discount_value_te")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.Ean13)
                    .HasColumnName("ean13")
                    .HasColumnType("varchar(13)");

                entity.Property(e => e.ExchangeRate)
                    .HasColumnName("exchange_rate")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.IdCurrency)
                    .HasColumnName("id_currency")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdProductAttribute)
                    .HasColumnName("id_product_attribute")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdSupplyOrder)
                    .HasColumnName("id_supply_order")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.PriceTe)
                    .HasColumnName("price_te")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.PriceTi)
                    .HasColumnName("price_ti")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.PriceWithDiscountTe)
                    .HasColumnName("price_with_discount_te")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.PriceWithOrderDiscountTe)
                    .HasColumnName("price_with_order_discount_te")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.QuantityExpected)
                    .HasColumnName("quantity_expected")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.QuantityReceived)
                    .HasColumnName("quantity_received")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Reference)
                    .IsRequired()
                    .HasColumnName("reference")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.SupplierReference)
                    .IsRequired()
                    .HasColumnName("supplier_reference")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.TaxRate)
                    .HasColumnName("tax_rate")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TaxValue)
                    .HasColumnName("tax_value")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.TaxValueWithOrderDiscount)
                    .HasColumnName("tax_value_with_order_discount")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.UnitPriceTe)
                    .HasColumnName("unit_price_te")
                    .HasColumnType("decimal(20,6)")
                    .HasDefaultValueSql("0.000000");

                entity.Property(e => e.Upc)
                    .HasColumnName("upc")
                    .HasColumnType("varchar(12)");
            });

            modelBuilder.Entity<PsSupplyOrderHistory>(entity =>
            {
                entity.HasKey(e => e.IdSupplyOrderHistory)
                    .HasName("PK_ps_supply_order_history");

                entity.ToTable("ps_supply_order_history");

                entity.HasIndex(e => e.IdEmployee)
                    .HasName("id_employee");

                entity.HasIndex(e => e.IdState)
                    .HasName("id_state");

                entity.HasIndex(e => e.IdSupplyOrder)
                    .HasName("id_supply_order");

                entity.Property(e => e.IdSupplyOrderHistory)
                    .HasColumnName("id_supply_order_history")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.EmployeeFirstname)
                    .HasColumnName("employee_firstname")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.EmployeeLastname)
                    .HasColumnName("employee_lastname")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.IdEmployee)
                    .HasColumnName("id_employee")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdState)
                    .HasColumnName("id_state")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdSupplyOrder)
                    .HasColumnName("id_supply_order")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsSupplyOrderReceiptHistory>(entity =>
            {
                entity.HasKey(e => e.IdSupplyOrderReceiptHistory)
                    .HasName("PK_ps_supply_order_receipt_history");

                entity.ToTable("ps_supply_order_receipt_history");

                entity.HasIndex(e => e.IdSupplyOrderDetail)
                    .HasName("id_supply_order_detail");

                entity.HasIndex(e => e.IdSupplyOrderState)
                    .HasName("id_supply_order_state");

                entity.Property(e => e.IdSupplyOrderReceiptHistory)
                    .HasColumnName("id_supply_order_receipt_history")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.EmployeeFirstname)
                    .HasColumnName("employee_firstname")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.EmployeeLastname)
                    .HasColumnName("employee_lastname")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.IdEmployee)
                    .HasColumnName("id_employee")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdSupplyOrderDetail)
                    .HasColumnName("id_supply_order_detail")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdSupplyOrderState)
                    .HasColumnName("id_supply_order_state")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsSupplyOrderState>(entity =>
            {
                entity.HasKey(e => e.IdSupplyOrderState)
                    .HasName("PK_ps_supply_order_state");

                entity.ToTable("ps_supply_order_state");

                entity.Property(e => e.IdSupplyOrderState)
                    .HasColumnName("id_supply_order_state")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.DeliveryNote)
                    .HasColumnName("delivery_note")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Editable)
                    .HasColumnName("editable")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Enclosed)
                    .HasColumnName("enclosed")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.PendingReceipt)
                    .HasColumnName("pending_receipt")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ReceiptState)
                    .HasColumnName("receipt_state")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsSupplyOrderStateLang>(entity =>
            {
                entity.HasKey(e => new { e.IdSupplyOrderState, e.IdLang })
                    .HasName("PK_ps_supply_order_state_lang");

                entity.ToTable("ps_supply_order_state_lang");

                entity.Property(e => e.IdSupplyOrderState)
                    .HasColumnName("id_supply_order_state")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(128)");
            });

            modelBuilder.Entity<PsTab>(entity =>
            {
                entity.HasKey(e => e.IdTab)
                    .HasName("PK_ps_tab");

                entity.ToTable("ps_tab");

                entity.HasIndex(e => e.ClassName)
                    .HasName("class_name");

                entity.HasIndex(e => e.IdParent)
                    .HasName("id_parent");

                entity.Property(e => e.IdTab)
                    .HasColumnName("id_tab")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.ClassName)
                    .IsRequired()
                    .HasColumnName("class_name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.HideHostMode)
                    .HasColumnName("hide_host_mode")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdParent)
                    .HasColumnName("id_parent")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Module)
                    .HasColumnName("module")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<PsTabAdvice>(entity =>
            {
                entity.HasKey(e => new { e.IdTab, e.IdAdvice })
                    .HasName("PK_ps_tab_advice");

                entity.ToTable("ps_tab_advice");

                entity.Property(e => e.IdTab)
                    .HasColumnName("id_tab")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdAdvice)
                    .HasColumnName("id_advice")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<PsTabLang>(entity =>
            {
                entity.HasKey(e => new { e.IdTab, e.IdLang })
                    .HasName("PK_ps_tab_lang");

                entity.ToTable("ps_tab_lang");

                entity.Property(e => e.IdTab)
                    .HasColumnName("id_tab")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsTabModulePreference>(entity =>
            {
                entity.HasKey(e => e.IdTabModulePreference)
                    .HasName("PK_ps_tab_module_preference");

                entity.ToTable("ps_tab_module_preference");

                entity.HasIndex(e => new { e.IdEmployee, e.IdTab, e.Module })
                    .HasName("employee_module")
                    .IsUnique();

                entity.Property(e => e.IdTabModulePreference)
                    .HasColumnName("id_tab_module_preference")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdEmployee)
                    .HasColumnName("id_employee")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdTab)
                    .HasColumnName("id_tab")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Module)
                    .IsRequired()
                    .HasColumnName("module")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<PsTag>(entity =>
            {
                entity.HasKey(e => e.IdTag)
                    .HasName("PK_ps_tag");

                entity.ToTable("ps_tag");

                entity.HasIndex(e => e.IdLang)
                    .HasName("id_lang");

                entity.HasIndex(e => e.Name)
                    .HasName("tag_name");

                entity.Property(e => e.IdTag)
                    .HasColumnName("id_tag")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<PsTagCount>(entity =>
            {
                entity.HasKey(e => new { e.IdGroup, e.IdTag })
                    .HasName("PK_ps_tag_count");

                entity.ToTable("ps_tag_count");

                entity.HasIndex(e => new { e.IdGroup, e.IdLang, e.IdShop, e.Counter })
                    .HasName("id_group");

                entity.Property(e => e.IdGroup)
                    .HasColumnName("id_group")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdTag)
                    .HasColumnName("id_tag")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Counter)
                    .HasColumnName("counter")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsTax>(entity =>
            {
                entity.HasKey(e => e.IdTax)
                    .HasName("PK_ps_tax");

                entity.ToTable("ps_tax");

                entity.Property(e => e.IdTax)
                    .HasColumnName("id_tax")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Deleted)
                    .HasColumnName("deleted")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Rate)
                    .HasColumnName("rate")
                    .HasColumnType("decimal(10,3)");
            });

            modelBuilder.Entity<PsTaxLang>(entity =>
            {
                entity.HasKey(e => new { e.IdTax, e.IdLang })
                    .HasName("PK_ps_tax_lang");

                entity.ToTable("ps_tax_lang");

                entity.Property(e => e.IdTax)
                    .HasColumnName("id_tax")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<PsTaxRule>(entity =>
            {
                entity.HasKey(e => e.IdTaxRule)
                    .HasName("PK_ps_tax_rule");

                entity.ToTable("ps_tax_rule");

                entity.HasIndex(e => e.IdTax)
                    .HasName("id_tax");

                entity.HasIndex(e => e.IdTaxRulesGroup)
                    .HasName("id_tax_rules_group");

                entity.HasIndex(e => new { e.IdTaxRulesGroup, e.IdCountry, e.IdState, e.ZipcodeFrom })
                    .HasName("category_getproducts");

                entity.Property(e => e.IdTaxRule)
                    .HasColumnName("id_tax_rule")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Behavior)
                    .HasColumnName("behavior")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.IdCountry)
                    .HasColumnName("id_country")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdState)
                    .HasColumnName("id_state")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdTax)
                    .HasColumnName("id_tax")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdTaxRulesGroup)
                    .HasColumnName("id_tax_rules_group")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ZipcodeFrom)
                    .IsRequired()
                    .HasColumnName("zipcode_from")
                    .HasColumnType("varchar(12)");

                entity.Property(e => e.ZipcodeTo)
                    .IsRequired()
                    .HasColumnName("zipcode_to")
                    .HasColumnType("varchar(12)");
            });

            modelBuilder.Entity<PsTaxRulesGroup>(entity =>
            {
                entity.HasKey(e => e.IdTaxRulesGroup)
                    .HasName("PK_ps_tax_rules_group");

                entity.ToTable("ps_tax_rules_group");

                entity.Property(e => e.IdTaxRulesGroup)
                    .HasColumnName("id_tax_rules_group")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DateAdd)
                    .HasColumnName("date_add")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateUpd)
                    .HasColumnName("date_upd")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deleted)
                    .HasColumnName("deleted")
                    .HasColumnType("tinyint(1) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<PsTaxRulesGroupShop>(entity =>
            {
                entity.HasKey(e => new { e.IdTaxRulesGroup, e.IdShop })
                    .HasName("PK_ps_tax_rules_group_shop");

                entity.ToTable("ps_tax_rules_group_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdTaxRulesGroup)
                    .HasColumnName("id_tax_rules_group")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsTheme>(entity =>
            {
                entity.HasKey(e => e.IdTheme)
                    .HasName("PK_ps_theme");

                entity.ToTable("ps_theme");

                entity.Property(e => e.IdTheme)
                    .HasColumnName("id_theme")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DefaultLeftColumn)
                    .HasColumnName("default_left_column")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DefaultRightColumn)
                    .HasColumnName("default_right_column")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Directory)
                    .IsRequired()
                    .HasColumnName("directory")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.ProductPerPage)
                    .HasColumnName("product_per_page")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Responsive)
                    .HasColumnName("responsive")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PsThemeMeta>(entity =>
            {
                entity.HasKey(e => e.IdThemeMeta)
                    .HasName("PK_ps_theme_meta");

                entity.ToTable("ps_theme_meta");

                entity.HasIndex(e => e.IdMeta)
                    .HasName("id_meta");

                entity.HasIndex(e => e.IdTheme)
                    .HasName("id_theme");

                entity.HasIndex(e => new { e.IdTheme, e.IdMeta })
                    .HasName("id_theme_2")
                    .IsUnique();

                entity.Property(e => e.IdThemeMeta)
                    .HasColumnName("id_theme_meta")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdMeta)
                    .HasColumnName("id_meta")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdTheme)
                    .HasColumnName("id_theme")
                    .HasColumnType("int(11)");

                entity.Property(e => e.LeftColumn)
                    .HasColumnName("left_column")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.RightColumn)
                    .HasColumnName("right_column")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");
            });

            modelBuilder.Entity<PsThemeSpecific>(entity =>
            {
                entity.HasKey(e => new { e.IdTheme, e.IdShop, e.Entity, e.IdObject })
                    .HasName("PK_ps_theme_specific");

                entity.ToTable("ps_theme_specific");

                entity.Property(e => e.IdTheme)
                    .HasColumnName("id_theme")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Entity)
                    .HasColumnName("entity")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdObject)
                    .HasColumnName("id_object")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsThemeconfigurator>(entity =>
            {
                entity.HasKey(e => e.IdItem)
                    .HasName("PK_ps_themeconfigurator");

                entity.ToTable("ps_themeconfigurator");

                entity.Property(e => e.IdItem)
                    .HasColumnName("id_item")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Hook)
                    .HasColumnName("hook")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Html)
                    .HasColumnName("html")
                    .HasColumnType("text");

                entity.Property(e => e.IdLang)
                    .HasColumnName("id_lang")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ImageH)
                    .HasColumnName("image_h")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.ImageW)
                    .HasColumnName("image_w")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.ItemOrder)
                    .HasColumnName("item_order")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Target)
                    .HasColumnName("target")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.TitleUse)
                    .HasColumnName("title_use")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<PsTimezone>(entity =>
            {
                entity.HasKey(e => e.IdTimezone)
                    .HasName("PK_ps_timezone");

                entity.ToTable("ps_timezone");

                entity.Property(e => e.IdTimezone)
                    .HasColumnName("id_timezone")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<PsWarehouse>(entity =>
            {
                entity.HasKey(e => e.IdWarehouse)
                    .HasName("PK_ps_warehouse");

                entity.ToTable("ps_warehouse");

                entity.Property(e => e.IdWarehouse)
                    .HasColumnName("id_warehouse")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Deleted)
                    .HasColumnName("deleted")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdAddress)
                    .HasColumnName("id_address")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdCurrency)
                    .HasColumnName("id_currency")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdEmployee)
                    .HasColumnName("id_employee")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(45)");

                entity.Property(e => e.Reference)
                    .HasColumnName("reference")
                    .HasColumnType("varchar(32)");
            });

            modelBuilder.Entity<PsWarehouseCarrier>(entity =>
            {
                entity.HasKey(e => new { e.IdCarrier, e.IdWarehouse })
                    .HasName("PK_ps_warehouse_carrier");

                entity.ToTable("ps_warehouse_carrier");

                entity.HasIndex(e => e.IdCarrier)
                    .HasName("id_carrier");

                entity.HasIndex(e => e.IdWarehouse)
                    .HasName("id_warehouse");

                entity.Property(e => e.IdCarrier)
                    .HasColumnName("id_carrier")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdWarehouse)
                    .HasColumnName("id_warehouse")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsWarehouseProductLocation>(entity =>
            {
                entity.HasKey(e => e.IdWarehouseProductLocation)
                    .HasName("PK_ps_warehouse_product_location");

                entity.ToTable("ps_warehouse_product_location");

                entity.HasIndex(e => new { e.IdProduct, e.IdProductAttribute, e.IdWarehouse })
                    .HasName("id_product")
                    .IsUnique();

                entity.Property(e => e.IdWarehouseProductLocation)
                    .HasColumnName("id_warehouse_product_location")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_product")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdProductAttribute)
                    .HasColumnName("id_product_attribute")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdWarehouse)
                    .HasColumnName("id_warehouse")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.Location)
                    .HasColumnName("location")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsWarehouseShop>(entity =>
            {
                entity.HasKey(e => new { e.IdShop, e.IdWarehouse })
                    .HasName("PK_ps_warehouse_shop");

                entity.ToTable("ps_warehouse_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.HasIndex(e => e.IdWarehouse)
                    .HasName("id_warehouse");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdWarehouse)
                    .HasColumnName("id_warehouse")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsWebBrowser>(entity =>
            {
                entity.HasKey(e => e.IdWebBrowser)
                    .HasName("PK_ps_web_browser");

                entity.ToTable("ps_web_browser");

                entity.Property(e => e.IdWebBrowser)
                    .HasColumnName("id_web_browser")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsWebserviceAccount>(entity =>
            {
                entity.HasKey(e => e.IdWebserviceAccount)
                    .HasName("PK_ps_webservice_account");

                entity.ToTable("ps_webservice_account");

                entity.HasIndex(e => e.Key)
                    .HasName("key");

                entity.Property(e => e.IdWebserviceAccount)
                    .HasColumnName("id_webservice_account")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(2)");

                entity.Property(e => e.ClassName)
                    .IsRequired()
                    .HasColumnName("class_name")
                    .HasColumnType("varchar(50)")
                    .HasDefaultValueSql("WebserviceRequest");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.IsModule)
                    .HasColumnName("is_module")
                    .HasColumnType("tinyint(2)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Key)
                    .IsRequired()
                    .HasColumnName("key")
                    .HasColumnType("varchar(32)");

                entity.Property(e => e.ModuleName)
                    .HasColumnName("module_name")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<PsWebserviceAccountShop>(entity =>
            {
                entity.HasKey(e => new { e.IdWebserviceAccount, e.IdShop })
                    .HasName("PK_ps_webservice_account_shop");

                entity.ToTable("ps_webservice_account_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdWebserviceAccount)
                    .HasColumnName("id_webservice_account")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });

            modelBuilder.Entity<PsWebservicePermission>(entity =>
            {
                entity.HasKey(e => e.IdWebservicePermission)
                    .HasName("PK_ps_webservice_permission");

                entity.ToTable("ps_webservice_permission");

                entity.HasIndex(e => e.IdWebserviceAccount)
                    .HasName("id_webservice_account");

                entity.HasIndex(e => e.Resource)
                    .HasName("resource");

                entity.Property(e => e.IdWebservicePermission)
                    .HasColumnName("id_webservice_permission")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdWebserviceAccount)
                    .HasColumnName("id_webservice_account")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Resource)
                    .IsRequired()
                    .HasColumnName("resource")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<PsZone>(entity =>
            {
                entity.HasKey(e => e.IdZone)
                    .HasName("PK_ps_zone");

                entity.ToTable("ps_zone");

                entity.Property(e => e.IdZone)
                    .HasColumnName("id_zone")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(64)");
            });

            modelBuilder.Entity<PsZoneShop>(entity =>
            {
                entity.HasKey(e => new { e.IdZone, e.IdShop })
                    .HasName("PK_ps_zone_shop");

                entity.ToTable("ps_zone_shop");

                entity.HasIndex(e => e.IdShop)
                    .HasName("id_shop");

                entity.Property(e => e.IdZone)
                    .HasColumnName("id_zone")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.IdShop)
                    .HasColumnName("id_shop")
                    .HasColumnType("int(11) unsigned");
            });
        }
    }
}