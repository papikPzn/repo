﻿using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreAsp.Models
{
    public class MysqlConnectionController
    {
        public string ConnectionString { get; set; }

        public MysqlConnectionController(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        public List<PsImageLang> GetAllProducts()
        {
            List<PsImageLang> list = new List<PsImageLang>();

            using (var context = new prestashopContext())
            {
                list = (from x in context.PsImageLang
                       select x).ToList();
            }

            return list;
        }
    }
}

