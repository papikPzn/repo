﻿using System;
using System.Collections.Generic;

namespace NetCoreAsp.Models
{
    public partial class PsCartRuleShop
    {
        public int IdCartRule { get; set; }
        public int IdShop { get; set; }
    }
}
